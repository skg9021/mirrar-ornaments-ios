//
//  ComputePoints.m
//  CubeExample
//
//  Created on 9/9/12.
//
//  This class contains video capture,
//  and all the pre-processing on image
//  the image is then passed to renderer
//  class where it is fed back to GPU
//  pipeline and then passed to texture
//  output and the actual renderer is
//  called 

#import "ComputePoints.h"

@implementation ComputePoints
@synthesize faceDetector;
@synthesize renderers = _renderers;
@synthesize faceQueue;
//@synthesize corners;
@synthesize cornersCount;

- (id)initWithSize:(CGSize)newSize
{
    //Shared Context
    computeContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2 sharegroup:[[[GPUImageOpenGLESContext sharedImageProcessingOpenGLESContext] context] sharegroup]];
    
    if (!computeContext || ![EAGLContext setCurrentContext:computeContext]) {
    //    [self release];
        return nil;
    }
    
    //hingePointL_CV = CGPointMake(0, 0);
    //hingePointR_CV = CGPointMake(0, 0);
    
    backingWidth = (int)newSize.width;
    backingHeight = (int)newSize.height;

    _renderers = [[NSMutableArray alloc] init];
    
    CGSize videoPixelSize = CGSizeMake(480.0, 640.0);
    CGSize scaledVideoPixelSize = CGSizeMake(240.0, 320.0);
    
    rawOutputData = (GLubyte *)calloc(videoPixelSize.width * videoPixelSize.height * 4, sizeof(GLubyte));
    rawProcessData = (GLubyte *)calloc(scaledVideoPixelSize.width * scaledVideoPixelSize.height * 4, sizeof(GLubyte));
    
    videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionFront];
    videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    
    gammaFilter = [[GPUImageGammaFilter alloc] init];
    
    brightnessFilter = [[GPUImageBrightnessFilter alloc] init];
    [brightnessFilter forceProcessingAtSize:scaledVideoPixelSize];
    
    shiTomasiFilter = [[GPUImageShiTomasiFeatureDetectionFilter alloc] init];
    [(GPUImageShiTomasiFeatureDetectionFilter *)shiTomasiFilter setThreshold:0.10];
    
    crosshairGenerator = [[GPUImageCrosshairGenerator alloc] init];
    crosshairGenerator.crosshairWidth = 15.0;
    [crosshairGenerator forceProcessingAtSize:CGSizeMake(480.0, 640.0)];
    
    blendFilter = [[GPUImageAlphaBlendFilter alloc] init];
    [blendFilter forceProcessingAtSize:CGSizeMake(480.0, 640.0)];
    
    rawDataOutput = [[GPUImageRawDataOutput alloc] initWithImageSize:videoPixelSize resultsInBGRAFormat:YES];
    rawDataProcess = [[GPUImageRawDataOutput alloc] initWithImageSize:scaledVideoPixelSize resultsInBGRAFormat:YES];
    
    [videoCamera addTarget:gammaFilter];
    [gammaFilter addTarget:brightnessFilter];
    [gammaFilter addTarget:shiTomasiFilter]; // Added
    [gammaFilter addTarget:blendFilter]; // Added
    [crosshairGenerator addTarget:blendFilter]; // Added
    //[blendFilter addTarget:rawDataOutput]; // Added
    [gammaFilter addTarget:rawDataOutput];
    [brightnessFilter addTarget:rawDataProcess];
    
    NSDictionary *detectorOptions = [[NSDictionary alloc] initWithObjectsAndKeys:CIDetectorAccuracyLow, CIDetectorAccuracy, nil];
    self.faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:detectorOptions];
    faceThinking = NO;
    [videoCamera setDelegate:self];

    
    faceQueue = dispatch_queue_create("com.rhapsodylabs.mo.fqueue", NULL);
    
    ContourDetectionSample *detection = new ContourDetectionSample();
    
    start = false;
    calc = false;
    
    countFrame = 0;

    ///////////////////***** Blocks *****////////////////////////////
    
    // The image goes to Shi Tomasi and corners are retrieved
    [(GPUImageHarrisCornerDetectionFilter *)shiTomasiFilter setCornersDetectedBlock:^(GLfloat * cornerArray, NSUInteger cornersDetected, CMTime frameTime) {
        free(corners);
        cornersCount = cornersDetected;
        corners = (GLfloat *)calloc(cornersCount * 2, sizeof(GLfloat));
        //corners = cornerArray;
        memcpy(corners, cornerArray, cornersCount*2*sizeof(GLfloat));
        //[crosshairGenerator renderCrosshairsFromArray:cornerArray count:cornersDetected frameTime:frameTime];
    }];
    
    
    // The Image goes to Renderer
    __weak GPUImageRawDataOutput *weakRawDataOutput = rawDataOutput;
    
    [weakRawDataOutput setNewFrameAvailableBlock:^{
        rawOutputData = [weakRawDataOutput rawBytesForImage];
        
        for (id <VideoInputProtocol> renderer in _renderers) {
            [renderer videoInputReady];
        }
        
    }];
    
    
    // The Image goes for Processing
    __weak GPUImageRawDataOutput *weakRawDataProcess = rawDataProcess;
    
    [weakRawDataProcess setNewFrameAvailableBlock:^{
        rawProcessData = [weakRawDataProcess rawBytesForImage];
        
        frame = cv::Mat(scaledVideoPixelSize.height, scaledVideoPixelSize.width, CV_8UC4, rawProcessData);
        
        if (clicked && needToInit && start && calc) {
            CGPoint lTappedGL, rTappedGL;
            lTappedGL.x = (clickPointL.x) *2 -1;
            lTappedGL.y = (clickPointL.y) *2 -1;
            
            rTappedGL.x = (clickPointR.x) *2 -1;
            rTappedGL.y = (clickPointR.y) *2 -1;
            
            
            tappedLeft[0] = (lTappedGL.x - facePositiongl[1].x);
            tappedRight[0] = (rTappedGL.x - facePositiongl[1].x);
            
            tappedLeft[1] = (lTappedGL.y - facePositiongl[1].y);
            tappedRight[1] = (rTappedGL.y - facePositiongl[1].y);
            
            tappedLeft[0] = tappedLeft[0]/fabs(facePositiongl[1].x - facePositiongl[2].x);
            tappedRight[0] = tappedRight[0]/fabs(facePositiongl[1].x - facePositiongl[2].x);
            
            tappedLeft[1] = tappedLeft[1]/fabs(facePositiongl[1].y - facePositiongl[0].y);
            tappedRight[1] = tappedRight[1]/fabs(facePositiongl[1].y - facePositiongl[0].y);
            
            
            calc = false;
            
        }
        
        
        if (start) {
            dispatch_async(faceQueue, ^{
                
                GLfloat *cornerArray = (GLfloat *)calloc(cornersCount * 2, sizeof(GLfloat));
                memcpy(cornerArray, corners, cornersCount*2*sizeof(GLfloat));
                
                passedClicked = clicked;
                passedNeedToInit = needToInit;
                passedClickPointL = clickPointL;
                passedClickPointR = clickPointR;
                passedHingePointL_CV = CGPointMake(hingePointL_CV.x, hingePointL_CV.y);
                passedHingePointR_CV = CGPointMake(hingePointR_CV.x, hingePointR_CV.y);
                passedCorrectPoints = correctPoints;
                
                detection->processFrame(frame, outputFrame, roiRect, passedClicked, passedNeedToInit, passedCorrectPoints, passedClickPointL, passedClickPointR, passedHingePointL_CV, passedHingePointR_CV, cornerArray, cornersCount);
                
                clicked = passedClicked;
                needToInit = passedNeedToInit;
                //clickPointL = passedClickPointL;
                //clickPointR = passedClickPointR;
                hingePointL_CV = CGPointMake(passedHingePointL_CV.x, passedHingePointL_CV.y);
                hingePointR_CV = CGPointMake(passedHingePointR_CV.x, passedHingePointR_CV.y);
                correctPoints = passedCorrectPoints;
                free(cornerArray);
                
                //NSLog(@"Left Hinge Point : %@ \t Right Hinge Point :%@",NSStringFromCGPoint(hingePointL_CV),NSStringFromCGPoint(hingePointR_CV));
                
                if (correctPoints && start && !calc) {
                    NSLog(@"INSIDE COORECTPOINTS");
                    float faceWidth = fabs(facePositiongl[1].x - facePositiongl[2].x);
                    float faceHeight = fabs(facePositiongl[1].y - facePositiongl[0].y);
                    clickPointL.x = (tappedLeft[0]*faceWidth + facePositiongl[1].x +1)/2;
                    clickPointL.y = (tappedLeft[1]*faceHeight + facePositiongl[1].y +1)/2;
                    
                    clickPointR.x = (tappedRight[0]*faceWidth + facePositiongl[1].x +1)/2;
                    clickPointR.y = (tappedRight[1]*faceHeight + facePositiongl[1].y +1)/2;
                    
                    needToInit = true;
                    clicked = true;
                    correctPoints = false;
                }
                
                if (countFrame<150){
                    countFrame++;
                }
                else{
                    countFrame = 0;
                }
                
            });
            

        }
        
        
    }];
    
    return self;
}



- (void)startCameraCapture;
{
    [videoCamera startCameraCapture];
}


#pragma mark-
#pragma UI Delegates
- (void)sliderValueChanged:(NSNumber *)value
{
    
    
}

- (void)upperSliderValueChanged:(NSNumber *)value
{
    
    
}

- (void) blurSliderValueChanged:(NSNumber *)value
{
    
}

- (void)setleftHingePoint:(NSValue *)leftImageHingePoint rightHingePoint:(NSValue *)rightImageHingePoint;
{
    mLeftImageHingePoint = [leftImageHingePoint CGPointValue];
    mRightImageHingePoint = [rightImageHingePoint CGPointValue];
}

- (void)setImageWidth:(NSNumber *)width ImageHeight:(NSNumber *)height
{
    mImageWidth = [width floatValue];
    mImageHeight = [height floatValue];
}

-(void)setImageScale:(NSNumber *)scale
{
    mImageScale = [scale floatValue];
}

- (void)computeCLickPoints:(NSValue *)imagePositionGL
{
    CGPoint imagePosition = [imagePositionGL CGPointValue];
    imagePosition = CGPointMake((imagePosition.x+1)/2, (imagePosition.y+1)/2);
    
    imagePosition = CGPointMake(imagePosition.x*1536, imagePosition.y*2048);
    
    clickPointL.x = imagePosition.x-mImageScale*mImageWidth/2+mImageScale*mLeftImageHingePoint.x;
    clickPointL.y = imagePosition.y-mImageScale*mImageHeight/2+mImageScale*mLeftImageHingePoint.y;
    
    clickPointR.x = imagePosition.x-mImageScale*mImageWidth/2+mImageScale*mRightImageHingePoint.x;
    clickPointR.y = imagePosition.y-mImageScale*mImageHeight/2+mImageScale*mRightImageHingePoint.y;
    
    //NSLog(@"Left Click Point : %@ \t Right Click Point :%@",NSStringFromCGPoint(clickPointL),NSStringFromCGPoint(clickPointR));
    
    clickPointL = CGPointMake(clickPointL.x/1536, clickPointR.y/2048);
    clickPointR = CGPointMake(clickPointR.x/1536, clickPointR.y/2048);
    
    myClickL = CGPoint(clickPointL);
    myClickR = CGPoint(clickPointR);
    
    NSLog(@"Points after normalization Left Click Point : %@ \t Right Click Point :%@",NSStringFromCGPoint(clickPointL),NSStringFromCGPoint(clickPointR));
    
    hingePointL_CV = CGPointMake(1-clickPointL.x, clickPointL.y);
    hingePointR_CV = CGPointMake(1-clickPointR.x, clickPointR.y);
    
    NSLog(@"Compute points HingePoints Left:%f \t %f",hingePointL_CV.x,hingePointL_CV.y);
    NSLog(@"Compute Points HingePoints Right:%f \t %f",hingePointR_CV.x,hingePointR_CV.y);
    
    clicked = true;
    passedClicked = true;
    calc = true;
}

- (void) touchedAtXY:(NSValue *)leftPosition :(NSValue *)rightPosition
{
    clickPointL = [leftPosition CGPointValue];
    clickPointR = [rightPosition CGPointValue];
    
    clicked = true;
    passedClicked = true;
    calc = true;
}

-(void) needToInit:(NSNumber *)value
{
    if ([value intValue] == 1) {
        // Clicked Flags
        clicked = true;
        passedClicked = true;
        calc = true;
        //////////////////
        needToInit = true;
        passedNeedToInit = true;
        start = true;
        countFrame = 0;
        
    }
    else if ([value intValue] == 0) {
        needToInit = false;
    }
    
}

- (void) stopTracking:(NSNumber *)status
{
    if ([status intValue] == 1) {
        start = false;
        //hingePointL_CV = CGPointMake(0, 0);
        //hingePointR_CV = CGPointMake(0, 0);
    }
    else {
        start = true;
    }
}

#pragma mark - Face Detection Delegate Callback
-(void) willOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer{
    if (!faceThinking) {
        CFAllocatorRef allocator = CFAllocatorGetDefault();
        CMSampleBufferRef sbufCopyOut;
        CMSampleBufferCreateCopy(allocator,sampleBuffer,&sbufCopyOut);
        [self performSelectorInBackground:@selector(grepFacesForSampleBuffer:) withObject:CFBridgingRelease(sbufCopyOut)];
    }
}

-(void)grepFacesForSampleBuffer:(CMSampleBufferRef)sampleBuffer{
    faceThinking = TRUE;
    //NSLog(@"Faces thinking");
    CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
	CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, sampleBuffer, kCMAttachmentMode_ShouldPropagate);
	CIImage *convertedImage = [[CIImage alloc] initWithCVPixelBuffer:pixelBuffer options:(__bridge NSDictionary *)attachments];
    
	if (attachments)
		CFRelease(attachments);
	NSDictionary *imageOptions = nil;
	UIDeviceOrientation curDeviceOrientation = [[UIDevice currentDevice] orientation];
	int exifOrientation;
	
    /* kCGImagePropertyOrientation values
     The intended display orientation of the image. If present, this key is a CFNumber value with the same value as defined
     by the TIFF and EXIF specifications -- see enumeration of integer constants.
     The value specified where the origin (0,0) of the image is located. If not present, a value of 1 is assumed.
     
     used when calling featuresInImage: options: The value for this key is an integer NSNumber from 1..8 as found in kCGImagePropertyOrientation.
     If present, the detection will be done based on that orientation but the coordinates in the returned features will still be based on those of the image. */
    
	enum {
		PHOTOS_EXIF_0ROW_TOP_0COL_LEFT			= 1, //   1  =  0th row is at the top, and 0th column is on the left (THE DEFAULT).
		PHOTOS_EXIF_0ROW_TOP_0COL_RIGHT			= 2, //   2  =  0th row is at the top, and 0th column is on the right.
		PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT      = 3, //   3  =  0th row is at the bottom, and 0th column is on the right.
		PHOTOS_EXIF_0ROW_BOTTOM_0COL_LEFT       = 4, //   4  =  0th row is at the bottom, and 0th column is on the left.
		PHOTOS_EXIF_0ROW_LEFT_0COL_TOP          = 5, //   5  =  0th row is on the left, and 0th column is the top.
		PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP         = 6, //   6  =  0th row is on the right, and 0th column is the top.
		PHOTOS_EXIF_0ROW_RIGHT_0COL_BOTTOM      = 7, //   7  =  0th row is on the right, and 0th column is the bottom.
		PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM       = 8  //   8  =  0th row is on the left, and 0th column is the bottom.
	};
	BOOL isUsingFrontFacingCamera = FALSE;
    AVCaptureDevicePosition currentCameraPosition = [videoCamera cameraPosition];
    
    if (currentCameraPosition != AVCaptureDevicePositionBack)
    {
        isUsingFrontFacingCamera = TRUE;
    }
    
	switch (curDeviceOrientation) {
		case UIDeviceOrientationPortraitUpsideDown:  // Device oriented vertically, home button on the top
			exifOrientation = PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM;
			break;
		case UIDeviceOrientationLandscapeLeft:       // Device oriented horizontally, home button on the right
			if (isUsingFrontFacingCamera)
				exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
			else
				exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
			break;
		case UIDeviceOrientationLandscapeRight:      // Device oriented horizontally, home button on the left
			if (isUsingFrontFacingCamera)
				exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
			else
				exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
			break;
		case UIDeviceOrientationPortrait:            // Device oriented vertically, home button on the bottom
		default:
			exifOrientation = PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP;
			break;
	}
    
	imageOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:exifOrientation] forKey:CIDetectorImageOrientation];
    //NSLog(@"Current Device Orientation %@", [NSString stringWithFormat:@"%d",curDeviceOrientation]);
    //NSLog(@"Face Detector %@", [self.faceDetector description]);
    //NSLog(@"converted Image %@", [convertedImage description]);
    NSArray *features = [self.faceDetector featuresInImage:convertedImage options:imageOptions];
    
    
    // get the clean aperture
    // the clean aperture is a rectangle that defines the portion of the encoded pixel dimensions
    // that represents image data valid for display.
    CMFormatDescriptionRef fdesc = CMSampleBufferGetFormatDescription(sampleBuffer);
    CGRect clap = CMVideoFormatDescriptionGetCleanAperture(fdesc, false /*originIsTopLeft == false*/);
    
    
    [self GPUVCWillOutputFeatures:features forClap:clap andOrientation:curDeviceOrientation];
    faceThinking = FALSE;
    
}

- (void)GPUVCWillOutputFeatures:(NSArray*)featureArray forClap:(CGRect)clap
                 andOrientation:(UIDeviceOrientation)curDeviceOrientation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //dispatch_async(faceQueue, ^{
        //NSLog(@"Did receive array");
        
        
        //CGRect previewBox = self.view.frame;
        CGRect previewBox = CGRectMake(0, 0, backingWidth, backingHeight);
        //CGRect previewBox = CGRectMake(0, 0, 1, 1);
        
        if (featureArray == nil && faceView) {
            [faceView removeFromSuperview];
            faceView = nil;
        }
        
        
        for (CIFaceFeature *faceFeature in featureArray) {
            
            // find the correct position for the square layer within the previewLayer
            // the feature box originates in the bottom left of the video frame.
            // (Bottom right if mirroring is turned on)
            //NSLog(@"%@", NSStringFromCGRect([faceFeature bounds]));
            
            //Update face bounds for iOS Coordinate System
            faceRect = [faceFeature bounds];
            
            if ([faceFeature hasLeftEyePosition]) {
                leftEyePosition = [faceFeature leftEyePosition];
            }
            if ([faceFeature hasRightEyePosition]) {
                rightEyePosition = [faceFeature rightEyePosition];
            }
            if ([faceFeature hasMouthPosition]) {
                mouthPosition = [faceFeature mouthPosition];
            }
            
            CGFloat temp = leftEyePosition.x;
            leftEyePosition.x = leftEyePosition.y;
            leftEyePosition.y = temp;
            
            temp = rightEyePosition.x;
            rightEyePosition.x = rightEyePosition.y;
            rightEyePosition.y = temp;
            
            temp = mouthPosition.x;
            mouthPosition.x = mouthPosition.y;
            mouthPosition.y = temp;
            
            // flip preview width and height
            temp = faceRect.size.width;
            faceRect.size.width = faceRect.size.height;
            faceRect.size.height = temp;
            temp = faceRect.origin.x;
            faceRect.origin.x = faceRect.origin.y;
            faceRect.origin.y = temp;
            // scale coordinates so they fit in the preview box, which may be scaled
            
            //float reduceWidthBy = 0.2*faceRect.size.width;
            //faceRect.size.width -= reduceWidthBy;
            //faceRect.origin.x += reduceWidthBy;
            
            CGFloat widthScaleBy = 1 / clap.size.height;
            CGFloat heightScaleBy = 1 / clap.size.width;
            
            leftEyePosition.x *= widthScaleBy;
            leftEyePosition.y *= heightScaleBy;
            
            rightEyePosition.x *= widthScaleBy;
            rightEyePosition.y *= heightScaleBy;
            
            mouthPosition.x *= widthScaleBy;
            mouthPosition.y *= heightScaleBy;
            
            //tempFaceRect = CGRectMake(faceRect.origin.x, 640-faceRect.origin.y, faceRect.size.width, faceRect.size.height);
            tempFaceRect = faceRect;
            tempFaceRect.size.width *= widthScaleBy;
            tempFaceRect.size.height *= heightScaleBy;
            tempFaceRect.origin.x *= widthScaleBy;
            tempFaceRect.origin.y *= heightScaleBy;
            
            tempFaceRect = CGRectOffset(tempFaceRect, previewBox.origin.x, previewBox.origin.y);
            tempFaceCentPointX = tempFaceRect.origin.x + tempFaceRect.size.width/2;
            tempFaceCentPointY = tempFaceRect.origin.y + tempFaceRect.size.height/2;
            
            //Upper right Corner
            facePositiongl[0].x = (1 - tempFaceRect.origin.x) *2 -1;
            facePositiongl[0].y = tempFaceRect.origin.y *2 -1;
            
            //Bottom Right Corner
            facePositiongl[1].x = (1 - tempFaceRect.origin.x) *2 -1;
            facePositiongl[1].y = (tempFaceRect.origin.y *2 -1) + tempFaceRect.size.height*2;
            
            //Bottom Left Corner
            facePositiongl[2].x = ((1 - tempFaceRect.origin.x) *2 -1) - tempFaceRect.size.width*2;
            facePositiongl[2].y = (tempFaceRect.origin.y *2 -1) + tempFaceRect.size.height*2;
            
            //Upper Left Corner
            facePositiongl[3].x = ((1 - tempFaceRect.origin.x) *2 -1) - tempFaceRect.size.width*2;
            facePositiongl[3].y = (tempFaceRect.origin.y *2 -1);
            
            //set the region for processing
            neckArea[3].x = 0.0f;//((facePositiongl[3].x+1)/2) - 0.2*2*tempFaceRect.size.width;
            neckArea[3].y = mouthPosition.y;
            
            neckArea[2].x = 0.0f;
            neckArea[2].y = mouthPosition.y + ((facePositiongl[2].y+1)/2 - mouthPosition                              .y)*2;
            
            neckArea[1].x = 1.0f;
            neckArea[1].y = neckArea[2].y;
            
            neckArea[0].x = 1.0;
            neckArea[0].y = neckArea[3].y;
            
            cropRect = CGRectMake(0, mouthPosition.y + ((facePositiongl[2].y+1)/2 - mouthPosition.y)*0.66 , 1.0, ((facePositiongl[2].y+1)/2 - mouthPosition.y)*2);
            
            //cropRect = CGRectMake(0, (facePositiongl[2].y+1)/2 , 1.0, ((facePositiongl[2].y+1)/2 - mouthPosition.y)*2);
            
            
            roiRect = cropRect;
            
            
            //normFaceRect = CGRectMake((1-tempFaceRect.origin.x)*2 - 1, tempFaceRect.origin.y*2-1, tempFaceRect.size.width * 2, tempFaceRect.size.height * 2);
            
            //          CGFloat widthScaleBy = previewBox.size.width / clap.size.height;
            //          CGFloat heightScaleBy = previewBox.size.height / clap.size.width;
            /*            faceRect.size.width *= widthScaleBy;
             faceRect.size.height *= heightScaleBy;
             faceRect.origin.x *= widthScaleBy;
             faceRect.origin.y *= heightScaleBy;
             
             faceRect = CGRectOffset(faceRect, previewBox.origin.x, previewBox.origin.y);
             */
            if (faceView) {
                [faceView removeFromSuperview];
                faceView =  nil;
            }
            
            // create a UIView using the bounds of the face
            faceView = [[UIView alloc] initWithFrame:faceRect];
            
            // add a border around the newly created UIView
            faceView.layer.borderWidth = 1;
            faceView.layer.borderColor = [[UIColor redColor] CGColor];
            
            // add the new view to create a box around the face
            //[self.view addSubview:faceView];
            
            
        }
    });
    
}

@end
