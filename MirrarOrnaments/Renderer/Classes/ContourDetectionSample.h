//
//  ContourDetectionSample.h
//  OpenCV Tutorial
//
//  
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef OpenCV_Tutorial_ContourDetectionSample_h
#define OpenCV_Tutorial_ContourDetectionSample_h

//#include "SampleBase.h"
#include <CoreGraphics/CoreGraphics.h>
#include <Foundation/Foundation.h>



class ContourDetectionSample
{
   
  
    
    void getGray(const cv::Mat& input, cv::Mat& gray);

private:
    
    cv::Mat gray, edges, prevGray;
    
    cv::FastFeatureDetector detectorFast;
    
    std::vector<cv::KeyPoint> keyPointFeatures;
    std::vector<cv::KeyPoint>::iterator keyPointIt;
    
    std::vector<cv::Point2f>::iterator pointIt;
    std::vector<cv::Vec2f>::iterator vecIt;
    
    cv::Size winSize;
    
    bool adPoint;
    std::vector<cv::Point2f> points[2];

    std::vector<cv::Point2f> rPoint;
    std::vector<cv::Point2f> lPoint;
    
    std::vector<cv::Vec2f> leftHalf, rightHalf, tempLeft, tempRight;
    
    CGPoint hingePointL;
    CGPoint hingePointR;
    
    int i;
    
    int countL;
    int countR;
    
    int factorX;
    int factorY;
    
    float errorL;
    float errorR;
    
    int countFrame;
    
    cv::Size subPixWinSize;
    
    int mLeftFeaturesState;
    int mRightFeaturesState;
    /*
        -1 - Reduce the size of Circle
         0 - Size of Circle is Perfect
        +1 - Increase the size of Circle
     */
    
    int mLeftCircleSize;
    int mRightCircleSize;
    //dispatch_queue_t opticalFlowQueue;
    
    
public:
    //! Processes a frame and returns output image
    virtual bool processFrame(const cv::Mat& inputFrame, cv::Mat& outputFrame, const CGRect&roiRect, bool& clicked, bool& needToInit, bool& correctPoints, CGPoint& clickPointL, CGPoint& clickPointR, CGPoint& hingePointL_CV, CGPoint& hingePointR_CV, GLfloat *cornerArray, int cornerCount);
    
    void init(bool& flagCorrect);
    
    ContourDetectionSample();
};

#endif
