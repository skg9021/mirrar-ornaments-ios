#import "DisplayViewController.h"

@interface DisplayViewController ()

@end

@implementation DisplayViewController
@synthesize delegate;

- (IBAction)sliderChanged:(id)sender
{
    sliderLabel.text = [NSString stringWithFormat:@"%f",[(UISlider *)sender value]];
    [delegate performSelector:@selector(sliderValueChanged:) withObject:[NSNumber numberWithFloat:[(UISlider *)sender value]]];
}

- (IBAction)upperSliderChanged:(id)sender
{
    upperSliderLabel.text = [NSString stringWithFormat:@"%f",[(UISlider *)sender value]];
    [delegate performSelector:@selector(upperSliderValueChanged:) withObject:[NSNumber numberWithFloat:[(UISlider *)sender value]]];
}

-(IBAction)imageSwitched:(id)sender{
    
    [delegate performSelector:@selector(imageSwitchChanged:) withObject:[NSNumber numberWithBool:[(UISwitch *)sender isOn]]];
}

- (IBAction)blurSliderChanged:(id)sender
{
    blurLabel.text = [NSString stringWithFormat:@"%f",[(UISlider *)sender value]];
    [delegate performSelector:@selector(blurSliderValueChanged:) withObject:[NSNumber numberWithFloat:[(UISlider *)sender value]]];
    
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    

    return self;
}

- (IBAction)calibratePressed:(id)sender
{
    if (!buttonPressed)
        buttonPressed = true;
    count = 1;

}

- (IBAction)needToInitPressed:(id)sender
{
    [delegate performSelector:@selector(needToInit:) withObject:[NSNumber numberWithBool:YES]];
}

- (void)dealloc
{
    //[renderer release];
    
    //[sliderLabel release];
    //[super dealloc];
}

- (void)loadView
{
    buttonPressed = false;
    count = 0;
    CGRect mainScreenFrame = [[UIScreen mainScreen] applicationFrame];	
	GPUImageView *primaryView = [[GPUImageView alloc] initWithFrame:mainScreenFrame];

    computePointsEngine = [[ComputePoints alloc] initWithSize:[primaryView sizeInPixels]];
    
    //renderer = [[ES2Renderer alloc] initWithSize:[primaryView sizeInPixels]];
    [renderer setComputePoints:computePointsEngine];
    
    NSMutableArray *renderers = [computePointsEngine renderers];
    __weak ES2Renderer *weakRenderer = renderer;
    [renderers addObject:weakRenderer];
    self.delegate = computePointsEngine;
    
    
    //Creating the UI
    CGRect frame  = CGRectMake(100, 20, 500, 20);
    CGRect upperFrame = CGRectMake(100, 50, 500, 20);
    UISlider *slider = [[UISlider alloc] initWithFrame:frame];
    UISlider *upperSlider = [[UISlider alloc] initWithFrame:upperFrame];
	self.view = primaryView;

    
    
    [slider addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
    [slider setBackgroundColor:[UIColor clearColor]];
    slider.minimumValue = 0.0;
    slider.maximumValue = 1.0;
    slider.continuous = YES;
    
    [self.view addSubview:slider];
    
    CGRect labelFrame = CGRectMake(620, 20, 80, 25);
    sliderLabel = [[UILabel alloc] initWithFrame:labelFrame];
    [self.view addSubview:sliderLabel];
    
    
    [upperSlider addTarget:self action:@selector(upperSliderChanged:) forControlEvents:UIControlEventValueChanged];
    [upperSlider setBackgroundColor:[UIColor clearColor]];
    upperSlider.minimumValue = 0.0;
    upperSlider.maximumValue = 1.0;
    upperSlider.continuous = YES;
    
    [self.view addSubview:upperSlider];
    
    CGRect upperLabelFrame = CGRectMake(620, 50, 80, 30);
    upperSliderLabel = [[UILabel alloc] initWithFrame:upperLabelFrame];
    [self.view addSubview:upperSliderLabel];
    
    //Blur slider
    upperLabelFrame = CGRectMake(60, 80, 500, 20);
    UISlider *blurSlider = [[UISlider alloc] initWithFrame:upperLabelFrame];
    [blurSlider addTarget:self action:@selector(blurSliderChanged:) forControlEvents:UIControlEventValueChanged];
    [blurSlider setBackgroundColor:[UIColor clearColor]];
    blurSlider.minimumValue = 0.0;
    blurSlider.maximumValue = 1.0;
    blurSlider.continuous = YES;
    
    [self.view addSubview:blurSlider];
    
    upperLabelFrame = CGRectMake(580, 80, 80, 30);
    blurLabel = [[UILabel alloc] initWithFrame:upperLabelFrame];
    [self.view addSubview:blurLabel];
    
    //Switch
    upperLabelFrame = CGRectMake(500,980,40,30);
    imageSwitch = [[UISwitch alloc] initWithFrame:upperLabelFrame];
    //[self.view addSubview:imageSwitch];
    
    //[imageSwitch addTarget:self action:@selector(imageSwitched:) forControlEvents:UIControlEventValueChanged];
    
    //Button
    CGRect buttonFrame = CGRectMake( 80, 980, 100, 30 );
    UIButton *button = [[UIButton alloc] initWithFrame: buttonFrame];
    button.backgroundColor = [UIColor colorWithRed:0.078 green:0.078 blue:0.078 alpha:1.0];
    button.showsTouchWhenHighlighted = YES;
    
    [button setTitle: @"Calibrate" forState: UIControlStateNormal];
    [button setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
    [button addTarget:self action:@selector(calibratePressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: button];
    
    // Need to init Button
    buttonFrame = CGRectMake( 200, 980, 100, 30 );
    UIButton *needToInitbutton = [[UIButton alloc] initWithFrame: buttonFrame];
    needToInitbutton.backgroundColor = [UIColor colorWithRed:0.078 green:0.078 blue:0.078 alpha:255];
    needToInitbutton.showsTouchWhenHighlighted = YES;
    [needToInitbutton setTitle: @"Init" forState: UIControlStateNormal];
    [needToInitbutton setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
    [needToInitbutton addTarget:self action:@selector(needToInitPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: needToInitbutton];
    
    //Textures for GPUImage
    
    textureInput = [[GPUImageTextureInput alloc] initWithTexture:renderer.outputTexture size:[primaryView sizeInPixels]];



    [textureInput addTarget:primaryView];
    
    
   
    
    [renderer setNewFrameAvailableBlock:^{
        float currentTimeInMilliseconds = [[NSDate date] timeIntervalSinceDate:startTime] * 1000.0;
        
        [textureInput processTextureWithFrameTime:CMTimeMake((int)currentTimeInMilliseconds, 1000)];
    }];
    
    [computePointsEngine startCameraCapture];
}

- (void)viewWillDisappear:(BOOL)animated
{
    // Note: I needed to stop camera capture before the view went off the screen in order to prevent a crash from the camera still sending frames
    [renderer->videoCamera stopCameraCapture];
    
	[super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)drawView:(id)sender
{
    [renderer renderByRotatingAroundX:0 rotatingAroundY:0 scale:1];
}

#pragma mark -
#pragma mark Touch-handling methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSMutableSet *currentTouches = [[event touchesForView:self.view] mutableCopy];
    [currentTouches minusSet:touches];
	
	// New touches are not yet included in the current touches for the view
	lastMovementPosition = [[touches anyObject] locationInView:self.view];
    
    if (count == 1) {
        leftClickPoint = CGPointMake(lastMovementPosition.x/768, lastMovementPosition.y/1024);
        count++;
    }
    else if (count == 2) {
        rightClickPoint = CGPointMake(lastMovementPosition.x/768, lastMovementPosition.y/1024);
        if (leftClickPoint.x>rightClickPoint.x) {
            CGPoint temp = rightClickPoint;
            rightClickPoint = leftClickPoint;
            leftClickPoint = temp;
        }
        count = 0;
        [delegate performSelector:@selector(touchedAtXY::) withObject:[NSValue valueWithCGPoint:leftClickPoint] withObject:[NSValue valueWithCGPoint:rightClickPoint]];
        //[delegate touchedAtXY:[NSValue valueWithCGPoint:leftClickPoint] :[NSValue valueWithCGPoint:rightClickPoint]];
    }
    
     //[NSNumber numberWithFloat:lastMovementPosition.x/768] withObject:[NSNumber numberWithFloat:lastMovementPosition.x/1024]];
    
    //[NSValue valueWithCGPoint:leftClickPoint];
    //[delegate performSelector:@selector(touchedAtY:) withObject:[NSNumber numberWithFloat:lastMovementPosition.x/1024]];

    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
{
	CGPoint currentMovementPosition = [[touches anyObject] locationInView:self.view];
	//[renderer renderByRotatingAroundX:(currentMovementPosition.x - lastMovementPosition.x) rotatingAroundY:(lastMovementPosition.y - currentMovementPosition.y)];
	lastMovementPosition = currentMovementPosition;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event 
{
	NSMutableSet *remainingTouches = [[event touchesForView:self.view] mutableCopy];
    [remainingTouches minusSet:touches];
    
	lastMovementPosition = [[remainingTouches anyObject] locationInView:self.view];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event 
{
	// Handle touches canceled the same as as a touches ended event
    [self touchesEnded:touches withEvent:event];
}


@end
