//
//  ComputePoints.h
//  CubeExample
//
//  Created on 9/9/12.
//
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <QuartzCore/QuartzCore.h>
#import "ContourDetectionSample.h"

#import "GPUImage.h"

@class ES2Renderer;

@protocol SliderProtocol <NSObject>
@optional
- (void) sliderValueChanged:(NSNumber *)value;
- (void) upperSliderValueChanged:(NSNumber *)value;
- (void) blurSliderValueChanged:(NSNumber *)value;
- (void) imageSwitchChanged:(NSNumber *)value;//(BOOL)value;
- (void) touchedAtXY:(NSValue *)leftPosition :(NSValue *)rightPosition;
- (void) setleftHingePoint:(NSValue *)leftImageHingePoint rightHingePoint:(NSValue *)rightImageHingePoint;
- (void) setImageWidth:(NSNumber *)width ImageHeight:(NSNumber *)height;
- (void) setImageScale:(NSNumber *)scale;
- (void) computeCLickPoints:(NSValue *)imagePositionGL;
- (void) needToInit:(NSNumber *)value;
- (void) stopTracking:(NSNumber *)status;
@end

@protocol VideoInputProtocol <NSObject>
- (void) videoInputReady;
@end

@interface ComputePoints : NSObject <GPUImageVideoCameraDelegate, SliderProtocol>
{
@private
    EAGLContext *computeContext;
    
    // The pixel dimensions of the CAEAGLLayer
    GLint backingWidth;
    GLint backingHeight;
    
    
 
    GPUImageGammaFilter *gammaFilter;
    GPUImageBrightnessFilter *brightnessFilter;
    GPUImageOutput<GPUImageInput> *shiTomasiFilter;
    GPUImageCrosshairGenerator *crosshairGenerator;
    GPUImageAlphaBlendFilter *blendFilter;

    GPUImageRawDataOutput *rawDataProcess;
    GPUImageRawDataOutput *rawDataOutput;
    
    cv::Mat frame, outputFrame;
    
@public
    GPUImageVideoCamera *videoCamera;
    
    //  Face Detection
    BOOL faceThinking;
    UIView *faceView;
    CGRect faceRect;
    CGPoint leftEyePosition;
    CGPoint rightEyePosition;
    CGPoint mouthPosition;
    CGRect tempFaceRect;
    float tempFaceCentPointX;
    float tempFaceCentPointY;
    CGPoint neckArea[4];
    CGPoint facePositiongl[4];
    /*
     
        (4)  --------------   (1)
            |              |
            |              |
            |              |
            |              |
            |              |
            |              |
            |              |
        (3)  --------------   (2)
     
     */
    
    CGRect cropRect;
    CGRect roiRect;
    
    // Click Points
    bool clicked;
    bool needToInit;
    CGPoint clickPointL;
    CGPoint clickPointR;
    bool passedClicked;
    bool passedNeedToInit;
    bool start;
    bool calc;
    
    // Processing
    float tappedLeft[2];
    float tappedRight[2];
    int countFrame;
    
    CGPoint passedClickPointL;
    CGPoint passedClickPointR;
    CGPoint passedHingePointL_CV;
    CGPoint passedHingePointR_CV;
    CGPoint hingePointL_CV;
    CGPoint hingePointR_CV;
    bool passedCorrectPoints, correctPoints;
    
    // Image HInge Points
    CGPoint mLeftImageHingePoint, mRightImageHingePoint;
    float mImageWidth,mImageHeight, mImageScale;
    
    CGPoint myClickL, myClickR;
    
   
    
    GLubyte *rawOutputData;
    GLubyte *rawProcessData;
    
    GLfloat *corners;
}

@property(nonatomic,retain) CIDetector*faceDetector;
@property (nonatomic, copy) void(^newFrameAvailableBlock)(void);
@property (strong, nonatomic) NSMutableArray *renderers;
- (void)GPUVCWillOutputFeatures:(NSArray*)featureArray forClap:(CGRect)clap
                 andOrientation:(UIDeviceOrientation)curDeviceOrientation;
@property (nonatomic, assign)  dispatch_queue_t faceQueue;
//@property GLfloat * corners;
@property int cornersCount;


- (void)startCameraCapture;
- (id)initWithSize:(CGSize)newSize;
//- (void)videoOutput;
//- (void)setWidth:(int)width setHeight:(int)height;
@end
