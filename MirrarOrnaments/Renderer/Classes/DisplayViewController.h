#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ESRenderer.h"
#import "ES2Renderer.h"
#import "GPUImage.h"




@interface DisplayViewController : UIViewController
{
    CGPoint lastMovementPosition;
@private
    ES2Renderer *renderer;
    GPUImageTextureInput *textureInput;
    GPUImageFilter *filter;
    ComputePoints *computePointsEngine;
    
    UILabel *sliderLabel;
    UILabel *upperSliderLabel;
    UILabel *blurLabel;
    
    UISwitch *imageSwitch;

    
    NSDate *startTime;
    
    
    CGPoint leftClickPoint;
    CGPoint rightClickPoint;
    
    bool buttonPressed;
    
    int count;
}

- (void)drawView:(id)sender;
- (IBAction) sliderChanged:(id)sender;
- (IBAction) upperSliderChanged:(id)sender;
- (IBAction) blurSliderChanged:(id)sender;
- (IBAction)imageSwitched:(id)sender;
- (IBAction)calibratePressed:(id)sender;
- (IBAction)needToInitPressed:(id)sender;
@property (strong,nonatomic) id <SliderProtocol> delegate;
//@property (nonatomic, retain) ES2Renderer *renderer;
@end
