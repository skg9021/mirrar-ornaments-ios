//
//  ContourDetectionSample.cpp
//  OpenCV Tutorial
//
//  
//  
//

#include <iostream>
#include "ContourDetectionSample.h"


void ContourDetectionSample::init(bool& flagCorrect)
{
    
    //adPoint = clicked;
    flagCorrect = false;
    countL = 0;
    countR = 0;
    
    errorL = 0.0f;
    errorR = 0.0f;

    
    if (mLeftFeaturesState==-1) {
        mLeftCircleSize -= mLeftCircleSize/2;
        if (mLeftCircleSize<5) {
            mLeftCircleSize = 5;
        }
    }
    else if (mLeftFeaturesState==1) {
        mLeftCircleSize += mLeftCircleSize/2;
        if (mLeftCircleSize>160) {
            mLeftCircleSize = 160;
        }
    }
    
    if (mRightFeaturesState==-1) {
        mRightCircleSize -= mRightCircleSize/2;
        if (mRightCircleSize<5) {
            mRightCircleSize = 5;
        }
    }
    else if (mRightFeaturesState==1) {
        mRightCircleSize += mRightCircleSize/2;
        if (mRightCircleSize>160) {
            mRightCircleSize = 160;
        }
    }
    
}

ContourDetectionSample::ContourDetectionSample()
{
    countFrame = 0;
    
    mLeftCircleSize = 23;
    mRightCircleSize = 23;
    
    mLeftFeaturesState = 0;
    mRightFeaturesState = 0;
    
    detectorFast = cv::FastFeatureDetector(7, true);
    winSize = cvSize(15, 15);
    
    factorX = 2;
    factorY = 2;
    
        
    subPixWinSize = cvSize(10, 10);
    



}

//! Processes a frame and returns output image 
bool ContourDetectionSample::processFrame(const cv::Mat& inputFrame, cv::Mat& outputFrame, const CGRect&roiRect, bool& clicked, bool& needToInit, bool& correctPoints, CGPoint& clickPointL, CGPoint& clickPointR, CGPoint& hingePointL_CV, CGPoint& hingePointR_CV, GLfloat *cornerArray, int cornerCount)
{
    
    init(correctPoints);
    
    hingePointL_CV = CGPointMake(0, 0);
    hingePointR_CV = CGPointMake(0, 0);
    //inputFrame.copyTo(outputFrame);
    //cv::resize(inputFrame, edges, cv::Size(), 1./factorX, 1./factorY);
    getGray(inputFrame, gray);
    
    //opticalFlowQueue = dispatch_queue_create("com.yourcompany.CubeExample.ofqueue", NULL);
    
    if (needToInit) {
        hingePointL = CGPointMake(0, 0);
        hingePointR = CGPointMake(0, 0);
        
        if (mLeftCircleSize>160 || mRightCircleSize>160) {
            mLeftCircleSize = 23;
            mRightCircleSize = 23;
        }
        
        
        //clickPointL = CGPointMake(480-clickPointL.x*480, clickPointL.y*640);
        //clickPointR = CGPointMake(480-clickPointR.x*480, clickPointR.y*640);
        clickPointL = CGPointMake((gray.cols-clickPointL.x*gray.cols), (clickPointL.y*gray.rows));
        clickPointR = CGPointMake((gray.cols-clickPointR.x*gray.cols), (clickPointR.y*gray.rows));
        points[1].clear();
        
        cv::Vec2f clickVectorL = cv::Vec2f(clickPointL.x, clickPointL.y);
        cv::Vec2f clickVectorR = cv::Vec2f(clickPointR.x, clickPointR.y);
        
        cv::TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03);
        
        
        //detectorFast.detect(inputFrame, keyPointFeatures);
       
        /*
        cv::goodFeaturesToTrack(gray, points[1], 300, 0.01, 10, cv::Mat(), 3, false, 0.04);
        // refine corner locations
        cv::cornerSubPix(gray, points[1], subPixWinSize, cv::Size(-1,-1), termcrit);
        */
        
        if (clicked) {
            NSLog(@"Inside the feature Detector");
            leftHalf.clear();
            rightHalf.clear();
            int i;
            for ( i=0; i<=cornerCount*2; ) {
                float cornerArrayX = (1-cornerArray[i])*480/factorX;
                float cornerArrayY = cornerArray[i+1]*640/factorY;
                
                double distL = sqrt((clickPointL.x-cornerArrayX)*(clickPointL.x-cornerArrayX) + (clickPointL.y-cornerArrayY)*(clickPointL.y-cornerArrayY));
                
                double distR = sqrt((clickPointR.x-cornerArrayX)*(clickPointR.x-cornerArrayX) + (clickPointR.y-cornerArrayY)*(clickPointR.y-cornerArrayY));
                
                if (distL > mLeftCircleSize && distR > mRightCircleSize) {
                    //pointIt = points[1].erase(pointIt);
                    i += 2;
                }
                else {
                    if (distL<mLeftCircleSize && countL<15) {
                        //hingePointL = CGPointMake(hingePointL.x+pointIt->x, hingePointL.y+pointIt->y);
                        leftHalf.push_back(clickVectorL - cv::Vec2f(cornerArrayX,cornerArrayY));
                        points[1].push_back(cv::Point2f(cornerArrayX, cornerArrayY));
                        //++pointIt;
                        i += 2;
                        countL++;
                    }
                    else if (distR<mRightCircleSize && countR<15) {
                        //hingePointR = CGPointMake(hingePointR.x+pointIt->x, hingePointR.y+pointIt->y);
                        rightHalf.push_back(clickVectorR - cv::Vec2f(cornerArrayX, cornerArrayY));
                        points[1].push_back(cv::Point2f(cornerArrayX,cornerArrayY));
                        //++pointIt;
                        i += 2;
                        countR++;
                    }
                    else {
                        if (countL>=15) {
                            countL++;
                        }
                        if (countR>=15) {
                            countR++;
                        }
                        //pointIt = points[1].erase(pointIt);
                        i += 2;
                    }
                    
                }
                
                
            }
                
        
        
            /*
            for (keyPointIt = keyPointFeatures.begin(); keyPointIt != keyPointFeatures.end(); ) {
                
                double distL = sqrt((clickPointL.x-keyPointIt->pt.x)*(clickPointL.x-keyPointIt->pt.x) + (clickPointL.y-keyPointIt->pt.y)*(clickPointL.y-keyPointIt->pt.y));
                
                double distR = sqrt((clickPointR.x-keyPointIt->pt.x)*(clickPointR.x-keyPointIt->pt.x) + (clickPointR.y-keyPointIt->pt.y)*(clickPointR.y-keyPointIt->pt.y));
                
                if (distL > 23 && distR > 23)
                    keyPointIt = keyPointFeatures.erase(keyPointIt);
                else {
                    if (distL<23 && countL<15) {
                        //hingePointL = CGPointMake(hingePointL.x+keyPointIt->pt.x, hingePointL.y+keyPointIt->pt.y);
                        leftHalf.push_back(clickVectorL - cv::Vec2f(keyPointIt->pt.x,keyPointIt->pt.y));
                        points[1].push_back(keyPointIt->pt);
                        ++keyPointIt;
                        countL++;
                    }
                    else if (distR<23 && countR<15) {
                        //hingePointR = CGPointMake(hingePointR.x+keyPointIt->pt.x, hingePointR.y+keyPointIt->pt.y);
                        rightHalf.push_back(clickVectorR - cv::Vec2f(keyPointIt->pt.x,keyPointIt->pt.y));
                        points[1].push_back(keyPointIt->pt);
                        ++keyPointIt;
                        countR++;
                    }
                    else
                        keyPointIt = keyPointFeatures.erase(keyPointIt);
                    
                }
                    
                
            }
             
            */
            
            if(countL!=0)
                hingePointL = clickPointL;
                //hingePointL = CGPointMake(hingePointL.x/countL, hingePointL.y/countL);
            if(countR!=0)
                hingePointR = clickPointR;
                //hingePointR = CGPointMake(hingePointR.x/countR, hingePointR.y/countR);
            
            //NSLog(@"CountL : %d \tCountR : %d",countL,countR);
            
            if (countL!=0 && countR!=0) {
                clicked = false;
                needToInit = false;
            }
            else {
                
            }
            
            if (countL<4) {
                mLeftFeaturesState = 1;
            }
            else if (countL>18) {
                mLeftFeaturesState = -1;
            }
            else {
                mLeftFeaturesState = 0;
            }
            
            if (countR<4) {
                mRightFeaturesState = 1;
            }
            else if (countR>18) {
                mRightFeaturesState = -1;
            }
            else {
                mRightFeaturesState = 0;
            }
            

        }
        
        NSLog(@"mLeftCircleSize : %d", mLeftCircleSize);
        NSLog(@"mRightCircleSize : %d", mRightCircleSize);
        
        /*
        for (keyPointIt = keyPointFeatures.begin(), i = 0; keyPointIt != keyPointFeatures.end(); ++keyPointIt, ++i) {
            points[1].push_back(keyPointIt->pt);
            if (i==499)
                break;
            
        }
         */
        
    }
    else if (!points[0].empty()) {
        NSLog(@"Inside Optical Flow");
        std::vector<uchar> status;
        std::vector<float> err;
        cv::TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03);
        countL = 0;
        countR = 0;
        
        if (prevGray.empty())
            gray.copyTo(prevGray);
        
        //float t = cv::getTickCount();
        //dispatch_async(dispatch_get_main_queue(), ^{
            cv::calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize, 3, termcrit, 0, 0.001);
        //});
        
        //NSLog(@"Time Taken %f",1000*(cv::getTickCount()-t)/cv::getTickFrequency());
        
        size_t i, k;
        
        for (i = k =0; i < points[1].size(); i++) {
            if (!status[i]) {
                NSLog(@"Wasted");
                
                double distL = sqrt((hingePointL.x-leftHalf[countL][0]-points[0][i].x)*(hingePointL.x-leftHalf[countL][0]-points[0][i].x) + (hingePointL.y-leftHalf[countL][1]-points[0][i].y)*(hingePointL.y-leftHalf[countL][1]-points[0][i].y));
                
                double distR = sqrt((hingePointR.x-rightHalf[countR][0]-points[0][i].x)*(hingePointR.x-rightHalf[countR][0]-points[0][i].x) + (hingePointR.y-rightHalf[countR][1]-points[0][i].y)*(hingePointR.y-rightHalf[countR][1]-points[0][i].y));
                
                if (distL<1) {
                    leftHalf.erase(leftHalf.begin() + countL);
                }
                else if (distR<1) {
                    rightHalf.erase(rightHalf.begin() + countR);
                }
                
                /*
                vecIt = std::find(leftHalf.begin(), leftHalf.end(), cv::Vec2f(points[1][i].x,points[1][i].y));
                if (vecIt!= leftHalf.end()) {
                    leftHalf.erase(vecIt);
                    
                }
                else {
                    vecIt = std::find(rightHalf.begin(), rightHalf.end(), cv::Vec2f(points[1][i].x,points[1][i].y));
                    rightHalf.erase(vecIt);
                }
                 */
                continue;
            }
            
            double distL = sqrt((hingePointL.x-points[1][i].x)*(hingePointL.x-points[1][i].x) +
                                (hingePointL.y-points[1][i].y)*(hingePointL.y-points[1][i].y));
            
            double distR = sqrt((hingePointR.x-points[1][i].x)*(hingePointR.x-points[1][i].x) +
                                (hingePointR.y-points[1][i].y)*(hingePointR.y-points[1][i].y));
            
            
            
            if(distL<distR){
                tempLeft.push_back(cv::Vec2f(points[1][i].x, points[1][i].y));
                //hingePointL_CV = CGPointMake(hingePointL_CV.x+points[1][i].x, hingePointL_CV.y+points[1][i].y);
                hingePointL_CV = CGPointMake(hingePointL_CV.x+points[1][i].x+leftHalf[countL][0], hingePointL_CV.y+points[1][i].y+leftHalf[countL][1]);
                countL++;
                errorL += err[i];
            }
            else{
                tempRight.push_back(cv::Vec2f(points[1][i].x, points[1][i].y));
                //hingePointR_CV = CGPointMake(hingePointR_CV.x+points[1][i].x, hingePointR_CV.y+points[1][i].y);
                hingePointR_CV = CGPointMake(hingePointR_CV.x+points[1][i].x+rightHalf[countR][0], hingePointR_CV.y+points[1][i].y+rightHalf[countR][0]);
                
                countR++;
                errorR += err[i];
            }
            points[1][k++] = points[1][i];
            
            
            //Draw the points points[1][i]
            //cv::circle( outputFrame, cv::Point2f(points[1][i].x*factorX,points[1][i].y*factorY), 3, cv::Scalar(0,255,0), -1, 8);
            
        }
        //NSLog(@"CountL : %d \tCountR : %d",countL,countR);
        points[1].resize(k);
        hingePointL = CGPointMake(hingePointL_CV.x/countL, hingePointL_CV.y/countL);
        hingePointR = CGPointMake(hingePointR_CV.x/countR, hingePointR_CV.y/countR);
        
        
        leftHalf.clear();
        rightHalf.clear();
        
        for (i=0; i<tempLeft.size(); i++) {
            leftHalf.push_back(cv::Vec2f(hingePointL.x,hingePointL.y) - tempLeft[i]);
            
        }
        NSLog(@"Left Hand Size: %ld",leftHalf.size());
        for (i=0; i<tempRight.size(); i++) {
            rightHalf.push_back(cv::Vec2f(hingePointR.x,hingePointR.y) - tempRight[i]);
            
        }
        NSLog(@"Right Hand Size: %ld",rightHalf.size());
        tempLeft.clear();
        tempRight.clear();
        
        errorL = errorL/countL;
        errorR = errorR/countR;
        
        
        
        NSLog(@"Error in Left : %f \t Error in Right : %f",errorL,errorR);
        NSLog(@"CountL : %d \tCountR : %d",countL,countR);
        NSLog(@"Left Hinge Point : %@ \t Right Hinge Point :%@",NSStringFromCGPoint(hingePointL),NSStringFromCGPoint(hingePointR));
        
        if (countL==0 || countR==0) {
            correctPoints = true;
        }
    }
    
//    hingePointL_CV = CGPointMake((2*hingePointL.x)/480, (2*hingePointL.y)/640);
//    hingePointR_CV = CGPointMake((2*hingePointR.x)/480, (2*hingePointR.y)/640);
    hingePointL_CV = CGPointMake((factorX*hingePointL.x)/480, (factorY*hingePointL.y)/640);
    hingePointR_CV = CGPointMake((factorX*hingePointR.x)/480, (factorY*hingePointR.y)/640);

    
    if (hingePointL_CV.y<roiRect.origin.y || hingePointL_CV.y>(roiRect.size.height+roiRect.origin.y) || hingePointR_CV.y<roiRect.origin.y || hingePointR_CV.y>(roiRect.size.height+roiRect.origin.y) ||
        errorL>9.0 || errorR>9.0) {
        //NSLog(@"Put your face straight");
        
        countFrame++;
        if(countFrame>200){
            hingePointL_CV.y = roiRect.origin.y;
            hingePointR_CV.y = roiRect.origin.y;
            correctPoints = true;
        }
            
        //countFrame = 0;
    }
    
    if (countFrame>550) {
        //correctPoints = true;
        NSLog(@" RESETING click location");
    }
    
        //inputFrame.copyTo(outputFrame);
    
    /*
    for (int y=0; y<100; y++) {
        uchar * rowPtr = outputFrame.ptr(y);
        for (int x= 0; x<480; x++) {
            
            outputFrame.data[outputFrame.cols * y + 4*x ] = 0;
            outputFrame.data[outputFrame.cols * y + 4*x + 1] = 0;
            outputFrame.data[outputFrame.cols * y + 4*x + 2] = 0;
            outputFrame.data[outputFrame.cols * y + 4*x + 3] = 255;
            
            rowPtr[4*x] = 0;
            rowPtr[4*x+1] = 0;
            rowPtr[4*x+2] = 0;
            rowPtr[4*x+3] = 255;
        }
    }
    */
    
    std::swap(points[1], points[0]);
    std::swap(prevGray, gray);
    return true;
}


#if (!TARGET_IPHONE_SIMULATOR)
namespace neon
{
    static void neon_asm_convert(uint8_t * __restrict dest, uint8_t * __restrict src, int numPixels)
    {
        __asm__ volatile("lsr          %2, %2, #3      \n"
                         "# build the three constants: \n"
                         "mov         r4, #28          \n" // Blue channel multiplier
                         "mov         r5, #151         \n" // Green channel multiplier
                         "mov         r6, #77          \n" // Red channel multiplier
                         "vdup.8      d4, r4           \n"
                         "vdup.8      d5, r5           \n"
                         "vdup.8      d6, r6           \n"
                         "0:						   \n"
                         "# load 8 pixels:             \n"
                         "vld4.8      {d0-d3}, [%1]!   \n"
                         "# do the weight average:     \n"
                         "vmull.u8    q7, d0, d4       \n"
                         "vmlal.u8    q7, d1, d5       \n"
                         "vmlal.u8    q7, d2, d6       \n"
                         "# shift and store:           \n"
                         "vshrn.u16   d7, q7, #8       \n" // Divide q3 by 256 and store in the d7
                         "vst1.8      {d7}, [%0]!      \n"
                         "subs        %2, %2, #1       \n" // Decrement iteration count
                         "bne         0b            \n" // Repeat unil iteration count is not zero
                         :
                         : "r"(dest), "r"(src), "r"(numPixels)
                         : "r4", "r5", "r6"
                         );
    }
    
    static void cvtBgra2Gray(const cv::Mat& input, cv::Mat& gray)
    {
        if (gray.rows != input.rows || gray.cols != input.cols || gray.type() != CV_8UC1)
        {
            gray.create(input.rows, input.cols, CV_8UC1);
        }
        
        uint8_t * __restrict dest = gray.data;
        uint8_t * __restrict src  = input.data;
        int numPixels = input.rows * input.cols;
        
        neon_asm_convert(dest, src, numPixels);
    }
}
#endif

void ContourDetectionSample::getGray(const cv::Mat& input, cv::Mat& gray)
{
    const int numChannes = input.channels();
    
    if (numChannes == 4)
    {
#if TARGET_IPHONE_SIMULATOR
        cv::cvtColor(input, gray, CV_BGRA2GRAY);
#else
        neon::cvtBgra2Gray(input, gray);
#endif
        
    }
    else if (numChannes == 3)
    {
        cv::cvtColor(input, gray, CV_BGR2GRAY);
    }
    else if (numChannes == 1)
    {
        gray = input;
    }
}
