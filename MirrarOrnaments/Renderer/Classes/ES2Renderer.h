#import "ESRenderer.h"

#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <QuartzCore/QuartzCore.h>
#import <Accelerate/Accelerate.h>
#import "ComputePoints.h"
#import "GPUImage.h"
#import "ContourDetectionSample.h"
//#import "DisplayViewController.h"

@class PVRTexture;



@interface ES2Renderer : NSObject <ESRenderer, GPUImageTextureOutputDelegate, VideoInputProtocol>
{
@private
    EAGLContext *context;

	GLuint textureForCubeFace, outputTexture, jewelTexture;
    
    // The pixel dimensions of the CAEAGLLayer
    GLint backingWidth;
    GLint backingHeight;

    // The OpenGL ES names for the framebuffer and renderbuffer used to render to this view
    GLuint defaultFramebuffer, colorRenderbuffer, depthBuffer;

	CATransform3D currentCalculatedMatrix;
    CATransform3D currentJewelryCalculateMatrix;
    GLuint program;
    
    GLubyte *rawData;
    
    
    GPUImageFilter *inputFilter, *outputFilter;
    GPUImageTextureOutput *textureOutput;
    GPUImageOutput<GPUImageInput> *cannyFilter;
    GPUImageOutput<GPUImageInput> *shiTomasiFilter;
    GPUImageOutput<GPUImageInput> *cropFilter;
    GPUImageCrosshairGenerator *crosshairGenerator;
    GPUImageAlphaBlendFilter *blendFilter;
    GPUImageGammaFilter *gammaFilter;
    GPUImageBrightnessFilter *brightnessFilter;
    
    
    
    GPUImageRawDataOutput *videoRawData;
    GPUImageRawDataInput *processedRawData;
    
    cv::Mat frame, outputFrame;
    cv::Rect facialRect;
    cv::Vec4f lineL;
    cv::Vec4f lineR;
    cv::Point2f pointL;
    cv::Point2f pointR;
    
    //Properties of image that will be provided
    float normHingeDistL;
    float normHingeDistR;
    
    cv::Vec2f imageHingePointsL;
    cv::Vec2f imageHingePointsR;
    
    float imageWidth;
    float imageHeight;
    
    //Calculated points
    float hingeDistL;
    float hingeDistR;
    
    cv::Vec2f hingePointL;
    cv::Vec2f hingePointR;
    
    float imageScaleFactor;
    
    float scaledImageWidth;
    float scaledImageHeight;
    
    
    //Points Calculated by OpenCV
    cv::Vec2f refPointL;
    cv::Vec2f refPointR;
    
    //Final points to be set
    float xScale;
    float yScale;
    
    float xTranslate;
    float yTranslate;
    
    
    
    UIView *faceView;
    CIDetector *faceDetector;
    CGRect faceRect;
    CGRect normFaceRect;
    CGRect tempFaceRect;
    float tempFaceCentPointX;
    float tempFaceCentPointY;
    
    BOOL faceThinking;
    BOOL videoMode;
    bool prevState;
    
    CGPoint leftEyePosition;
    CGPoint rightEyePosition;
    CGPoint mouthPosition;
    
    CGPoint neckArea[4];
    CGPoint facePositiongl[4];
    /*
     
       (4)  --------------   (1)
           |              |
           |              |
           |              |
           |              |
           |              |
           |              |
           |              |
       (3)  --------------   (2)
   
     */
    
    CGRect cropRect;
    CGPoint touchPoint;
    std::vector<cv::KeyPoint> keyPointsOut;
    GLfloat* cornerPoints;
    CGPoint corners;
    NSUInteger noCorners;
    
    dispatch_queue_t faceQueue;
    
    
    bool clicked;
    bool needToInit;
    CGPoint clickPointL;
    CGPoint clickPointR;
    CGPoint hingePointL_CV;
    CGPoint hingePointR_CV;
    
    bool passedClicked;
    bool passedNeedToInit;
    CGPoint passedClickPointL;
    CGPoint passedClickPointR;
    CGPoint passedHingePointL_CV;
    CGPoint passedHingePointR_CV;
    
    float tappedLeft[2];
    float tappedRight[2];
    
    CGRect roiRect;
    bool passedCorrectPoints, correctPoints, start, calc;
    int countFrame;
    CGSize videoPixelSize;
    
    CGPoint myClickL, myClickR;
    
@public
    GPUImageVideoCamera *videoCamera;
    
    GLfloat currentModelViewMatrix[16];
    GLfloat currentJewelryModelViewMatrix[16];

}

@property(readonly) GLuint outputTexture;
@property(nonatomic, copy) void(^newFrameAvailableBlock)(void);
@property(nonatomic,retain) CIDetector*faceDetector;
@property (strong, nonatomic) ComputePoints *computePoints;

- (id)initWithSize:(CGSize)newSize jewelryName:(NSString *)fileName leftPoint:(CGPoint)leftHingePoint rightPoint:(CGPoint)rightHingePoint;

- (void)renderByRotatingAroundX:(float)xRotation rotatingAroundY:(float)yRotation scale:(float)currentScale;
- (void)convert3DTransform:(CATransform3D *)transform3D toMatrix:(GLfloat *)matrix;
- (void)calculatePoints;
- (void)unNormalizePoints;
- (float)calcEucDistPoint1:(cv::Point2f)point1 Point2:(cv::Point2f)point2;
- (float)findNorm:(CGPoint)vec;
- (float)findNormCV:(cv::Vec2f)vecNorm;
- (void)updateModelViewMatrix;
- (void)updateCameraModelViewMatrix;

- (GLuint)setupTexture:(NSString *)fileName;


@end

