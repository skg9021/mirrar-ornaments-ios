#import "ES2Renderer.h"

// uniform index
enum {
    UNIFORM_MODELVIEWMATRIX,
    UNIFORM_TEXTURE,
    NUM_UNIFORMS
};
GLint uniforms[NUM_UNIFORMS];

// attribute index
enum {
    ATTRIB_VERTEX,
    ATTRIB_TEXTUREPOSITION,
    NUM_ATTRIBUTES
};

@interface ES2Renderer (PrivateMethods)
- (BOOL)loadShaders;
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;
@end

@implementation ES2Renderer

@synthesize outputTexture;
@synthesize newFrameAvailableBlock;
@synthesize faceDetector;
@synthesize computePoints;

- (id)initWithSize:(CGSize)newSize jewelryName:(NSString *)fileName leftPoint:(CGPoint)leftHingePoint rightPoint:(CGPoint)rightHingePoint;
{
    if ((self = [super init]))
    {
        // Need to use a share group based on the GPUImage context to share textures with the 3-D scene
        context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2 sharegroup:[[[GPUImageOpenGLESContext sharedImageProcessingOpenGLESContext] context] sharegroup]];

        if (!context || ![EAGLContext setCurrentContext:context] || ![self loadShaders])
        {
            //[self release];
            return nil;
        }
        
        backingWidth = (int)newSize.width;
        backingHeight = (int)newSize.height-88;
		
		currentCalculatedMatrix = CATransform3DIdentity;
        currentJewelryCalculateMatrix = CATransform3DIdentity;
        [self convert3DTransform:&currentJewelryCalculateMatrix toMatrix:currentJewelryModelViewMatrix];
		//currentCalculatedMatrix = CATransform3DScale(currentCalculatedMatrix, 0.5, 0.5 * (320.0/480.0), 0.5);
        //currentCalculatedMatrix = CATransform3DScale(currentCalculatedMatrix, 0.5, 0.5 * (320.0/480.0), 0.5);
        //currentCalculatedMatrix = CATransform3DScale(currentCalculatedMatrix, 1.0, 1.0 * (480.0/640.0), 1.0);
        
        glActiveTexture(GL_TEXTURE0);
        glGenTextures(1, &outputTexture);
        glBindTexture(GL_TEXTURE_2D, outputTexture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // This is necessary for non-power-of-two textures
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glBindTexture(GL_TEXTURE_2D, 0);

        glActiveTexture(GL_TEXTURE1);
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        glBindTexture(GL_TEXTURE_2D, outputTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, backingWidth, backingHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, outputTexture, 0);
        
//        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
//        
//        NSAssert(status == GL_FRAMEBUFFER_COMPLETE, @"Incomplete filter FBO: %d", status);
        glBindTexture(GL_TEXTURE_2D, 0);
        
        
        
/*
        videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionFront];
        videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
        
        shiTomasiFilter = [[GPUImageShiTomasiFeatureDetectionFilter alloc] init];
        [(GPUImageShiTomasiFeatureDetectionFilter *)shiTomasiFilter setThreshold:0.20];
        
        cropFilter = [[GPUImageCropFilter alloc] initWithCropRegion:CGRectMake(0.0, 0.0, 1.0, 1.0)];
 */
        
        
        textureOutput = [[GPUImageTextureOutput alloc] init];
        textureOutput.delegate = self;
        
        videoPixelSize = CGSizeMake(480.0, 640.0);
        
        rawData = (GLubyte*)calloc(videoPixelSize.width * videoPixelSize.height * 4, sizeof(GLubyte));
        processedRawData = [[GPUImageRawDataInput alloc] initWithBytes:rawData size:videoPixelSize];
        
        
/*
        
        //CGSize videoPixelSize = CGSizeMake(480.0, 640.0);
        CGSize videoPixelSize = CGSizeMake(240.0, 320.0);
                
        rawData = (GLubyte*)calloc(videoPixelSize.width * videoPixelSize.height * 4, sizeof(GLubyte));
        
        videoRawData = [[GPUImageRawDataOutput alloc] initWithImageSize:videoPixelSize resultsInBGRAFormat:YES];
        
        processedRawData = [[GPUImageRawDataInput alloc] initWithBytes:rawData size:videoPixelSize];
        
        ContourDetectionSample *detection = new ContourDetectionSample();
        
        pointL = cv::Point2f(0,0);

        
        crosshairGenerator = [[GPUImageCrosshairGenerator alloc] init];
        crosshairGenerator.crosshairWidth = 15.0;
        [crosshairGenerator forceProcessingAtSize:CGSizeMake(480.0, 640.0)];
        
        [(GPUImageShiTomasiFeatureDetectionFilter *)shiTomasiFilter setCornersDetectedBlock:^(GLfloat* cornerArray, NSUInteger cornersDetected, CMTime frameTime){
            [crosshairGenerator renderCrosshairsFromArray:cornerArray count:cornersDetected frameTime:frameTime];
        }];
        
        blendFilter = [[GPUImageAlphaBlendFilter alloc] init];
        [blendFilter forceProcessingAtSize:CGSizeMake(480.0, 640.0)];
        
        gammaFilter = [[GPUImageGammaFilter alloc] init];
        
        brightnessFilter = [[GPUImageBrightnessFilter alloc] init];
        [brightnessFilter forceProcessingAtSize:CGSizeMake(240.0, 320.0)];
*/
 /*        [videoCamera addTarget:shiTomasiFilter];
        [videoCamera addTarget:gammaFilter];
        [gammaFilter addTarget:blendFilter];
        [crosshairGenerator addTarget:blendFilter];
        [blendFilter addTarget:textureOutput];
        //[gammaFilter addTarget:textureOutput];
        //[videoCamera addTarget:textureOutput];
 
*/
//        [videoCamera addTarget:gammaFilter];
//        [gammaFilter addTarget:brightnessFilter];
//        [brightnessFilter addTarget:videoRawData];
//        [gammaFilter addTarget:textureOutput];
        //[gammaFilter addTarget:videoRawData];

/*
        NSDictionary *detectorOptions = [[NSDictionary alloc] initWithObjectsAndKeys:CIDetectorAccuracyLow, CIDetectorAccuracy, nil];
        self.faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:detectorOptions];
        faceThinking = NO;
        [videoCamera setDelegate:self];
        
        
        [videoCamera startCameraCapture];
        
        jewelTexture = [self setupTexture:@"silver_neck.png"];
        prevState = false;
        
        start = false;
        calc = false;
        
        countFrame = 0;
        
        faceQueue = dispatch_queue_create("com.yourcompany.CubeExample.fqueue", NULL);
        
*/
        imageHingePointsL = cv::Vec2f(leftHingePoint.x, leftHingePoint.y);
        imageHingePointsR = cv::Vec2f(rightHingePoint.x, rightHingePoint.y);
        
        jewelTexture = [self setupTexture:fileName];
       

    }

    return self;
}

- (void) videoInputReady
{
    faceRect = computePoints->faceRect;
    leftEyePosition = computePoints->leftEyePosition;
    rightEyePosition = computePoints->rightEyePosition;
    mouthPosition = computePoints->mouthPosition;
    tempFaceRect = computePoints->tempFaceRect;
    tempFaceCentPointX = computePoints->tempFaceCentPointX;
    tempFaceCentPointY = computePoints->tempFaceCentPointY;
    facePositiongl[0] = CGPointMake(computePoints->facePositiongl[0].x, computePoints->facePositiongl[0].y);
    facePositiongl[1] = CGPointMake(computePoints->facePositiongl[1].x, computePoints->facePositiongl[1].y);
    facePositiongl[2] = CGPointMake(computePoints->facePositiongl[2].x, computePoints->facePositiongl[2].y);
    facePositiongl[3] = CGPointMake(computePoints->facePositiongl[3].x, computePoints->facePositiongl[3].y);
    roiRect = CGRect(computePoints->roiRect);
    start = computePoints->start;
    
    hingePointL_CV = CGPoint(computePoints->hingePointL_CV);
    hingePointR_CV = CGPoint(computePoints->hingePointR_CV);
    //NSLog(@"HingePoints Left:%f \t %f",hingePointL_CV.x,hingePointL_CV.y);
    //NSLog(@"HingePoints Right:%f \t %f",hingePointR_CV.x,hingePointR_CV.y);
    
    countFrame = computePoints->countFrame;
    
    myClickL = CGPoint(computePoints->myClickL);
    myClickR = CGPoint(computePoints->myClickR);
    
    rawData = computePoints->rawOutputData;
    [processedRawData updateDataFromBytes:rawData size:videoPixelSize];
    [processedRawData addTarget:textureOutput];
    [processedRawData processData];
}

- (GLuint)setupTexture:(NSString *)fileName {
    
    normHingeDistL = 0.220183f;
    normHingeDistR = 0.159325f;
    
    //imageHingePointsL = cv::Vec2f(6,19);      //(50,64);       //(54, 42); // 82, 20);
    //imageHingePointsR = cv::Vec2f(376,19);         //(446,64);      //(507, 18); //582, 20);
    
    // 1
    NSData *thumbnail = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:fileName]];
    CGImageRef spriteImage = [UIImage imageWithData:thumbnail].CGImage;
    if (!spriteImage) {
        NSLog(@"Failed to load image %@", fileName);
        exit(1);
    }
    
    // 2
    size_t width = CGImageGetWidth(spriteImage);
    size_t height = CGImageGetHeight(spriteImage);
    
    imageWidth = width;
    imageHeight = height;
    scaledImageWidth = width;
    scaledImageHeight = height;
    
    GLubyte * spriteData = (GLubyte *) calloc(width*height*4, sizeof(GLubyte));
    
    CGContextRef spriteContext = CGBitmapContextCreate(spriteData, width, height, 8, width*4, CGImageGetColorSpace(spriteImage), kCGImageAlphaPremultipliedLast);
    
    // 3
    CGContextDrawImage(spriteContext, CGRectMake(0, 0, width, height), spriteImage);
    
    CGContextRelease(spriteContext);
    
    // 4
    GLuint texName;
    glGenTextures(1, &texName);
    glBindTexture(GL_TEXTURE_2D, texName);
    
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, spriteData);
    
    free(spriteData);
    return texName;
    
}

#pragma UI

- (void) touchedAtY:(NSNumber *)value
{
    touchPoint.y = [value floatValue];
}

- (void) touchedAtX:(NSNumber *)value
{
    touchPoint.x = [value floatValue];
}

- (void) imageSwitchChanged:(NSNumber *)value
{
    if([value intValue] == 1)
        videoMode = YES;
    else if([value intValue] == 0)
        videoMode = NO;
}




#pragma Renderer

- (float)calcEucDistPoint1:(cv::Point2f)point1 Point2:(cv::Point2f)point2
{
    return sqrt((point2.x-point1.x)*(point2.x-point1.x) + (point2.y-point1.y)*(point2.y-point1.y));
}

- (float)findNorm:(CGPoint)vec
{
    return sqrt(vec.x*vec.x + vec.y*vec.y);
}

- (float)findNormCV:(cv::Vec2f)vecNorm
{
    return sqrt(vecNorm[0]*vecNorm[0] + vecNorm[1]*vecNorm[1]);
}

- (void)unNormalizePoints
{
    lineL[0] *= backingWidth;
    lineL[1] *= backingHeight;
    lineL[2] *= backingWidth;
    lineL[3] *= backingHeight;
    
    lineR[0] *= backingWidth;
    lineR[1] *= backingHeight;
    lineR[2] *= backingWidth;
    lineR[3] *= backingHeight;
    
    pointL.x *= backingWidth;
    pointL.y *= backingHeight;
    
    pointR.x *= backingWidth;
    pointR.y *= backingHeight;

    hingePointL_CV.x *= backingWidth;
    hingePointL_CV.y *= backingHeight;
    
    hingePointR_CV.x *= backingWidth;
    hingePointR_CV.y *= backingHeight;
}

- (void)calculatePoints
{
    [self unNormalizePoints];
    
    /*
    cv::Vec2f dirVectorL= cv::Vec2f(lineL[2]-pointL.x, lineL[3]-pointL.y);
    dirVectorL = dirVectorL/[self findNorm:dirVectorL]; //cv::norm(dirVectorL,cv::NORM_L2);
    
    cv::Vec2f dirVectorR= cv::Vec2f(lineR[2]-pointR.x, lineR[3]-pointR.y);
    dirVectorR = dirVectorR/[self findNorm:dirVectorR]; //cv::norm(dirVectorR,cv::NORM_L2);

    
    float distanceBwPoints = [self calcEucDistPoint1:pointL Point2:pointR];
    hingeDistL = normHingeDistL * distanceBwPoints;
    hingeDistR = normHingeDistR * distanceBwPoints;
     
    hingePointL = cv::Vec2f(pointL.x,pointL.y) + hingeDistL * dirVectorL;
    hingePointR = cv::Vec2f(pointR.x,pointR.y) + hingeDistR * dirVectorR;
    
    float distHingePoints = [self findNorm:hingePointR - hingePointL]; //cv::norm(hingePointR - hingePointL, cv::NORM_L2);
    
    float distImageHIngePoints = [self findNorm:imageHingePointsR - imageHingePointsL];   //cv::norm(imageHingePointsR - imageHingePointsL, cv::NORM_L2);
    
    imageScaleFactor = distHingePoints / distImageHIngePoints;
    
    xScale = imageScaleFactor;
    yScale = imageScaleFactor;
    
    refPointL = cv::Vec2f(pointL.x/backingWidth, pointL.y/backingHeight);
    refPointR = cv::Vec2f(pointR.x/backingWidth, pointR.y/backingHeight);
    
    float scaledImageWidth = xScale * imageWidth;
    float scaledImageHeight = yScale * imageHeight;
    
    xTranslate = hingePointL[0] + imageHingePointsL[0] - scaledImageWidth/2;
    yTranslate = hingePointL[1] - imageHingePointsL[1] + scaledImageHeight/2;
    */
    
    float distHingePoints = [self findNorm:CGPointMake(hingePointR_CV.x - hingePointL_CV.x, hingePointR_CV.y - hingePointL_CV.y)];
    
    float distImageHIngePoints = [self findNormCV:imageHingePointsR - imageHingePointsL];
    
    imageScaleFactor = distHingePoints / distImageHIngePoints;
    
    //NSLog(@"distHingePoints:%f",distHingePoints);
    
    xScale = imageScaleFactor;
    yScale = imageScaleFactor;

    scaledImageWidth = xScale * imageWidth;
    scaledImageHeight = yScale * imageHeight;
    //NSLog(@"HingePointsDistance %f",distHingePoints);
    //NSLog(@"ScaledImageWidth %f", scaledImageWidth);
    //xTranslate = hingePointL_CV.x + imageHingePointsL[0]*xScale - scaledImageWidth/2;
    //yTranslate = hingePointL_CV.y - imageHingePointsL[1] + scaledImageHeight/2;
    xTranslate = hingePointL_CV.x /*- (imageHingePointsL[0]/imageWidth)*distHingePoints*/ - distHingePoints/2;//scaledImageWidth/2;
    yTranslate = hingePointL_CV.y - imageHingePointsL[1] + scaledImageHeight/2 + imageHingePointsL[1]*0.5;


}

-(void)updateModelViewMatrix
{
    /*
    currentModelViewMatrix[1] = 0;
    currentModelViewMatrix[4] = 0;
    
    currentModelViewMatrix[0] = (countFrame*currentModelViewMatrix[0] + xScale)/(countFrame + 1);
    currentModelViewMatrix[5] = (countFrame*currentModelViewMatrix[5] + yScale)/(countFrame + 1);
    currentModelViewMatrix[12] = (1 - xTranslate/backingWidth)*2 -1;
    currentModelViewMatrix[13] = (yTranslate/backingHeight)*2 -1;
    */
    
    currentJewelryModelViewMatrix[1] = 0;
    currentJewelryModelViewMatrix[4] = 0;
    
    float faceWidth = (facePositiongl[0].x - facePositiongl[3].x)*1536;
    float maxScale = faceWidth/imageWidth;
    
    //NSLog(@"currentmatrixscale:%f",currentJewelryModelViewMatrix[0]);
    //NSLog(@"xscale:%f",xScale);
    //NSLog(@"maxscale:%f",maxScale);
    
    float scaleInX = (countFrame*currentJewelryModelViewMatrix[0] + xScale)/(countFrame + 1);
    float scaleInY = (countFrame*currentJewelryModelViewMatrix[5] + yScale)/(countFrame + 1);
    if (scaleInX>maxScale) {
        scaleInX = maxScale;
    }
    if (scaleInY>maxScale) {
        scaleInY = maxScale;
    }
    currentJewelryModelViewMatrix[0] = scaleInX;
    currentJewelryModelViewMatrix[5] = scaleInY;
    
    //NSLog(@"Scale from Renderer:%f",currentJewelryModelViewMatrix[0]);
    
    if (currentJewelryModelViewMatrix[0]!=currentJewelryModelViewMatrix[0] || currentJewelryModelViewMatrix[5]!=currentJewelryModelViewMatrix[5]) {
        currentJewelryModelViewMatrix[0] = 1;
        currentJewelryModelViewMatrix[5] = 1;
    }
    
    currentJewelryModelViewMatrix[12] = (1 - xTranslate/backingWidth)*2 -1;
    currentJewelryModelViewMatrix[13] = (yTranslate/backingHeight)*2 -1;

    // Bounds in Position
    if(currentJewelryModelViewMatrix[12]>0.95)
        currentJewelryModelViewMatrix[12] = 0.95;
    else if (currentJewelryModelViewMatrix[12]<-0.95)
        currentJewelryModelViewMatrix[12] = -0.95;
    
    if(currentJewelryModelViewMatrix[13]>0.95)
        currentJewelryModelViewMatrix[13] = 0.95;
    else if (currentJewelryModelViewMatrix[13]<-0.95)
        currentJewelryModelViewMatrix[13] = -0.95;
    
}

- (void)updateCameraModelViewMatrix
{
    currentModelViewMatrix[0] = 0;
    currentModelViewMatrix[1] = -1;
    currentModelViewMatrix[4] = 1;
    currentModelViewMatrix[5] = 0;
}

- (void)renderByRotatingAroundX:(float)xRotation rotatingAroundY:(float)yRotation scale:(float)currentScale;
{
    static const GLfloat cubeVertices[] = {
 
        -1.0, -1.0,  0.7, // 4
        1.0, -1.0,  0.7, // 5
        1.0,  1.0,  0.7, // 6
        
        1.0,  1.0,  0.7, // 6
        -1.0,  1.0,  0.7,  // 7
        -1.0, -1.0,  0.7 // 4
        
    };  

	const GLfloat cubeTexCoords[] = {
/*
        0.0, 0.0,
        0.0, 1.0,
        1.0, 1.0,
        
        1.0, 1.0,
        1.0, 0.0,
        0.0, 0.0
*/
        

        1.0, 0.0,
        0.0, 0.0,
        0.0, 1.0,
        
        0.0, 1.0,
        1.0, 1.0,
        1.0, 0.0

    };

    const GLfloat squareBigVertices[] = {
    
        -imageWidth/(backingWidth),-imageHeight/(backingWidth),0.7,
        imageWidth/(backingWidth),-imageHeight/(backingWidth),0.7,
        imageWidth/(backingWidth),imageHeight/(backingWidth),0.7,
        
        imageWidth/(backingWidth),imageHeight/(backingWidth),0.7,
        -imageWidth/(backingWidth),imageHeight/(backingWidth),0.7,
        -imageWidth/(backingWidth),-imageHeight/(backingWidth),0.7
        /*
        -scaledImageWidth/(2*backingWidth), -scaledImageHeight/(2*backingWidth),0.5,
        scaledImageWidth/(2*backingWidth),-scaledImageHeight/(2*backingWidth),0.5,
        scaledImageWidth/(2*backingWidth),scaledImageHeight/(2*backingWidth),0.5,
        
        scaledImageWidth/(2*backingWidth),scaledImageHeight/(2*backingWidth),0.5,
        -scaledImageWidth/(2*backingWidth),scaledImageHeight/(2*backingWidth),0.5,
        -scaledImageWidth/(2*backingWidth),-scaledImageHeight/(2*backingWidth),0.5
        
        -0.5,-0.7,0.5,
        0.5,-0.7,0.5,
        0.5,0.7,0.5,
        
        0.5,0.7,0.5,
        -0.5,0.7,0.5,
        -0.5,-0.7,0.5
     
        
        -imageWidth/(2*backingWidth),-imageHeight/(2*backingWidth),0.5,
        imageWidth/(2*backingWidth),-imageHeight/(2*backingWidth),0.5,
        imageWidth/(2*backingWidth),imageHeight/(2*backingWidth),0.5,
        
        imageWidth/(2*backingWidth),imageHeight/(2*backingWidth),0.5,
        -imageWidth/(2*backingWidth),imageHeight/(2*backingWidth),0.5,
        -imageWidth/(2*backingWidth),-imageHeight/(2*backingWidth),0.5
      
        -0.2, -0.2,  0.5, // 4
        0.2, -0.2,  0.5, // 5
        0.2,  0.2,  0.5, // 6
        
        0.2,  0.2,  0.5, // 6
        -0.2,  0.2,  0.5,  // 7
        -0.2, -0.2,  0.5 // 4
        
      */
        
    };
    const GLfloat squareBigTexCords[] = {
        
        1.0, 0.0,
        0.0, 0.0,
        0.0, 1.0,
        
        0.0, 1.0,
        1.0, 1.0,
        1.0, 0.0
        
    };
    
    const GLfloat smallSquare[] = {
        
        -0.02, -0.02,  0.7,
        0.02, -0.02,  0.7,
        0.02,  0.02,  0.7,
            
        0.02,  0.02,  0.7,
        -0.02,  0.02,  0.7,
        -0.02, -0.02,  0.7
            
    };
    
    const GLfloat verySmallSquare[] = {
        
        -0.05, -0.05,  0.7,
        0.05, -0.05,  0.7,
        0.05,  0.05,  0.7,
        
        0.05,  0.05,  0.7,
        -0.05,  0.05,  0.7,
        -0.05, -0.05,  0.7
        
    };
    
    [EAGLContext setCurrentContext:context];

    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
	
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

    glViewport(0, 0, backingWidth, backingHeight);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
	
    glUseProgram(program);	

/*
	// Perform incremental rotation based on current angles in X and Y	
	if ((xRotation != 0.0) || (yRotation != 0.0))
	{
		GLfloat totalRotation = sqrt(xRotation*xRotation + yRotation*yRotation);
		
        CATransform3D tempMatrix = CATransform3DTranslate(currentCalculatedMatrix, xRotation, yRotation, 0);
        
		//CATransform3D temporaryMatrix = CATransform3DRotate(currentCalculatedMatrix, totalRotation * M_PI / 180.0,
		//													((xRotation/totalRotation) * currentCalculatedMatrix.m12 + (yRotation/totalRotation) * currentCalculatedMatrix.m11),
		//													((xRotation/totalRotation) * currentCalculatedMatrix.m22 + (yRotation/totalRotation) * currentCalculatedMatrix.m21),
		//													((xRotation/totalRotation) * currentCalculatedMatrix.m32 + (yRotation/totalRotation) * currentCalculatedMatrix.m31));
		//if ((temporaryMatrix.m11 >= -100.0) && (temporaryMatrix.m11 <= 100.0))
		//	currentCalculatedMatrix = temporaryMatrix;
        if ((tempMatrix.m11 >= -100.0) && (tempMatrix.m11 <= 100.0))
			currentCalculatedMatrix = tempMatrix;
	}
	else
	{
	}
	
*/
	

	[self convert3DTransform:&currentCalculatedMatrix toMatrix:currentModelViewMatrix];
    
    //[self calculatePoints];
    
////// Draw the Camera video /////////////////
    glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, textureForCubeFace);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
//    if (videoMode == YES) {
        //[self updateCameraModelViewMatrix];
//    }
    //[self updateCameraModelViewMatrix];
    
    // Update uniform value
	glUniform1i(uniforms[UNIFORM_TEXTURE], 4);
	glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWMATRIX], 1, 0, currentModelViewMatrix);

    // Update attribute values
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, cubeVertices);
    glEnableVertexAttribArray(ATTRIB_VERTEX);
	glVertexAttribPointer(ATTRIB_TEXTUREPOSITION, 2, GL_FLOAT, 0, 0, cubeTexCoords);
	glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITION);

	glDrawArrays(GL_TRIANGLES, 0, 6);
    
    
///////// Draw a jewelery image ///////////////
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, jewelTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
    // Perform incremental rotation based on current angles in X and Y
	if ((xRotation != 0.0) || (yRotation != 0.0) || (currentScale != 1.0))
	{
    
        //[self convert3DTransform:&currentJewelryCalculateMatrix toMatrix:currentJewelryModelViewMatrix];
        
        if(currentScale!=1.0)/*currentJewelryModelViewMatrix[0]!=currentScale)*/
        {
            currentJewelryModelViewMatrix[0] += currentScale;
            currentJewelryModelViewMatrix[5] += currentScale;
            //NSLog(@"Current Scale from Renderer : %f",currentJewelryModelViewMatrix[0]);
        }
        
        // Bounds on Scaling
        if (currentJewelryModelViewMatrix[0]>1.8) {
            currentJewelryModelViewMatrix[0] = 1.8;
            currentJewelryModelViewMatrix[5] = 1.8;
        }
        else if (currentJewelryModelViewMatrix[0]<0.3) {
            currentJewelryModelViewMatrix[0] = 0.3;
            currentJewelryModelViewMatrix[5] = 0.3;
        }
        
        currentJewelryModelViewMatrix[12] += ((xRotation*2)/1536)*2;
        currentJewelryModelViewMatrix[13] += ((yRotation*2)/2048)*2;
        
        //NSLog(@"Position from Renderer window X:%f \tY:%f",currentJewelryModelViewMatrix[12],currentJewelryModelViewMatrix[13]);
    
        // Bounds on Position
        if(currentJewelryModelViewMatrix[12]>1.0)
            currentJewelryModelViewMatrix[12] = 1.0;
        else if (currentJewelryModelViewMatrix[12]<-1.0)
            currentJewelryModelViewMatrix[12] = -1.0;
        
    
        if(currentJewelryModelViewMatrix[13]>1.0)
            currentJewelryModelViewMatrix[13] = 1.0;
        else if (currentJewelryModelViewMatrix[13]<-1.0)
            currentJewelryModelViewMatrix[13] = -1.0;
	}
	else
	{
        if (start) {
            //Update Model View Matrix
            [self calculatePoints];
            [self updateModelViewMatrix];
        }
        
	}
    
    
    // Update uniform value
	glUniform1i(uniforms[UNIFORM_TEXTURE], 4);
	glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWMATRIX], 1, 0, currentJewelryModelViewMatrix);
    

	glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, squareBigVertices);
	glEnableVertexAttribArray(ATTRIB_VERTEX);
	glVertexAttribPointer(ATTRIB_TEXTUREPOSITION, 2, GL_FLOAT, 0, 0, squareBigTexCords);
	glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITION);
    
    glDrawArrays(GL_TRIANGLES, 0, 6);

////////// Draw two Squares at the two points ///////////////
    currentModelViewMatrix[0] = 1;
    currentModelViewMatrix[5] = 1;
    
                    //Draw on Left Eye
    currentModelViewMatrix[12] = (1 - leftEyePosition.x) *2 -1;
    currentModelViewMatrix[13] = leftEyePosition.y *2 -1;
    // Update uniform value
	glUniform1i(uniforms[UNIFORM_TEXTURE], 2);
	glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWMATRIX], 1, 0, currentModelViewMatrix);
    
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, verySmallSquare);
	glEnableVertexAttribArray(ATTRIB_VERTEX);
	glVertexAttribPointer(ATTRIB_TEXTUREPOSITION, 2, GL_FLOAT, 0, 0, squareBigTexCords);
	glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITION);
	
    //glDrawArrays(GL_TRIANGLES, 0, 6);
    
                    //Draw on Right Eye
    currentModelViewMatrix[12] = (1 - rightEyePosition.x) *2 -1;
    currentModelViewMatrix[13] = rightEyePosition.y *2 -1;
    // Update uniform value
	glUniform1i(uniforms[UNIFORM_TEXTURE], 2);
	glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWMATRIX], 1, 0, currentModelViewMatrix);
    
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, verySmallSquare);
	glEnableVertexAttribArray(ATTRIB_VERTEX);
	glVertexAttribPointer(ATTRIB_TEXTUREPOSITION, 2, GL_FLOAT, 0, 0, squareBigTexCords);
	glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITION);
	
    //glDrawArrays(GL_TRIANGLES, 0, 6);
    
                        //Draw on Mouth
    currentModelViewMatrix[12] = (1 - mouthPosition.x) *2 -1;
    currentModelViewMatrix[13] = mouthPosition.y *2 -1;
    // Update uniform value
	glUniform1i(uniforms[UNIFORM_TEXTURE], 2);
	glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWMATRIX], 1, 0, currentModelViewMatrix);
    
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, verySmallSquare);
	glEnableVertexAttribArray(ATTRIB_VERTEX);
	glVertexAttribPointer(ATTRIB_TEXTUREPOSITION, 2, GL_FLOAT, 0, 0, squareBigTexCords);
	glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITION);
	
    //glDrawArrays(GL_TRIANGLES, 0, 6);
    
 
    //Draw on Face upper right corner
    
    currentModelViewMatrix[12] = facePositiongl[0].x;
    currentModelViewMatrix[13] = facePositiongl[0].y;
    
    // Update uniform value
	glUniform1i(uniforms[UNIFORM_TEXTURE], 2);
	glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWMATRIX], 1, 0, currentModelViewMatrix);
    
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, verySmallSquare);
	glEnableVertexAttribArray(ATTRIB_VERTEX);
	glVertexAttribPointer(ATTRIB_TEXTUREPOSITION, 2, GL_FLOAT, 0, 0, squareBigTexCords);
	glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITION);
	
    //glDrawArrays(GL_TRIANGLES, 0, 6);

    
    //Draw on Face right bottom corner
    currentModelViewMatrix[12] = facePositiongl[1].x;
    currentModelViewMatrix[13] = facePositiongl[1].y;
    
    // Update uniform value
	glUniform1i(uniforms[UNIFORM_TEXTURE], 2);
	glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWMATRIX], 1, 0, currentModelViewMatrix);
    
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, verySmallSquare);
	glEnableVertexAttribArray(ATTRIB_VERTEX);
	glVertexAttribPointer(ATTRIB_TEXTUREPOSITION, 2, GL_FLOAT, 0, 0, squareBigTexCords);
	glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITION);
	
    //glDrawArrays(GL_TRIANGLES, 0, 6);

   
    //Draw on Face left bottom corner
    currentModelViewMatrix[12] = facePositiongl[2].x;
    currentModelViewMatrix[13] = facePositiongl[2].y;
    
    // Update uniform value
	glUniform1i(uniforms[UNIFORM_TEXTURE], 2);
	glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWMATRIX], 1, 0, currentModelViewMatrix);
    
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, verySmallSquare);
	glEnableVertexAttribArray(ATTRIB_VERTEX);
	glVertexAttribPointer(ATTRIB_TEXTUREPOSITION, 2, GL_FLOAT, 0, 0, squareBigTexCords);
	glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITION);
	
    //glDrawArrays(GL_TRIANGLES, 0, 6);

    
    //Draw on Face left upper corner
    currentModelViewMatrix[12] = facePositiongl[3].x;
    currentModelViewMatrix[13] = facePositiongl[3].y;
    
    // Update uniform value
	glUniform1i(uniforms[UNIFORM_TEXTURE], 2);
	glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWMATRIX], 1, 0, currentModelViewMatrix);
    
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, verySmallSquare);
	glEnableVertexAttribArray(ATTRIB_VERTEX);
	glVertexAttribPointer(ATTRIB_TEXTUREPOSITION, 2, GL_FLOAT, 0, 0, squareBigTexCords);
	glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITION);
	
    //glDrawArrays(GL_TRIANGLES, 0, 6);

    hingePointL_CV = CGPointMake(hingePointL_CV.x/backingWidth, hingePointL_CV.y/backingHeight);
    hingePointR_CV = CGPointMake(hingePointR_CV.x/backingWidth, hingePointR_CV.y/backingHeight);
    
    //Draw Left point
    currentModelViewMatrix[12] = (1-hingePointL_CV.x)*2-1;
    currentModelViewMatrix[13] = hingePointL_CV.y*2-1;
    glUniform1i(uniforms[UNIFORM_TEXTURE], 2);
	glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWMATRIX], 1, 0, currentModelViewMatrix);
    
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, smallSquare);
	glEnableVertexAttribArray(ATTRIB_VERTEX);
	glVertexAttribPointer(ATTRIB_TEXTUREPOSITION, 2, GL_FLOAT, 0, 0, squareBigTexCords);
	glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITION);
	
    //glDrawArrays(GL_TRIANGLES, 0, 6);

    //Draw Right Point
    currentModelViewMatrix[12] = (1-hingePointR_CV.x)*2-1;
    currentModelViewMatrix[13] = hingePointR_CV.y*2-1;
    glUniform1i(uniforms[UNIFORM_TEXTURE], 2);
	glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWMATRIX], 1, 0, currentModelViewMatrix);
    
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, smallSquare);
	glEnableVertexAttribArray(ATTRIB_VERTEX);
	glVertexAttribPointer(ATTRIB_TEXTUREPOSITION, 2, GL_FLOAT, 0, 0, squareBigTexCords);
	glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITION);
	
    //glDrawArrays(GL_TRIANGLES, 0, 6);
    
    
    // Draw myclick
    currentModelViewMatrix[12] = (myClickL.x)*2-1;
    currentModelViewMatrix[13] = myClickL.y*2-1;
    glUniform1i(uniforms[UNIFORM_TEXTURE], 2);
	glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWMATRIX], 1, 0, currentModelViewMatrix);
    
    glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, 0, 0, smallSquare);
	glEnableVertexAttribArray(ATTRIB_VERTEX);
	glVertexAttribPointer(ATTRIB_TEXTUREPOSITION, 2, GL_FLOAT, 0, 0, squareBigTexCords);
	glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITION);
	
    //glDrawArrays(GL_TRIANGLES, 0, 6);
    
    
    // The flush is required at the end here to make sure the FBO texture is written to before passing it back to GPUImage
    glFlush();
    
	newFrameAvailableBlock();
}

#pragma mark-
#pragma Shader compile and link

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file
{
    GLint status;
    const GLchar *source;

    source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source)
    {
        NSLog(@"Failed to load vertex shader");
        return FALSE;
    }

    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);

#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif

    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0)
    {
        glDeleteShader(*shader);
        return FALSE;
    }

    return TRUE;
}

- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;

    glLinkProgram(prog);

#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif

    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0)
        return FALSE;

    return TRUE;
}

- (BOOL)validateProgram:(GLuint)prog
{
    GLint logLength, status;

    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }

    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0)
        return FALSE;

    return TRUE;
}

- (BOOL)loadShaders
{
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;

    // Create shader program
    program = glCreateProgram();

    // Create and compile vertex shader
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname])
    {
        NSLog(@"Failed to compile vertex shader");
        return FALSE;
    }

    // Create and compile fragment shader
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname])
    {
        NSLog(@"Failed to compile fragment shader");
        return FALSE;
    }

    // Attach vertex shader to program
    glAttachShader(program, vertShader);

    // Attach fragment shader to program
    glAttachShader(program, fragShader);

    // Bind attribute locations
    // this needs to be done prior to linking
    glBindAttribLocation(program, ATTRIB_VERTEX, "position");
    glBindAttribLocation(program, ATTRIB_TEXTUREPOSITION, "inputTextureCoordinate");

    // Link program
    if (![self linkProgram:program])
    {
        NSLog(@"Failed to link program: %d", program);

        if (vertShader)
        {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader)
        {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (program)
        {
            glDeleteProgram(program);
            program = 0;
        }
        
        return FALSE;
    }

    // Get uniform locations
    uniforms[UNIFORM_MODELVIEWMATRIX] = glGetUniformLocation(program, "modelViewProjMatrix");
    uniforms[UNIFORM_TEXTURE] = glGetUniformLocation(program, "texture");

    // Release vertex and fragment shaders
    if (vertShader)
        glDeleteShader(vertShader);
    if (fragShader)
        glDeleteShader(fragShader);

    return TRUE;
}

- (void)dealloc
{
    // Tear down GL
    if (defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }

    if (colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }

    if (program)
    {
        glDeleteProgram(program);
        program = 0;
    }

    // Tear down context
    if ([EAGLContext currentContext] == context)
        [EAGLContext setCurrentContext:nil];

    //[context release];
    context = nil;

    //[super dealloc];
}

- (void)convert3DTransform:(CATransform3D *)transform3D toMatrix:(GLfloat *)matrix;
{
	//	struct CATransform3D
	//	{
	//		CGFloat m11, m12, m13, m14;
	//		CGFloat m21, m22, m23, m24;
	//		CGFloat m31, m32, m33, m34;
	//		CGFloat m41, m42, m43, m44;
	//	};
	
	matrix[0] = (GLfloat)transform3D->m11;
	matrix[1] = (GLfloat)transform3D->m12;
	matrix[2] = (GLfloat)transform3D->m13;
	matrix[3] = (GLfloat)transform3D->m14;
	matrix[4] = (GLfloat)transform3D->m21;
	matrix[5] = (GLfloat)transform3D->m22;
	matrix[6] = (GLfloat)transform3D->m23;
	matrix[7] = (GLfloat)transform3D->m24;
	matrix[8] = (GLfloat)transform3D->m31;
	matrix[9] = (GLfloat)transform3D->m32;
	matrix[10] = (GLfloat)transform3D->m33;
	matrix[11] = (GLfloat)transform3D->m34;
	matrix[12] = (GLfloat)transform3D->m41;
	matrix[13] = (GLfloat)transform3D->m42;
	matrix[14] = (GLfloat)transform3D->m43;
	matrix[15] = (GLfloat)transform3D->m44;
}

#pragma mark -
#pragma mark GPUImageTextureOutputDelegate delegate method

- (void)newFrameReadyFromTextureOutput:(GPUImageTextureOutput *)callbackTextureOutput;
{
    textureForCubeFace = callbackTextureOutput.texture;
    
    [self renderByRotatingAroundX:0.0 rotatingAroundY:0.0 scale:1.0];
}


@end
