//
//  MOFileSystemManager.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 15/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MOFileSystemManager : NSObject

@property (strong, nonatomic) NSFileManager *fileManager;
@property (strong, nonatomic) NSString      *storageDirectory;
@property (strong, nonatomic) NSString      *imageStorageDirectory;

- (id) init;
- (float) getFreeDiskSpaceInBytes;

- (NSString *) pathInStorageDirectory:(NSString *)key;
- (NSString *) pathInImageStorageDirectory:(NSString *)key;

// General File Functions
- (NSArray *) listContentsOfDirectoryAtPath:(NSString *)directoryPath;
- (BOOL) fileExistsInPath:(NSString *)path;
- (BOOL) deleteFileAtPath:(NSString *)path error:(NSError *)error;
- (BOOL) deleteDirectoryAtPath:(NSString *)path error:(NSError *)error;
- (BOOL) createDirectoryAtPath:(NSString *)path error:(NSError *)error;
- (BOOL) moveContentsOfDirectory:(NSString *)path toDirectory:(NSString *)path;

// Image Functions
- (void) storeImage: (UIImage *)image withName:(NSString*)name
                        ofType:(NSString *)type atPath:(NSString*)path;
- (UIImage *) retrieveImageWithName: (NSString *)name;
- (void) deleteImageWithName: (NSString *)name;

@end
