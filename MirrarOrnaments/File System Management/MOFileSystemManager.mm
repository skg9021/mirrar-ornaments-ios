//
//  MOFileSystemManager.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 15/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOFileSystemManager.h"

@implementation MOFileSystemManager

@synthesize fileManager;
@synthesize imageStorageDirectory;
@synthesize storageDirectory;

- (id) init
{
    if (self = [super init]) {
        [self setFileManager:[NSFileManager defaultManager]];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        [self setStorageDirectory:[documentsDirectory stringByAppendingPathComponent:@".MirrarOrnamentsData"]];
        [self setImageStorageDirectory:[self.storageDirectory stringByAppendingPathComponent:@"Images"]];
    }
    return self;
}

- (float) getFreeDiskSpaceInBytes
{
    float totalSpace = 0.0f;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [self.fileManager attributesOfFileSystemForPath:[paths lastObject] error: &error];
	
    if (dictionary)
	{
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemFreeSize];		//Or NSFileSystemSize for total size
        totalSpace = [fileSystemSizeInBytes floatValue];
    }
	else
	{
        NSLog(@"Error Obtaining File System Info: Domain = %@, Code = %d", [error domain], [error code]);
    }
	
    return totalSpace;
}

#pragma mark -
#pragma mark FILE FUNCTIONS
- (NSString *) pathInStorageDirectory:(NSString *)key
{
    return [[self.storageDirectory copy] stringByAppendingPathComponent:key];
}

- (NSString *) pathInImageStorageDirectory:(NSString *)key
{
    return [[self.imageStorageDirectory copy] stringByAppendingPathComponent:key];
}


- (NSArray *) listContentsOfDirectoryAtPath:(NSString *)directoryPath
{
    return [self.fileManager contentsOfDirectoryAtPath:directoryPath error:nil];
}

- (BOOL) fileExistsInPath:(NSString *)path
{
    return ([self.fileManager fileExistsAtPath:path]?YES:NO);
}

- (BOOL) deleteFileAtPath:(NSString *)path error:(NSError *)error
{
    if ([self fileExistsInPath:path]) {
        if (![self.fileManager removeItemAtPath:path error:&error]) {
            return NO;
        }
    }
    return YES;
}

- (BOOL) deleteDirectoryAtPath:(NSString *)path error:(NSError *)error
{
    if ([self fileExistsInPath:path]) {
        if (![self.fileManager removeItemAtPath:path error:&error]) {
            return NO;
        }
    }
    return YES;
}

- (BOOL) createDirectoryAtPath:(NSString *)path error:(NSError *)error
{
    if (![self fileExistsInPath:path]) {
        if ([self.fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil]) {
            return YES;
        }
        else
            return NO;
    }
    return YES;
}

- (BOOL) moveContentsOfDirectory:(NSString *)source toDirectory:(NSString *)dest
{
    NSArray *contents = [self listContentsOfDirectoryAtPath:source];
    for (NSString *filename in contents) {
        [self.fileManager moveItemAtPath:[[source copy] stringByAppendingPathComponent:filename] toPath:[[dest copy] stringByAppendingPathComponent:filename] error:nil];
    }
    return YES;
}

#pragma mark -
#pragma mark Image File Functions

- (void) storeImage:(UIImage *)image withName:(NSString *)name ofType:(NSString *)type atPath:(NSString*)path
{
    NSString *imagePath = [[path copy] stringByAppendingPathComponent:name];
    if ([type isEqualToString:@"png"]) {
        NSData *d = UIImagePNGRepresentation(image);
        [d writeToFile:imagePath atomically:YES];
    }
    else if ([type isEqualToString:@"jpg"])
    {
        NSData *d = UIImageJPEGRepresentation(image, 0.0);
        [d writeToFile:imagePath atomically:YES];
    }
}

- (UIImage *) retrieveImageWithName:(NSString *)name
{
    UIImage *storedImage = [UIImage imageWithContentsOfFile:
                            [self pathInImageStorageDirectory:name]];
    if (!storedImage) {
        NSLog(@"Error - Image not found: %@", [self pathInImageStorageDirectory:name]);
    }
    return storedImage;
}

- (void) deleteImageWithName:(NSString *)name
{
    NSString *path = [self pathInImageStorageDirectory:name];
    NSError *error;
    [self.fileManager removeItemAtPath:path error:&error];
}


@end
