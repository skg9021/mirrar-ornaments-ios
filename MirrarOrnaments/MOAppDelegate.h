//
//  MOAppDelegate.h
//  MirrarOrnaments
//
//  Created by skg on 9/19/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOUpdateManager.h"

@interface MOAppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate, MOUpdateManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MOUpdateManager *updateManager;
@property (strong, nonatomic) UIAlertView *updateAlertView;

@property (nonatomic, retain, readonly) NSManagedObjectContext          *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel            *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator    *persistentStoreCoordinator;
@property (nonatomic, retain, readonly) NSMutableDictionary             *menuCategorizationPredicates;
-(void)saveContext;
-(NSURL *)applicationDocumentsDirectory;
-(void) customizeAppearance;
@end
