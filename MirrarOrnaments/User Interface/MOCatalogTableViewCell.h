//
//  MOCatalogTableViewCell.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 20/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MOCatalogViewCellProtocol <NSObject>

- (void) selectedItemAtIndex:(int)index withIndexPath:(NSIndexPath*)indexPath;

@end

@interface MOCatalogTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIButton *ornament1;
@property (nonatomic, retain) IBOutlet UIButton *ornament2;
@property (nonatomic, retain) IBOutlet UIButton *ornament3;

@property (nonatomic, retain) NSIndexPath *indexPath;

@property (nonatomic, retain) id <MOCatalogViewCellProtocol> delegate;

- (IBAction)ornament1Clicked:(id)sender;
- (IBAction)ornament2Clicked:(id)sender;
- (IBAction)ornament3Clicked:(id)sender;

@end
