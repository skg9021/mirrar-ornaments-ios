//
//  TDSemiModalViewController.h
//  TDSemiModal
//
//  Created by Nathan  Reed on 18/10/10.
//  Copyright 2010 Nathan Reed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MOSemiModalViewController : UIViewController
{
    CGRect _rect;
}
@property (nonatomic, strong) UIView *coverView;

- (CGRect) rect;
- (void) setRect:(CGRect)rect;
@end
