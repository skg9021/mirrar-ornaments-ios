//
//  MOCartViewCell.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 24/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)


#import "MOCartViewCell.h"

@implementation MOCartViewCell
@synthesize detailImageView;
@synthesize captionText;
@synthesize description;
@synthesize tryButton;
@synthesize detailsButton;
@synthesize removeButton;

@synthesize delegate;
@synthesize details;
@synthesize jewelery;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) awakeFromNib
{
    details = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)tryButtonClicked:(id)sender {
}

- (IBAction)detailsButtonClicked:(id)sender {
    if (details) {
        [self.detailsButton setTitle:@"Details" forState:UIControlStateNormal];
        details=NO;
        [self.description setText:jewelery.jewelery_description];
    }
    else{
        [self.detailsButton setTitle:@"Info" forState:UIControlStateNormal];
        details=YES;
        [self.description setText:[NSString stringWithFormat:@"%@",jewelery.jewelery_price]];
    }
}

- (IBAction)removeButtonClicked:(id)sender {
    SAFE_PERFORM_WITH_ARG(delegate, @selector(removeButtonClicked:), self.indexPath);
}
@end
