//
//  MOCartViewController.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 24/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOCartViewController.h"
#import "MOCompareViewController.h"
#import "MOJewelery.h"


@interface MOCartViewController ()

@end

@implementation MOCartViewController
@synthesize shoppingCart = _shoppingCart;
@synthesize tableView = _tableView;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize selectedIndexes;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    selectedIndexes = [[NSMutableArray alloc] init];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_shoppingCart count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CartCell";
    MOCartViewCell *cell = (MOCartViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    MOJewelery *jewelery = [_shoppingCart objectAtIndex:[indexPath row]];
    NSString *imagePath = jewelery.jewelery_preview_image;
    imagePath = [imagePath stringByAppendingString:@"@2x.png"];
    NSData *thumbnail = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
    [cell.detailImageView setImage:[UIImage imageWithData:thumbnail]];
    [cell.captionText setText:jewelery.jewelery_caption_text];
    [cell.description setText:jewelery.jewelery_description];
    cell.delegate = self;
    cell.indexPath = indexPath;
    cell.jewelery = jewelery;
    // Configure the cell...
    if ([selectedIndexes containsObject:[NSNumber numberWithInt:indexPath.row]]) [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    if ([selectedCell accessoryType] == UITableViewCellAccessoryNone) {
        [selectedCell setAccessoryType:UITableViewCellAccessoryCheckmark];
        [selectedIndexes addObject:[NSNumber numberWithInt:indexPath.row]];
        NSLog(@"IVC selectedIndexes AddObject @ %d:", indexPath.row);
        NSLog(@"IVC selectedIndexes: %@", selectedIndexes);
    } else {
        [selectedCell setAccessoryType:UITableViewCellAccessoryNone];
        [selectedIndexes removeObject:[NSNumber numberWithInt:indexPath.row]];
        NSLog(@"IVC selectedIndexes RemoveObject @ %d:", indexPath.row);
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void) removeButtonClicked:(NSIndexPath *)indexPath
{
    [_shoppingCart removeObjectAtIndex:[indexPath row]];
    [selectedIndexes removeObject:[NSNumber numberWithInt:indexPath.row]];
    [_tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SplitViewSegue"]) {
        
        
        
        NSMutableArray *selectedJewelery = [[NSMutableArray alloc] init];
        for (NSNumber *index in selectedIndexes) {
            [selectedJewelery addObject:[_shoppingCart objectAtIndex:[index intValue]]];
        }
        
        MOCompareViewController *compareViewController = (MOCompareViewController *)[segue destinationViewController];
        //[compareViewController setComputePointsEngine:computePointsEngine];
        [compareViewController setJeweleryArray:selectedJewelery];
       
        NSLog(@"proceed");
    }
}
@end
