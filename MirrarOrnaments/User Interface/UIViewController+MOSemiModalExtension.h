//
//  UIViewController+TDSemiModalExtension.h
//  TDSemiModal
//
//  Created by Nathan  Reed on 18/10/10.
//  Copyright 2010 Nathan Reed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MOSemiModalViewController.h"

@interface UIViewController (MOSemiModalExtension)

-(void)presentSemiModalViewController:(MOSemiModalViewController*)vc;
-(void)dismissSemiModalViewController:(MOSemiModalViewController*)vc;
-(void)dismissSemiModalViewControllerEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;

@end
