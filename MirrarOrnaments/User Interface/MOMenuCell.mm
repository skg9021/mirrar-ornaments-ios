//
//  MOMenuCell.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 20/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOMenuCell.h"

@implementation MOMenuCell

@synthesize categoryDescription1;
@synthesize categoryButton1;
@synthesize categoryButton1Image;
@synthesize categoryDescription2;
@synthesize categoryButton2;
@synthesize categoryButton2Image;
@synthesize categoryImage1;
@synthesize categoryImage2;
@synthesize categoryName1;
@synthesize categoryName2;

@synthesize delegate;
@synthesize indexPath;

@synthesize button1Selected;
@synthesize button2Selected;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)highlightButton:(UIButton *)b {
    b.selected = YES;
    b.highlighted = YES;
}

- (void)unhighlightButton:(UIButton *)b {
    b.selected = NO;
    b.highlighted = NO;
}

- (IBAction)button1Clicked:(id)sender {
    [delegate selectedItemAtIndex:0 withIndexPath:indexPath];
//    UIButton *button = (UIButton*)sender;
//    if (!button.selected) {
//        [self performSelector:@selector(highlightButton:) withObject:button afterDelay:0.0];
//    }
//    else {
//        [self performSelector:@selector(unhighlightButton:) withObject:button afterDelay:0.0];
//    }
}

- (IBAction)button2Clicked:(id)sender {
    [delegate selectedItemAtIndex:1 withIndexPath:indexPath];
//    UIButton *button = (UIButton*)sender;
//    if (!button.selected) {
//        [self performSelector:@selector(highlightButton:) withObject:button afterDelay:0.0];
//    }
//    else {
//        [self performSelector:@selector(unhighlightButton:) withObject:button afterDelay:0.0];
//    }
}


@end
