//
//  MOCatalogViewController.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 18/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOCatalogViewController.h"
#import "MOCoreDataHelper.h"
#import "MOSemiModal.h"
#import "MOCartViewController.h"
#import "MOCalibrateViewController.h"

#import "DisplayViewController.h"

static int rowCount = 0;

@interface MOCatalogViewController ()

@end

@implementation MOCatalogViewController
@synthesize tableView = _tableView;

@synthesize managedObjectContext;
@synthesize jeweleryFetchedResultsController = _jeweleryFetchedResultsController;
@synthesize menuCategorizationPredicates = _menuCategorizationPredicates;
@synthesize userPreferencesDictionary = _userPreferencesDictionary;
@synthesize shoppingCart = _shoppingCart;
@synthesize cellIndexes = _cellIndexes;
@synthesize detailView;


- (void)viewDidLoad
{
    [super viewDidLoad];

    NSError *errorFetch;
    if (![[self jeweleryFetchedResultsController] performFetch:&errorFetch]) {
        abort();
    }
    
    self.title = @"Catalog";
    _cellIndexes = [[NSMutableDictionary alloc] init];
    _tableView.delegate = self;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.jeweleryFetchedResultsController = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    id  sectionInfo = [[_jeweleryFetchedResultsController sections] objectAtIndex:section];
    rowCount = [sectionInfo numberOfObjects];
    return rowCount/3 + (rowCount%3?1:0);
}

- (void)configureCell:(MOCatalogTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.delegate  = self;
    cell.indexPath = indexPath;
    MOJewelery *jewelery;
    NSInteger row;
    row = [indexPath row];
    cell.ornament1.enabled = NO;
    cell.ornament2.enabled = NO;
    cell.ornament3.enabled = NO;
    
    if (3*row+2 < rowCount) {
        jewelery = [_jeweleryFetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:3*[indexPath row] inSection:[indexPath section]]];
        if (jewelery != nil) {
            NSString *imagePath = jewelery.jewelery_preview_image;
            imagePath = [imagePath stringByAppendingString:@"@2x.png"];
            //NSLog(@"1. %@", imagePath);
            NSData *thumbnail = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
            [cell.ornament1 setImage:[UIImage imageWithData:thumbnail] forState:UIControlStateNormal];
            cell.ornament1.enabled = YES;
        }
        
        jewelery = [_jeweleryFetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:3*[indexPath row]+1 inSection:[indexPath section]]];
        if (jewelery != nil) {
            NSString *imagePath = jewelery.jewelery_preview_image;
            imagePath = [imagePath stringByAppendingString:@"@2x.png"];
            //NSLog(@"2. %@", imagePath);
            NSData *thumbnail = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
            [cell.ornament2 setImage:[UIImage imageWithData:thumbnail] forState:UIControlStateNormal];
            cell.ornament2.enabled = YES;
        }
        
        jewelery = [_jeweleryFetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:3*[indexPath row]+2 inSection:[indexPath section]]];
        if (jewelery != nil) {
            NSString *imagePath = jewelery.jewelery_preview_image;
            imagePath = [imagePath stringByAppendingString:@"@2x.png"];
            //NSLog(@"3. %@", imagePath);
            NSData *thumbnail = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
            [cell.ornament3 setImage:[UIImage imageWithData:thumbnail] forState:UIControlStateNormal];
            cell.ornament3.enabled = YES;
        }
        
    }
    else if (3*row + 1 < rowCount)
    {
        jewelery = [_jeweleryFetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:3*[indexPath row] inSection:[indexPath section]]];
        if (jewelery != nil) {
            NSString *imagePath = jewelery.jewelery_preview_image;
            imagePath = [imagePath stringByAppendingString:@"@2x.png"];
            NSData *thumbnail = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
            [cell.ornament1 setImage:[UIImage imageWithData:thumbnail] forState:UIControlStateNormal];
            cell.ornament1.enabled = YES;
        }
        
        jewelery = [_jeweleryFetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:3*[indexPath row]+1 inSection:[indexPath section]]];
        if (jewelery != nil) {
            NSString *imagePath = jewelery.jewelery_preview_image;
            imagePath = [imagePath stringByAppendingString:@"@2x.png"];
            NSData *thumbnail = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
            [cell.ornament2 setImage:[UIImage imageWithData:thumbnail] forState:UIControlStateNormal];
            cell.ornament2.enabled = YES;
        }
        
    }
    else
    {
        jewelery = [_jeweleryFetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:3*[indexPath row] inSection:[indexPath section]]];
        if (jewelery != nil) {
            NSString *imagePath = jewelery.jewelery_preview_image;
            imagePath = [imagePath stringByAppendingString:@"@2x.png"];
            NSData *thumbnail = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
            [cell.ornament1 setImage:[UIImage imageWithData:thumbnail] forState:UIControlStateNormal];
            cell.ornament1.enabled = YES;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CatalogCell";
    MOCatalogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MOCatalogTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    //DisplayViewController *rendererDisplay = [[DisplayViewController alloc] init];
    //[self.navigationController pushViewController:rendererDisplay animated:YES];
}


#pragma mark - Core Data Methods
- (NSArray *)menuCategorizationPredicates
{
    if (_menuCategorizationPredicates != nil) {
        return _menuCategorizationPredicates;
    }
    
    // Fetch the categorization predicates to fill the menu
    NSFetchRequest *fetchRequestCategorizationPredicates = [[NSFetchRequest alloc] init];
    NSEntityDescription *categorizationPredicate = [NSEntityDescription entityForName:@"MOJeweleryCategorizationPredicate" inManagedObjectContext:managedObjectContext];
    [fetchRequestCategorizationPredicates setEntity:categorizationPredicate];
    [fetchRequestCategorizationPredicates setReturnsDistinctResults:YES];
    //[fetchRequestCategorizationPredicates returnsObjectsAsFaults:NO];
    
    NSSortDescriptor *sortByPredicate = [[NSSortDescriptor alloc] initWithKey:@"categorization_predicate_level" ascending:NO];
    
    [fetchRequestCategorizationPredicates setSortDescriptors:[NSArray arrayWithObject:sortByPredicate]];
    
    NSExpression *exprCategorizationPredicateLevel = [NSExpression expressionForKeyPath:@"categorization_predicate_level"];
    NSExpression *exprTargetPredicateLevel = [NSExpression expressionForConstantValue:[NSNumber numberWithInt:0]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprCategorizationPredicateLevel
                                                                rightExpression:exprTargetPredicateLevel modifier:NSDirectPredicateModifier
                                                                           type:NSGreaterThanPredicateOperatorType options:0];
    
    [fetchRequestCategorizationPredicates setPredicate:predicate];
    
    self.menuCategorizationPredicates = [managedObjectContext executeFetchRequest:fetchRequestCategorizationPredicates error:nil];
    
    self.userPreferencesDictionary = [[NSMutableDictionary alloc] init];
    
    for (int i = 0; i < _menuCategorizationPredicates.count; i++) {
        
        MOJeweleryCategorizationPredicate *predicate = [_menuCategorizationPredicates objectAtIndex:i];
        [_userPreferencesDictionary setObject:[NSNumber numberWithInt:0] forKey:predicate.categorization_predicate_name];
    }
    
    return _menuCategorizationPredicates;
    
}

- (NSFetchedResultsController *)jeweleryFetchedResultsController
{
    if (_jeweleryFetchedResultsController != nil) {
        return _jeweleryFetchedResultsController;
    }
    
    
     NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
     NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOJewelery"
     inManagedObjectContext:self.managedObjectContext];
     
     NSSortDescriptor *sort = [[NSSortDescriptor alloc]
     initWithKey:@"jewelery_item_id" ascending:NO];
     
     [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
     [fetchRequest setEntity:entity];
    [fetchRequest setReturnsDistinctResults:YES];
     [fetchRequest setFetchBatchSize:20];
    [fetchRequest setReturnsObjectsAsFaults:YES];
    NSArray *categories = [self.userPreferencesDictionary allValues];
    NSPredicate *zeroPredicate = [NSPredicate predicateWithFormat:@"SELF != 0"];
    NSArray *filteredCategories = [categories filteredArrayUsingPredicate:zeroPredicate];
    //NSArray *categories = [NSArray arrayWithObjects:[NSNumber numberWithInt:2],[NSNumber numberWithInt:6], nil];
    //NSPredicate *jeweleryPredicate = [NSPredicate predicateWithFormat:@"SUBQUERY(belongs_to_categories, $category, $category.category_id IN %@).@count = %d",categories, [categories count]];
    NSPredicate *jeweleryPredicate = [NSPredicate predicateWithFormat:@"(SUBQUERY(belongs_to_categories, $category, $category.category_id IN %@).@count==%d)", filteredCategories, [filteredCategories count]];
    [fetchRequest setPredicate:jeweleryPredicate];
    
    
     NSFetchedResultsController *theFetchedResultsController =
     [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
     managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
     
     self.jeweleryFetchedResultsController = theFetchedResultsController;
     _jeweleryFetchedResultsController.delegate = self;
     
     return _jeweleryFetchedResultsController;
}

//NSFetchedResultsController Protocol implementation
- (void) controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void) controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
        atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
       newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(MOCatalogTableViewCell*)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark
#pragma Catalog Cell Protocol
- (void) selectedItemAtIndex:(int)index withIndexPath:(NSIndexPath *)indexPath
{
    MOJewelery *jewelery = [_jeweleryFetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:3*[indexPath row] + index inSection:[indexPath section]]];
    if (detailView != nil) {
        [detailView.coverView setAlpha:0.0];
        [detailView.coverView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0];
        detailView.coverView = nil;
        [detailView.view removeFromSuperview];
        detailView = nil;
        
    }
    self.detailView = [[MODetailImageView alloc] initWithNibName:@"MODetailImageView" bundle:nil];
    detailView.selectedJewelery = jewelery;
    detailView.viewDelegate = self;
    [detailView setRect:self.view.frame];
//    NSArray *detailSubViews = [self.view subviews];
//    for (UIView *detailSubView in detailSubViews) {
//        if ([detailSubView respondsToSelector:@selector(removeViewFromStack)]) {
//            NSLog(@"I wasnt here");
//            [detailSubView performSelector:@selector(removeViewFromStack)];
//        }
//    }
    [self presentSemiModalViewController:detailView];
}

#pragma mark
#pragma Detail Image Protocol
- (void)viewClosed
{
    [self dismissSemiModalViewController:detailView];
    detailView = nil;
}

- (void)tryJewelery:(MOJewelery *)jewelery
{
    [self dismissSemiModalViewController:detailView];
    detailView = nil;
    //MOCalibrateViewController *calibrateView = [[MOCalibrateViewController alloc] init];
    MOCalibrateViewController *calibrateView = [[MOCalibrateViewController alloc] initWithNibName:@"MOSingleJeweleryTrial" bundle:nil];
    [calibrateView setSelectedJewelery:jewelery];
    [self.navigationController pushViewController:calibrateView animated:YES];
    
}

- (void)addJeweleryToCart:(MOJewelery *)jewelery
{
    [self dismissSemiModalViewController:detailView];
    detailView = nil;
    BOOL alreadyPresent=NO;
    for (MOJewelery *item in _shoppingCart) {
        if ([item.jewelery_item_id isEqualToNumber:jewelery.jewelery_item_id]) {
            alreadyPresent = YES;
        }
    }
    if (!alreadyPresent) {
        [_shoppingCart addObject:jewelery]; 
    }
    
}

#pragma mark -
#pragma mark Application Runtime Data
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShoppingCartSegue"]) {
        MOCartViewController *cartViewController = (MOCartViewController *)[segue destinationViewController];
        cartViewController.managedObjectContext = self.managedObjectContext;
        cartViewController.shoppingCart = self.shoppingCart;
    }
}

@end
