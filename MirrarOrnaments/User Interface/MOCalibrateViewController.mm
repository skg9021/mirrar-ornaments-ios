                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       //
//  MOCalibrateViewController.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 11/09/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOCalibrateViewController.h"

@interface MOCalibrateViewController ()

@property (strong,nonatomic) ComputePoints *computePointsEngine;
@property (strong,nonatomic) NSMutableArray *renderers;
//@property (strong,nonatomic) NSMutableArray *textureInput;
@property (strong,nonatomic) GPUImageTextureInput *textureInput;
@property (strong,nonatomic) ES2Renderer *calibrateRenderer;
@property CGPoint leftClickPoint;
@property CGPoint rightClickPoint;
@property CGPoint lastMovementPosition;
@property NSDate *startTime;

@end

@implementation MOCalibrateViewController
@synthesize calibrateRendererView;
@synthesize computePointsEngine;
@synthesize renderers;
@synthesize textureInput;
@synthesize calibrateRenderer;
@synthesize delegate;
@synthesize leftClickPoint;
@synthesize rightClickPoint;
@synthesize lastMovementPosition;
@synthesize startTime;
@synthesize selectedJewelery = _selectedJewelery;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    [calibrateRendererView setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Compute Engine comes into action
    computePointsEngine = [[ComputePoints alloc] initWithSize:[calibrateRendererView sizeInPixels]];
    self.delegate = computePointsEngine;
    
    [computePointsEngine startCameraCapture];
    
    // And here comes the Renderer
    NSSet *hingePoints = _selectedJewelery.jewelery_image.hinge_point_set.hinge_points;
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"hinge_point_x" ascending:YES];
    NSArray *hingePointsArray = [hingePoints sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];

    MOJeweleryHingePoint *leftPoint = [hingePointsArray objectAtIndex:0];
    
    MOJeweleryHingePoint *rightPoint = [hingePointsArray objectAtIndex:1];
    
    leftHingePoint = CGPointMake([leftPoint.hinge_point_x intValue], [leftPoint.hinge_point_y intValue]);
    
    rightHingePoint = CGPointMake([rightPoint.hinge_point_x intValue], [rightPoint.hinge_point_y intValue]);
    
    
    calibrateRenderer = [[ES2Renderer alloc] initWithSize:[calibrateRendererView sizeInPixels] jewelryName:[[_selectedJewelery.jewelery_image.jewelery_image_filename copy] stringByAppendingString:@".png"] leftPoint:leftHingePoint rightPoint:rightHingePoint];
    [calibrateRenderer setComputePoints:computePointsEngine];
    
    // Adding Renderer to Renderer Queue
    renderers = [computePointsEngine renderers];
    __weak ES2Renderer *weakRenderer = calibrateRenderer;
    [renderers addObject:weakRenderer];
    
    // Textures for GPUImage
    textureInput = [[GPUImageTextureInput alloc] initWithTexture:calibrateRenderer.outputTexture size:[calibrateRendererView sizeInPixels]];
    
    //__weak GPUImageTextureInput *texIp = textureInputrenderer;
    //[textureInput addObject:texIp];
    
    // Adding to Pipeline
    //for (GPUImageTextureInput *texInput in textureInput) {
      //  [texInput addTarget:calibrateRendererView];
    //}
    
    
    [textureInput addTarget:calibrateRendererView];
    
    pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [calibrateRendererView addGestureRecognizer:pinchGesture];
    mLastScale = 1.0f;
    mCurrentScale = 1.0f;
    for (ES2Renderer *curRenderer in renderers) {
        [curRenderer setNewFrameAvailableBlock:^{
            float currentTimeInMilliseconds = [[NSDate date] timeIntervalSinceDate:startTime] * 1000.0;
            
            [textureInput processTextureWithFrameTime:CMTimeMake((int)currentTimeInMilliseconds, 1000)];
        }];
    }
    
    //self.navigationItem.hidesBackButton = YES;
    
    mImagePositionGL = CGPointMake(0, 0);
    imageScale = 1.0;
}

- (void)viewDidUnload
{

    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    [self setCalibrateRendererView:nil];
    [renderers removeAllObjects];
    calibrateRenderer = nil;
    //computePointsEngine.faceQueue = nil;
    dispatch_release(computePointsEngine.faceQueue);
    
    computePointsEngine = nil;


}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    [self setCalibrateRendererView:nil];
    [renderers removeAllObjects];
    calibrateRenderer = nil;
    //computePointsEngine.faceQueue = nil;
    //dispatch_suspend(computePointsEngine.faceQueue);
    
    //computePointsEngine.faceQueue = nil;
    
    [delegate performSelector:@selector(stopTracking:) withObject:[NSNumber numberWithBool:YES]];
    dispatch_release(computePointsEngine.faceQueue);
    computePointsEngine.faceQueue = nil;
    computePointsEngine = nil;
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)needToInitPressed
{
    [delegate performSelector:@selector(needToInit:) withObject:[NSNumber numberWithBool:YES]];
}

- (IBAction)goToCart:(id)sender {
    
    [computePointsEngine->videoCamera stopCameraCapture];
    computePointsEngine = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)handlePinch:(UIPinchGestureRecognizer *)sender
{
    [delegate performSelector:@selector(stopTracking:) withObject:[NSNumber numberWithBool:YES]];
    //NSLog(@"lastScale = %f",mLastScale);
    
    mCurrentScale = [sender scale] - mLastScale;
    mLastScale = [sender scale];
    
    //NSLog(@"Current Scale = %f",mCurrentScale);
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        mLastScale = 1.0f;
    }
    
    imageScale += mCurrentScale;
    //NSLog(@"Current scale from calib window : %f",imageScale);
    [calibrateRenderer renderByRotatingAroundX:0.0 rotatingAroundY:0.0 scale:mCurrentScale];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSMutableSet *currentTouches = [[event touchesForView:self.view] mutableCopy];
    [currentTouches minusSet:touches];
	
	// New touches are not yet included in the current touches for the view
	lastMovementPosition = [[touches anyObject] locationInView:self.view];
    
    [delegate performSelector:@selector(stopTracking:) withObject:[NSNumber numberWithBool:YES]];
    
    mImagePositionGL = CGPointMake(calibrateRenderer->currentJewelryModelViewMatrix[12], calibrateRenderer->currentJewelryModelViewMatrix[13]);
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
{
	CGPoint currentMovementPosition = [[touches anyObject] locationInView:self.view];
    float currentPositionX = (currentMovementPosition.x-lastMovementPosition.x);
    float currentPositionY = (currentMovementPosition.y-lastMovementPosition.y);
    
    [calibrateRenderer renderByRotatingAroundX:currentPositionX rotatingAroundY:currentPositionY scale:mCurrentScale];
    //[calibrateRenderer renderByRotatingAroundX:currentMovementPosition.x rotatingAroundY:currentMovementPosition.y];
    
    mImagePositionGL = CGPointMake(mImagePositionGL.x + ((currentPositionX*2)/1536)*2, mImagePositionGL.y + ((currentPositionY*2)/2048)*2);
    
    NSLog(@"Position when Touch Moved X:%f \t Y:%f",mImagePositionGL.x,mImagePositionGL.y);
    
	lastMovementPosition = currentMovementPosition;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSMutableSet *remainingTouches = [[event touchesForView:self.view] mutableCopy];
    [remainingTouches minusSet:touches];
    
	lastMovementPosition = [[remainingTouches anyObject] locationInView:self.view];
    
    [self calibrate];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	// Handle touches canceled the same as as a touches ended event
    [self touchesEnded:touches withEvent:event];
}

- (IBAction)calibrateButtonPressed:(id)sender {
    
    //NSString *fileName = @"6-19-376-19.png";
    //CGImageRef spriteImage = [UIImage imageNamed:fileName].CGImage;
   /*
    size_t imgWidth = CGImageGetWidth(spriteImage);
    size_t imgHeight = CGImageGetHeight(spriteImage);
    */
    
    [self calibrate];
}

- (void)calibrate
{
    size_t imgWidth = [_selectedJewelery.jewelery_image.jewelery_image_width intValue];
    
    size_t imgHeight = [_selectedJewelery.jewelery_image.jewelery_image_height intValue];
    
    NSLog(@"Position from calib window X:%f \t Y:%f",mImagePositionGL.x,mImagePositionGL.y);
    NSLog(@"Current scale from calib window : %f",imageScale);
    
    
    [delegate performSelector:@selector(setleftHingePoint:rightHingePoint:) withObject:[NSValue valueWithCGPoint:leftHingePoint] withObject:[NSValue valueWithCGPoint:rightHingePoint]];
    
    
    [delegate performSelector:@selector(setImageWidth:ImageHeight:) withObject:[NSNumber numberWithFloat:imgWidth] withObject:[NSNumber numberWithFloat:imgHeight]];
    
    [delegate performSelector:@selector(setImageScale:) withObject:[NSNumber numberWithFloat:imageScale]];
    
    [delegate performSelector:@selector(computeCLickPoints:) withObject:[NSValue valueWithCGPoint:mImagePositionGL]];
    
    [self needToInitPressed];

}

@end
