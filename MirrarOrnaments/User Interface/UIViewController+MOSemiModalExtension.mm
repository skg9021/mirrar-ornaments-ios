//
//  UIViewController+TDSemiModalExtension.m
//  TDSemiModal
//
//  Created by Nathan  Reed on 18/10/10.
//  Copyright 2010 Nathan Reed. All rights reserved.
//

#import "UIViewController+MOSemiModalExtension.h"

@implementation UIViewController (MOSemiModalExtension)

// Use this to show the modal view (pops-up from the bottom)
- (void) presentSemiModalViewController:(MOSemiModalViewController*)vc {
#define DEGREES_TO_RADIANS(x) (M_PI * (x)/180.0)

	UIView* modalView = vc.view;
	UIView* coverView = vc.coverView;

	//UIWindow* mainWindow = [(id)[[UIApplication sharedApplication] delegate] window];

	//CGPoint middleCenter = self.view.center;
	//CGSize offSize = [UIScreen mainScreen].bounds.size;

	//UIInterfaceOrientation orientation =  UIApplication.sharedApplication.statusBarOrientation;

	//CGPoint offScreenCenter = CGPointZero;

    //offScreenCenter = CGPointMake(offSize.width / 2.0, offSize.height * 1.2);
    CGRect rect = [vc rect];
    float realx= rect.origin.x;
    float realy= rect.origin.y;
    [modalView setFrame:CGRectMake(40, realy + 40, 690, 900)];
    [coverView setFrame:CGRectMake(realx, realy, 768, 980)];

	// we start off-screen
	//modalView.center = offScreenCenter;
	 
	coverView.alpha = 0.0f;
	[modalView setAlpha:0.0f];
    
    //modalView.center = CGPointMake(middleCenter.x, middleCenter.y*realy);
	
    [self.view addSubview:coverView];
	[self.view addSubview:modalView];
	
	// Show it with a transition effect
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.6];
	
	modalView.alpha = 1.0f;
	coverView.alpha = 0.5;
    
	[UIView commitAnimations];

}

// Use this to slide the semi-modal view back down.
-(void) dismissSemiModalViewController:(MOSemiModalViewController*)vc {
	double animationDelay = 0.7;
	UIView* modalView = vc.view;
	UIView* coverView = vc.coverView;

	//CGSize offSize = [UIScreen mainScreen].bounds.size;

	//CGPoint offScreenCenter = CGPointZero;
	
    //offScreenCenter = CGPointMake(offSize.width / 2.0, offSize.height * 1.5);

	[UIView beginAnimations:nil context:(__bridge void *)(modalView)];
	[UIView setAnimationDuration:animationDelay];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(dismissSemiModalViewControllerEnded:finished:context:)];
	modalView.alpha = 0.0f;
	coverView.alpha = 0.0f;
	[UIView commitAnimations];

	[coverView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:animationDelay];

}

- (void) dismissSemiModalViewControllerEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
	UIView* modalView = (__bridge UIView*)context;
	[modalView removeFromSuperview];

}

@end
