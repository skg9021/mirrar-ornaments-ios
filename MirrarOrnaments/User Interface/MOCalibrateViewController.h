//
//  MOCalibrateViewController.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 11/09/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESRenderer.h"
#import "ES2Renderer.h"
#import "GPUImage.h"
#import "MOJewelery.h"

@interface MOCalibrateViewController : UIViewController
{
    //ES2Renderer *calibrateRenderer;
    //GPUImageTextureInput *textureInput;
    //NSMutableArray *renderers;
    UIPinchGestureRecognizer *pinchGesture;
    float mLastScale, mCurrentScale;
    CGPoint mImagePositionGL;
    float imageScale;
    CGPoint leftHingePoint, rightHingePoint;
}
@property (strong,nonatomic) id <SliderProtocol> delegate;

- (void)needToInitPressed;
- (IBAction)goToCart:(id)sender;

@property (weak, nonatomic) IBOutlet GPUImageView *calibrateRendererView;
@property (weak, nonatomic) MOJewelery *selectedJewelery;

- (IBAction)calibrateButtonPressed:(id)sender;
- (void)handlePinch:(UIPinchGestureRecognizer *)sender;
- (void)calibrate;
@end
