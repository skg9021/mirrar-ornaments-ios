//
//  MODetailImageView.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 21/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MODetailImageView.h"

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)

@implementation MODetailImageView

@synthesize detailImageView;
@synthesize nameLabel;
@synthesize descriptionLabel;

@synthesize selectedJewelery;
@synthesize viewDelegate;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void) viewDidLoad
{
    [super viewDidLoad];
    NSString *imagePath =  self.selectedJewelery.jewelery_preview_image;
    imagePath = [imagePath stringByAppendingString:@"_big@2x.png"];
    //NSLog(@"%@", imagePath);
    NSData *thumbnail = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
    [self.detailImageView setImage:[UIImage imageWithData:thumbnail]];
    [self.nameLabel setText:self.selectedJewelery.jewelery_caption_text];
    [self.descriptionLabel setText:self.selectedJewelery.jewelery_description];
}

- (void) viewDidUnload {
    //[self.view removeFromSuperview];
    [super viewDidUnload];
    
}


- (IBAction)closeDetailView:(id)sender {
    SAFE_PERFORM_WITH_ARG(self.viewDelegate, @selector(viewClosed), nil);
}

- (IBAction)tryJewelery:(id)sender {
    SAFE_PERFORM_WITH_ARG(self.viewDelegate, @selector(tryJewelery:), self.selectedJewelery);
}

- (IBAction)addJeweleryToCart:(id)sender {
    SAFE_PERFORM_WITH_ARG(self.viewDelegate, @selector(addJeweleryToCart:), self.selectedJewelery);
}

@end
