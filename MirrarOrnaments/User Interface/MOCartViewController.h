//
//  MOCartViewController.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 24/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOCartViewCell.h"

@interface MOCartViewController : UITableViewController<MOCartViewCellProtocol>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (strong) NSMutableArray           *shoppingCart;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *selectedIndexes;
@end
