                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       //
//  MOCalibrateViewController.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 11/09/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOSplitViewController.h"
#import "MOCompareViewController.h"

@interface MOSplitViewController ()


@property (strong,nonatomic) NSMutableArray *renderers;
//@property (strong,nonatomic) NSMutableArray *textureInput;
@property (strong,nonatomic) GPUImageTextureInput *textureInput;
@property (strong,nonatomic) ES2Renderer *calibrateRenderer;
@property CGPoint leftClickPoint;
@property CGPoint rightClickPoint;
@property CGPoint lastMovementPosition;
@property NSDate *startTime;

@end

@implementation MOSplitViewController

@synthesize calibrateRendererView;
@synthesize computePointsEngine;
@synthesize renderers;
@synthesize textureInput;
@synthesize calibrateRenderer;
@synthesize delegate;
@synthesize leftClickPoint;
@synthesize rightClickPoint;
@synthesize lastMovementPosition;
@synthesize startTime;
@synthesize selectedJewelery = _selectedJewelery;
@synthesize compareView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    [calibrateRendererView setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.delegate = computePointsEngine;
    
    // And here comes the Renderer
    NSSet *hingePoints = _selectedJewelery.jewelery_image.hinge_point_set.hinge_points;
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"hinge_point_x" ascending:YES];
    NSArray *hingePointsArray = [hingePoints sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];

    MOJeweleryHingePoint *leftPoint = [hingePointsArray objectAtIndex:0];
    
    MOJeweleryHingePoint *rightPoint = [hingePointsArray objectAtIndex:1];
    
    leftHingePoint = CGPointMake([leftPoint.hinge_point_x intValue], [leftPoint.hinge_point_y intValue]);
    
    rightHingePoint = CGPointMake([rightPoint.hinge_point_x intValue], [rightPoint.hinge_point_y intValue]);
    
    
    calibrateRenderer = [[ES2Renderer alloc] initWithSize:[calibrateRendererView sizeInPixels] jewelryName:[[_selectedJewelery.jewelery_image.jewelery_image_filename copy] stringByAppendingString:@".png"] leftPoint:leftHingePoint rightPoint:rightHingePoint];
    [calibrateRenderer setComputePoints:computePointsEngine];
    
    // Adding Renderer to Renderer Queue
    renderers = [computePointsEngine renderers];
    __weak ES2Renderer *weakRenderer = calibrateRenderer;
    [renderers addObject:weakRenderer];
    
    // Textures for GPUImage
    textureInput = [[GPUImageTextureInput alloc] initWithTexture:calibrateRenderer.outputTexture size:[calibrateRendererView sizeInPixels]];
    
    //__weak GPUImageTextureInput *texIp = textureInputrenderer;
    //[textureInput addObject:texIp];
    
    // Adding to Pipeline
    //for (GPUImageTextureInput *texInput in textureInput) {
      //  [texInput addTarget:calibrateRendererView];
    //}
    
    
    [textureInput addTarget:calibrateRendererView];
    
    pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [calibrateRendererView addGestureRecognizer:pinchGesture];
    mLastScale = 1.0f;
    mCurrentScale = 1.0f;
    for (ES2Renderer *curRenderer in renderers) {
        [curRenderer setNewFrameAvailableBlock:^{
            float currentTimeInMilliseconds = [[NSDate date] timeIntervalSinceDate:startTime] * 1000.0;
            
            [textureInput processTextureWithFrameTime:CMTimeMake((int)currentTimeInMilliseconds, 1000)];
        }];
    }
    
    //self.navigationItem.hidesBackButton = YES;
    
    mImagePositionGL = CGPointMake(0, 0);
    imageScale = 1.0;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    for (ES2Renderer *renderer in [computePointsEngine renderers]) {
        if ([renderer isEqual:calibrateRenderer]) {
            [[computePointsEngine renderers] removeObject:renderer];
        }
    }
    compareView->imageScale = imageScale;
    compareView->mLastScale = mLastScale;
    compareView->mImagePositionGL = mImagePositionGL;
    compareView->mCurrentScale = mCurrentScale;
    
    [self setCalibrateRendererView:nil];
    calibrateRenderer = nil;
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)needToInitPressed
{
    [delegate performSelector:@selector(needToInit:) withObject:[NSNumber numberWithBool:YES]];
}

- (void)handlePinch:(UIPinchGestureRecognizer *)sender
{
    [delegate performSelector:@selector(stopTracking:) withObject:[NSNumber numberWithBool:YES]];
    //NSLog(@"lastScale = %f",mLastScale);
    
    mCurrentScale = [sender scale] - mLastScale;
    mLastScale = [sender scale];
    
    //NSLog(@"Current Scale = %f",mCurrentScale);
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        mLastScale = 1.0f;
    }
    
    imageScale += mCurrentScale;
    //NSLog(@"Current scale from calib window : %f",imageScale);
    [calibrateRenderer renderByRotatingAroundX:0.0 rotatingAroundY:0.0 scale:mCurrentScale];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSMutableSet *currentTouches = [[event touchesForView:self.view] mutableCopy];
    [currentTouches minusSet:touches];
	
	// New touches are not yet included in the current touches for the view
	lastMovementPosition = [[touches anyObject] locationInView:self.view];
    
    [delegate performSelector:@selector(stopTracking:) withObject:[NSNumber numberWithBool:YES]];
    
    mImagePositionGL = CGPointMake(calibrateRenderer->currentJewelryModelViewMatrix[12], calibrateRenderer->currentJewelryModelViewMatrix[13]);
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
{
	CGPoint currentMovementPosition = [[touches anyObject] locationInView:self.view];
    float currentPositionX = (currentMovementPosition.x-lastMovementPosition.x);
    float currentPositionY = (currentMovementPosition.y-lastMovementPosition.y);
    
    [calibrateRenderer renderByRotatingAroundX:currentPositionX rotatingAroundY:currentPositionY scale:mCurrentScale];
    //[calibrateRenderer renderByRotatingAroundX:currentMovementPosition.x rotatingAroundY:currentMovementPosition.y];
    
    mImagePositionGL = CGPointMake(mImagePositionGL.x + ((currentPositionX*2)/1536)*2, mImagePositionGL.y + ((currentPositionY*2)/2048)*2);
    
    NSLog(@"Position when Touch Moved X:%f \t Y:%f",mImagePositionGL.x,mImagePositionGL.y);
    
	lastMovementPosition = currentMovementPosition;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSMutableSet *remainingTouches = [[event touchesForView:self.view] mutableCopy];
    [remainingTouches minusSet:touches];
    
	lastMovementPosition = [[remainingTouches anyObject] locationInView:self.view];
    
    [self calibrate];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	// Handle touches canceled the same as as a touches ended event
    [self touchesEnded:touches withEvent:event];
}

#pragma mark -
#pragma mark Application Runtime Data

- (void)calibrate
{
    size_t imgWidth = [_selectedJewelery.jewelery_image.jewelery_image_width intValue];
    
    size_t imgHeight = [_selectedJewelery.jewelery_image.jewelery_image_height intValue];
    
    NSLog(@"Position from calib window X:%f \t Y:%f",mImagePositionGL.x,mImagePositionGL.y);
    NSLog(@"Current scale from calib window : %f",imageScale);
    
    
    [delegate performSelector:@selector(setleftHingePoint:rightHingePoint:) withObject:[NSValue valueWithCGPoint:leftHingePoint] withObject:[NSValue valueWithCGPoint:rightHingePoint]];
    
    
    [delegate performSelector:@selector(setImageWidth:ImageHeight:) withObject:[NSNumber numberWithFloat:imgWidth] withObject:[NSNumber numberWithFloat:imgHeight]];
    
    [delegate performSelector:@selector(setImageScale:) withObject:[NSNumber numberWithFloat:imageScale]];
    
    [delegate performSelector:@selector(computeCLickPoints:) withObject:[NSValue valueWithCGPoint:mImagePositionGL]];
    
    [self needToInitPressed];

}

@end
