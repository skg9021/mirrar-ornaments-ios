//
//  MOScreensaverViewController.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

@interface MOScreensaverViewController : UIViewController <UIGestureRecognizerDelegate>

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer;
@property (strong) MPMoviePlayerController *screensaverController;

// Managed Object Context for the App
@property (strong) NSManagedObjectContext *managedObjectContext;
@end
