//
//  MOScreensaverViewController.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOScreensaverViewController.h"

@interface MOScreensaverViewController ()

@end

@implementation MOScreensaverViewController
@synthesize screensaverController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Bunny" withExtension:@"mp4"];
    screensaverController = [[MPMoviePlayerController alloc] initWithContentURL:url];
    
    [screensaverController prepareToPlay];
    screensaverController.controlStyle = MPMovieControlStyleNone;
    screensaverController.shouldAutoplay = YES;
    screensaverController.repeatMode = MPMovieRepeatModeOne;
    screensaverController.fullscreen = YES;
    [screensaverController.view  setFrame:self.view.bounds];
    [self.view addSubview:screensaverController.view];
    [screensaverController play];
     
    
    UITapGestureRecognizer* doubleTapRecon = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    [doubleTapRecon setNumberOfTapsRequired:2];
    [doubleTapRecon setDelegate:self];
    [screensaverController.view addGestureRecognizer:doubleTapRecon];
    /*
    UITapGestureRecognizer* singleTapRecon = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [singleTapRecon setNumberOfTapsRequired:1];
    //[singleTapRecon requireGestureRecognizerToFail:doubleTapRecon];
    [singleTapRecon setDelegate:self];
    [screensaverController.view addGestureRecognizer:singleTapRecon];
    */
    
    UIImage *textImage = [UIImage imageNamed:@"taptounlock_white.png"];
    CGFloat textWidth = textImage.size.width;
    CGFloat textHeight = textImage.size.height;
    
    CALayer *textLayer = [CALayer layer];
    textLayer.contents = (id)[textImage CGImage];
    textLayer.frame = CGRectMake(250.0f, 920.0f, textWidth, textHeight);
    
    CALayer *maskLayer = [CALayer layer];
    // Mask image ends with 0.15 opacity on both sides. Set the background color of the layer
    // to the same value so the layer can extend the mask image.
    maskLayer.backgroundColor = [[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.15f] CGColor];
    maskLayer.contents = (id)[[UIImage imageNamed:@"Mask.png"] CGImage];
    
    // Center the mask image on twice the width of the text layer, so it starts to the left
    // of the text layer and moves to its right when we translate it by width.
    maskLayer.contentsGravity = kCAGravityCenter;
    maskLayer.frame = CGRectMake(-textWidth, 0.0f, textWidth * 2, textHeight * 0.85f);
    
    // Animate the mask layer's horizontal position
    CABasicAnimation *maskAnim = [CABasicAnimation animationWithKeyPath:@"position.x"];
    maskAnim.byValue = [NSNumber numberWithFloat:textWidth];
    maskAnim.repeatCount = 3.4e38f;
    maskAnim.duration = 1.0f;
    [maskLayer addAnimation:maskAnim forKey:@"slideAnim"];
    
    textLayer.mask = maskLayer;
    [self.view.layer addSublayer:textLayer];
    
    /*
    [self.view addGestureRecognizer:singleTapRecon];
    [self.view addGestureRecognizer:doubleTapRecon];
     */
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void)stopScreensaver:(id)sender
{
    
}

-(void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [screensaverController stop];
    [screensaverController.view removeFromSuperview];
    [self performSegueWithIdentifier:@"Screensaver Segue" sender:self];
}

-(void)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
    [screensaverController stop];
    [screensaverController.view removeFromSuperview];
    [self performSegueWithIdentifier:@"Screensaver Segue" sender:self];
}
@end
