//
//  MOMenuCell.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 20/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MOMenuCellProtocol <NSObject>

- (void) selectedItemAtIndex:(int)index withIndexPath:(NSIndexPath*)indexPath;

@end


@interface MOMenuCell : UITableViewCell<UIGestureRecognizerDelegate>

@property (nonatomic, retain) IBOutlet UIImageView *categoryImage1;
@property (nonatomic, retain) IBOutlet UILabel *categoryName1;
@property (nonatomic, retain) IBOutlet UILabel *categoryDescription1;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton1;
@property (weak, nonatomic) IBOutlet UIImageView *categoryButton1Image;

@property (nonatomic, retain) IBOutlet UIImageView *categoryImage2;
@property (nonatomic, retain) IBOutlet UILabel *categoryName2;
@property (nonatomic, retain) IBOutlet UILabel *categoryDescription2;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton2;
@property (weak, nonatomic) IBOutlet UIImageView *categoryButton2Image;

- (IBAction)button1Clicked:(id)sender;
- (IBAction)button2Clicked:(id)sender;

@property (nonatomic, retain) NSIndexPath *indexPath;
@property (nonatomic, retain) id <MOMenuCellProtocol> delegate;

@property BOOL button1Selected;
@property BOOL button2Selected;
@end
