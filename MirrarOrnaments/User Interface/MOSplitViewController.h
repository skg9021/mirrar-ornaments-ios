//
//  MOSplitViewController.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 11/09/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESRenderer.h"
#import "ES2Renderer.h"
#import "GPUImage.h"
#import "MOJewelery.h"
#import "MOCompareViewController.h"

@interface MOSplitViewController : UIViewController
{
    UIPinchGestureRecognizer *pinchGesture;
    
    CGPoint leftHingePoint, rightHingePoint;
    
@public
    float mLastScale, mCurrentScale;
    CGPoint mImagePositionGL;
    float imageScale;
}
@property (strong,nonatomic) id <SliderProtocol> delegate;

- (void)needToInitPressed;

@property (weak, nonatomic) IBOutlet GPUImageView *calibrateRendererView;
@property (weak, nonatomic) MOJewelery *selectedJewelery;
@property (weak, nonatomic) ComputePoints *computePointsEngine;
@property (weak, nonatomic) MOCompareViewController *compareView;
- (void)handlePinch:(UIPinchGestureRecognizer *)sender;
- (void)calibrate;
@end
