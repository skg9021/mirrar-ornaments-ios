//
//  MOLoginScreenViewController.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOLoginScreenViewController.h"
#import "MOCoreDataHelper.h"
#import "MOMenuViewController.h"

@interface MOLoginScreenViewController ()

@end

@implementation MOLoginScreenViewController

@synthesize managedObjectContext, usernameField, passwordField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}


//  When the view reappears after logout we want to wipe the username and password fields
- (void)viewWillAppear:(BOOL)animated
{
    [usernameField setText:@""];
    [passwordField setText:@""];
}

// When we are done editing with the keyboard
- (IBAction)resignAndLogin:(id)sender
{
    // Get a reference to the text field on which the done button was pressed
    UITextField *textField = (UITextField *)sender;
    
    // Check the tag. If this is the username field, then jump to the password field automatically
    if(textField.tag == 1)
    {
        [passwordField becomeFirstResponder];
        
        //Otherwise we pressed the done on the password field, and want to attempt login
    } else {
        // First put away the keyboard
        [sender resignFirstResponder];
        
        // Set up a predicate (or search criteria) for checking the count of found users with these details
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(username == %@ && password == %@)", [usernameField text], [passwordField text]];
        
        //  Actually run the query in Core Data and return the count of found users with these details
        // Obviously if it found ANY then we got the username and password right!
        if ([MOCoreDataHelper countForEntity:@"MOUsers" withPredicate:pred andContext:managedObjectContext] > 0) {
            //  We found a matching login user!  Force the segue transition to the next view
            [self performSegueWithIdentifier:@"LoginSegue" sender:sender];
        } else {
            //  We didn't find any matching login users. Wipe the password field to re-enter
            [passwordField setText:@""];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Unsuccessful"
                                                            message:@"Username or Password entered is incorrect."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
    }
    
}

#pragma mark -
#pragma mark Application Runtime Data
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"LoginSegue"]) {
        
        UINavigationController *navigationController = [segue destinationViewController];
        MOMenuViewController *menuViewController = (MOMenuViewController *)navigationController.topViewController;
        menuViewController.managedObjectContext = self.managedObjectContext;
        //menuViewController.recomputeUserPreferences = YES;
    }
}
@end
