//
//  MOMenuViewController.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOMenuViewController.h"
#import "MOCoreDataHelper.h"
#import "MOCatalogViewController.h"

@interface MOMenuViewController ()

@end

@implementation MOMenuViewController
@synthesize tableView = _tableView;

@synthesize managedObjectContext;
@synthesize jeweleryFetchedResultsController = _jeweleryFetchedResultsController;
@synthesize menuCategorizationPredicates = _menuCategorizationPredicates;
@synthesize userPreferencesDictionary = _userPreferencesDictionary;
@synthesize shoppingCart = _shoppingCart;
@synthesize cellIndexes = _cellIndexes;
@synthesize fileSystemManager;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSError *errorFetch;
    if (![[self jeweleryFetchedResultsController] performFetch:&errorFetch]) {
        abort();
    }
    
    self.title = @"Menu";
    
    _cellIndexes = [[NSMutableDictionary alloc] init];
    _userPreferencesDictionary = [[NSMutableDictionary alloc] init];
    _shoppingCart = [[NSMutableArray alloc] init];
    self.fileSystemManager = [[MOFileSystemManager alloc] init];
    [self menuCategorizationPredicates];
    [self.tableView setBackgroundView:nil];
    [self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background@2x.png"]]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.jeweleryFetchedResultsController = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [[self.jeweleryFetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    if ([[self.jeweleryFetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo =
        [[self.jeweleryFetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects] / 2 + [sectionInfo numberOfObjects] % 2;
    }
    return numberOfRows;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(45, 0, tableView.bounds.size.width-88, 44)];
    UIImageView *headerImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"section_header_blue"]];
    headerImage.frame = CGRectMake(45, 20, tableView.bounds.size.width-88, 44);
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 32, tableView.bounds.size.width-88, 25)];
    headerLabel.textAlignment = UITextAlignmentCenter;
    headerLabel.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    headerLabel.backgroundColor = [UIColor clearColor];
    // Put shadow here
    [headerLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:21]];
    [headerView addSubview:headerImage];
    [headerView addSubview:headerLabel];
    
    NSString *sectionHeader = nil;
    if ([[self.jeweleryFetchedResultsController sections] count] > section) {
        id <NSFetchedResultsSectionInfo> sectionInfo =
        [[self.jeweleryFetchedResultsController sections] objectAtIndex:section];
        
        MOJeweleryCategorizationPredicate *categorizationPredicate = (MOJeweleryCategorizationPredicate *)[self.menuCategorizationPredicates objectAtIndex:[self.menuCategorizationPredicates count] - [[sectionInfo name] intValue]];
        sectionHeader = categorizationPredicate.categorization_predicate_name;
    }
    headerLabel.text = sectionHeader;
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 65;
}

- (void)configureCell:(MOMenuCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSInteger numberOfRows;
    id <NSFetchedResultsSectionInfo> sectionInfo =
    [[self.jeweleryFetchedResultsController sections] objectAtIndex:[indexPath section]];
    numberOfRows = [sectionInfo numberOfObjects];
    NSInteger currentRow = [indexPath row];
    MOJeweleryCategory *jeweleryCategory;
    NSString *imagePath;
    NSData *thumbnail;
    if (2*currentRow + 1 < numberOfRows) {
        jeweleryCategory = [_jeweleryFetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:2*[indexPath row] inSection:[indexPath section]]];
        cell.categoryName1.text = jeweleryCategory.category_name;
        imagePath = jeweleryCategory.category_caption_image;
        thumbnail = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
        [cell.categoryImage1 setImage:[UIImage imageWithData:thumbnail]];
        
        [_cellIndexes setObject:[NSArray arrayWithObjects:indexPath,[NSNumber numberWithInt:0], nil] forKey:[NSString stringWithFormat:@"%@",jeweleryCategory.category_id]];
        
        if (!(jeweleryCategory.category_description == (id)[NSNull null] || jeweleryCategory.category_description.length == 0 )) {
            cell.categoryDescription1.text = jeweleryCategory.category_description;
        }
        
        jeweleryCategory = [_jeweleryFetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:2*[indexPath row]+1 inSection:[indexPath section]]];
        cell.categoryName2.text = jeweleryCategory.category_name;
        imagePath = jeweleryCategory.category_caption_image;
        thumbnail = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
        [cell.categoryImage2 setImage:[UIImage imageWithData:thumbnail]];
        
        [_cellIndexes setObject:[NSArray arrayWithObjects:indexPath,[NSNumber numberWithInt:1], nil] forKey:[NSString stringWithFormat:@"%@",jeweleryCategory.category_id]];
        
        if (!(jeweleryCategory.category_description == (id)[NSNull null] || jeweleryCategory.category_description.length == 0 )) {
            cell.categoryDescription2.text = jeweleryCategory.category_description;
        }
        
        [cell setIndexPath:indexPath];
    }
    else
    {
        jeweleryCategory = [_jeweleryFetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:2*[indexPath row] inSection:[indexPath section]]];
        cell.categoryName1.text = jeweleryCategory.category_name;
        imagePath = jeweleryCategory.category_caption_image;
        thumbnail = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
        [cell.categoryImage1 setImage:[UIImage imageWithData:thumbnail]];
        
        [_cellIndexes setObject:[NSArray arrayWithObjects:indexPath,[NSNumber numberWithInt:0], nil] forKey:[NSString stringWithFormat:@"%@",jeweleryCategory.category_id]];
        
        if (!(jeweleryCategory.category_description == (id)[NSNull null] || jeweleryCategory.category_description.length == 0 )) {
            cell.detailTextLabel.text = jeweleryCategory.category_description;
        }
        
        [cell.categoryButton2 setHidden:YES];
        [cell setIndexPath:indexPath];
    }
    [cell setDelegate:self];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    MOMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MOMenuCell alloc] init];
        cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"menu_cell@2x.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0]];
    }
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MOJeweleryCategory *jeweleryCategory = [_jeweleryFetchedResultsController objectAtIndexPath:indexPath];
    NSString *parentPredicate = jeweleryCategory.category_predicate.categorization_predicate_name;
    [_userPreferencesDictionary setValue:[NSNumber numberWithInt:0] forKey:parentPredicate];
}

#pragma mark - Core Data Methods
- (NSArray *)menuCategorizationPredicates
{
    if (_menuCategorizationPredicates != nil) {
        return _menuCategorizationPredicates;
    }
    
    // Fetch the categorization predicates to fill the menu
    NSFetchRequest *fetchRequestCategorizationPredicates = [[NSFetchRequest alloc] init];
    NSEntityDescription *categorizationPredicate = [NSEntityDescription entityForName:@"MOJeweleryCategorizationPredicate" inManagedObjectContext:managedObjectContext];
    [fetchRequestCategorizationPredicates setEntity:categorizationPredicate];
    [fetchRequestCategorizationPredicates setReturnsDistinctResults:YES];
    
    
    NSSortDescriptor *sortByPredicate = [[NSSortDescriptor alloc] initWithKey:@"categorization_predicate_level" ascending:NO];
    
    [fetchRequestCategorizationPredicates setSortDescriptors:[NSArray arrayWithObject:sortByPredicate]];

    NSExpression *exprCategorizationPredicateLevel = [NSExpression expressionForKeyPath:@"categorization_predicate_level"];
    NSExpression *exprTargetPredicateLevel = [NSExpression expressionForConstantValue:[NSNumber numberWithInt:0]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprCategorizationPredicateLevel
            rightExpression:exprTargetPredicateLevel modifier:NSDirectPredicateModifier
                type:NSGreaterThanPredicateOperatorType options:0];
    
    [fetchRequestCategorizationPredicates setPredicate:predicate];
    
    self.menuCategorizationPredicates = [managedObjectContext executeFetchRequest:fetchRequestCategorizationPredicates error:nil];
    
    self.userPreferencesDictionary = [[NSMutableDictionary alloc] init];
    
    for (int i = 0; i < _menuCategorizationPredicates.count; i++) {
        
        MOJeweleryCategorizationPredicate *predicate = [_menuCategorizationPredicates objectAtIndex:i];
        [_userPreferencesDictionary setObject:[NSNumber numberWithInt:0] forKey:predicate.categorization_predicate_name];
    }
    
    return _menuCategorizationPredicates;
}

- (NSFetchedResultsController *)jeweleryFetchedResultsController
{
    if (_jeweleryFetchedResultsController != nil) {
        return _jeweleryFetchedResultsController;
    }
    
    // Fetch the corresponding categories
    NSFetchRequest *fetchRequestCategories = [[NSFetchRequest alloc] init];
    NSEntityDescription *category = [NSEntityDescription entityForName:@"MOJeweleryCategory"
                                                inManagedObjectContext:managedObjectContext];
    [fetchRequestCategories setEntity:category];
    [fetchRequestCategories setReturnsDistinctResults:YES];
    [fetchRequestCategories setReturnsObjectsAsFaults:NO];
    
    NSSortDescriptor *sortByPredicate = [[NSSortDescriptor alloc] initWithKey:@"category_predicate.categorization_predicate_level" ascending:NO];
    
    [fetchRequestCategories setSortDescriptors:[NSArray arrayWithObject:sortByPredicate]];
    
    NSPredicate *categoryPredicate = [NSPredicate predicateWithFormat:@"category_predicate IN %@",self.menuCategorizationPredicates];
    [fetchRequestCategories setPredicate:categoryPredicate];
    
    NSFetchedResultsController *menuResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequestCategories managedObjectContext:managedObjectContext sectionNameKeyPath:@"category_predicate.categorization_predicate_level" cacheName:nil];
    
    NSLog(@"%@", menuResultsController.sections);
    
    self.jeweleryFetchedResultsController = menuResultsController;
    _jeweleryFetchedResultsController.delegate = self;
    
    return _jeweleryFetchedResultsController;
}

//NSFetchedResultsController Protocol implementation
- (void) controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void) controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
        atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
       newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(MOMenuCell*)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:    
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark -
#pragma mark Application Runtime Data
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"CatalogSegue"]) {
        MOCatalogViewController *catalogViewController = (MOCatalogViewController *)[segue destinationViewController];
        catalogViewController.managedObjectContext = self.managedObjectContext;
        catalogViewController.userPreferencesDictionary = self.userPreferencesDictionary;
        catalogViewController.shoppingCart = self.shoppingCart;
    }
}

#pragma mark
#pragma Menu Cell Protocol
- (void) selectedItemAtIndex:(int)index withIndexPath:(NSIndexPath *)indexPath
{
    MOJeweleryCategory *jeweleryCategory = [_jeweleryFetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:2*[indexPath row] + index inSection:[indexPath section]]];
    
    NSString *parentPredicate = jeweleryCategory.category_predicate.categorization_predicate_name;
    NSNumber *currentlySelectedCategory = [_userPreferencesDictionary valueForKey:parentPredicate];
    
    if (currentlySelectedCategory == jeweleryCategory.category_id) {
        NSArray *cellInfo = [_cellIndexes valueForKey:[NSString stringWithFormat:@"%@",currentlySelectedCategory]];
        NSIndexPath *index  = [cellInfo objectAtIndex:0];
        MOMenuCell *cell = (MOMenuCell*)[_tableView cellForRowAtIndexPath:index];
        if ([cellInfo objectAtIndex:1] == [NSNumber numberWithInt:0]) {
            [cell.categoryButton1Image setImage:[UIImage imageNamed:@"menu_cell"]];
        }
        else
        {
            [cell.categoryButton2Image setImage:[UIImage imageNamed:@"menu_cell"]];
        }
        [_userPreferencesDictionary setValue:[NSNumber numberWithInt:0] forKey:parentPredicate];
        
        return;
    }
    else if (currentlySelectedCategory == nil)
    {
        [_userPreferencesDictionary setValue:jeweleryCategory.category_id forKey:parentPredicate];
        NSArray *cellInfo = [_cellIndexes valueForKey:[NSString stringWithFormat:@"%@",jeweleryCategory.category_id]];
        NSIndexPath *index  = [cellInfo objectAtIndex:0];
        MOMenuCell *cell = (MOMenuCell*)[_tableView cellForRowAtIndexPath:index];
        if ([cellInfo objectAtIndex:1] == [NSNumber numberWithInt:0]) {
            [cell.categoryButton1Image setImage:[UIImage imageNamed:@"Menu_cell2"]];
        }
        else
        {
            [cell.categoryButton2Image setImage:[UIImage imageNamed:@"Menu_cell2"]];
        }
        return;
    }
    else
    {
        [_userPreferencesDictionary setValue:jeweleryCategory.category_id forKey:parentPredicate];
        NSArray *cellInfo = [_cellIndexes valueForKey:[NSString stringWithFormat:@"%@",jeweleryCategory.category_id]];
        NSIndexPath *index  = [cellInfo objectAtIndex:0];
        MOMenuCell *cell = (MOMenuCell*)[_tableView cellForRowAtIndexPath:index];
        if ([cellInfo objectAtIndex:1] == [NSNumber numberWithInt:0]) {
           [cell.categoryButton1Image setImage:[UIImage imageNamed:@"Menu_cell2"]];
        }
        else
        {
            [cell.categoryButton2Image setImage:[UIImage imageNamed:@"Menu_cell2"]];
        }
        
        cellInfo = [_cellIndexes valueForKey:[NSString stringWithFormat:@"%@",currentlySelectedCategory]];
        index  = [cellInfo objectAtIndex:0];
        cell = (MOMenuCell*)[_tableView cellForRowAtIndexPath:index];
        if ([cellInfo objectAtIndex:1] == [NSNumber numberWithInt:0]) {
            [cell.categoryButton1Image setImage:[UIImage imageNamed:@"menu_cell"]];
        }
        else
        {
            [cell.categoryButton2Image setImage:[UIImage imageNamed:@"menu_cell"]];
        }
    }
}
@end
