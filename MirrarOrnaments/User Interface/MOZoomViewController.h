//
//  ZoomingViewController.h
//  TapZoomRotate
//
//  Created by Matt Gallagher on 2010/09/27.
//  Copyright 2010 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"

@interface MOZoomViewController : UIView

@property (nonatomic, strong, readonly) GPUImageView *proxyView;
//@property (nonatomic, retain, readonly) UIView *proxyView2;
@property (nonatomic, strong) GPUImageView *view;
//@property (nonatomic, retain) UIView *view2;
@property (nonatomic, strong) UITapGestureRecognizer *singleTapGestureRecognizer;

- (void)dismissFullscreenView;

@end
