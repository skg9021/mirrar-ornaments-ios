//
//  MODetailImageView.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 21/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOCoreDataPrereqs.h"
#import "MOSemiModal.h"

@protocol MODetailImageViewProtocol <NSObject>
- (void) viewClosed;
- (void) addJeweleryToCart:(MOJewelery *)jewelery;
- (void) tryJewelery:(MOJewelery *)jewelery;
@end

@interface MODetailImageView : MOSemiModalViewController {
    id delegate;
}


@property (nonatomic, strong) IBOutlet id delegate;

@property (weak, nonatomic) IBOutlet UIImageView *detailImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;

@property (strong, nonatomic) MOJewelery *selectedJewelery;

@property (strong, nonatomic) id <MODetailImageViewProtocol> viewDelegate;

- (IBAction)closeDetailView:(id)sender;
- (IBAction)tryJewelery:(id)sender;
- (IBAction)addJeweleryToCart:(id)sender;

@end


