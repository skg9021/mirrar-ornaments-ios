//
//  ZoomingViewController.m
//  TapZoomRotate
//
//  Created by Matt Gallagher on 2010/09/27.
//  Copyright 2010 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

#import "MOZoomViewController.h"

@implementation MOZoomViewController


@synthesize proxyView = _proxyView;
@synthesize view = _view;
@synthesize singleTapGestureRecognizer = _singleTapGestureRecognizer;
//@synthesize proxyView2;
//@synthesize view2;


- (void)toggleZoom:(UITapGestureRecognizer *)gestureRecognizer
{
	if (_proxyView)
	{
		CGRect frame =
			[_proxyView.superview
				convertRect:self.view.frame
				fromView:self.view.window];
		self.view.frame = frame;
		
		CGRect proxyViewFrame = _proxyView.frame;

		[_proxyView.superview addSubview:self.view];
		[_proxyView removeFromSuperview];
		_proxyView = nil;

		[GPUImageView
			animateWithDuration:0.2
			animations:^{
				self.view.frame = proxyViewFrame;
			}];
		
	}		
	else
	{
		_proxyView = [[GPUImageView alloc] initWithFrame:self.view.frame];//CGRectMake(0, 0, 768, 748 )];//
		_proxyView.hidden = YES;
		_proxyView.autoresizingMask = self.view.autoresizingMask;
		[self.view.superview addSubview:_proxyView];
		
		CGRect frame =
			[self.view.window
				convertRect:self.view.frame
				fromView:_proxyView.superview];
		[self.view.window addSubview:self.view];
		self.view.frame = frame;

		[GPUImageView
			animateWithDuration:0.2
			animations:^{
				self.view.frame = self.view.window.bounds;
			}];

	}
	

}

- (void)dismissFullscreenView
{
	if (_proxyView)
	{
		[self toggleZoom:nil];
	}
}

- (void)setView:(GPUImageView *)newView
{
	if (_view)
	{
		[self toggleZoom:nil];
		[_view removeGestureRecognizer:_singleTapGestureRecognizer];
		_singleTapGestureRecognizer = nil;
	}
	
	_view = newView;
	
	_singleTapGestureRecognizer =
		[[UITapGestureRecognizer alloc]
			initWithTarget:self action:@selector(toggleZoom:)];
	_singleTapGestureRecognizer.numberOfTapsRequired = 1;
	
	//[self.view addGestureRecognizer:_singleTapGestureRecognizer];
}
@end

