//
//  MOCatalogTableViewCell.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 20/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//
#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)

#import "MOCatalogTableViewCell.h"

@implementation MOCatalogTableViewCell

@synthesize delegate;
@synthesize indexPath;
@synthesize ornament1;
@synthesize ornament2;
@synthesize ornament3;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)ornament1Clicked:(id)sender {
    [delegate selectedItemAtIndex:0 withIndexPath:indexPath];
}

- (IBAction)ornament2Clicked:(id)sender {
    [delegate selectedItemAtIndex:1 withIndexPath:indexPath];
}

- (IBAction)ornament3Clicked:(id)sender {
    [delegate selectedItemAtIndex:2 withIndexPath:indexPath];
}
@end
