//
//  TDSemiModalViewController.m
//  TDSemiModal
//
//  Created by Nathan  Reed on 18/10/10.
//  Copyright 2010 Nathan Reed. All rights reserved.
//

#import "MOSemiModalViewController.h"

@implementation MOSemiModalViewController
@synthesize coverView;

-(void)viewDidLoad {
    [super viewDidLoad];
	self.coverView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
	//self.coverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
	coverView.backgroundColor = UIColor.blackColor;

	self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.coverView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

}

- (CGRect) rect
{
    return _rect;
}

- (void) setRect:(CGRect)rect
{
    _rect = rect;
}

- (void)viewDidUnload {
    [super viewDidUnload];
    [self.coverView removeFromSuperview];
	self.coverView = nil;
}


@end
