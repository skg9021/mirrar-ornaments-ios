//
//  TapZoomRotateViewController.h
//  TapZoomRotate
//
//  Created by Matt Gallagher on 2010/09/27.
//  Copyright 2010 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"
#import "ComputePoints.h"

@class MOZoomViewController;

@interface MOCompareViewController : UIViewController
{
	MOZoomViewController *zoomingViewController;
    	MOZoomViewController *zoomingViewController2;
	MOZoomViewController *zoomingViewController3;
    	MOZoomViewController *zoomingViewController4;
	GPUImageView *zoomingView;
    GPUImageView *zoomingView2;
    GPUImageView *zoomingView3;
    GPUImageView *zoomingView4;

    CGPoint leftHingePoint;
    CGPoint rightHingePoint;
    
    UIPinchGestureRecognizer *pinchGesture;
    UIPinchGestureRecognizer *pinchGesture2;
    UIPinchGestureRecognizer *pinchGesture3;
    UIPinchGestureRecognizer *pinchGesture4;

    UITapGestureRecognizer *singleTapGestureRecognizer;
    UITapGestureRecognizer *singleTapGestureRecognizer2;
    UITapGestureRecognizer *singleTapGestureRecognizer3;
    UITapGestureRecognizer *singleTapGestureRecognizer4;
    
    bool fullScreen;
    //CGPoint leftHingePoint, rightHingePoint;
    CGRect frame;
    bool tappedGesture;
    
@public
    float mLastScale, mCurrentScale;
    CGPoint mImagePositionGL;
    float imageScale;
    
}

- (IBAction)goToCart:(id)sender;
- (void)needToInitPressed;
- (void)handlePinch:(UIPinchGestureRecognizer *)sender;
- (void)calibrate;

@property (nonatomic, retain) IBOutlet GPUImageView *zoomingView;
@property (nonatomic, retain) IBOutlet GPUImageView *zoomingView2;
@property (nonatomic, retain) IBOutlet GPUImageView *zoomingView3;
@property (nonatomic, retain) IBOutlet GPUImageView *zoomingView4;
@property (nonatomic, retain) ES2Renderer *renderer;
@property (nonatomic, retain) ES2Renderer *renderer2;
@property (nonatomic, retain) ES2Renderer *renderer3;
@property (nonatomic, retain) ES2Renderer *renderer4;
@property (nonatomic, retain) GPUImageTextureInput *textureInput;
@property (nonatomic, retain) GPUImageTextureInput *textureInput2;
@property (nonatomic, retain) GPUImageTextureInput *textureInput3;
@property (nonatomic, retain) GPUImageTextureInput *textureInput4;
@property NSDate *startTime;

@property (nonatomic, strong) UITapGestureRecognizer *singleTapGestureRecognize;
@property (nonatomic, strong) ComputePoints *computePointsEngine;
@property (nonatomic, strong) NSMutableArray *renderers;
@property (nonatomic, strong) NSMutableArray *jeweleryArray;
@property (strong,nonatomic) id <SliderProtocol> delegate;
@property (nonatomic, strong, readonly) GPUImageView *proxyView;


@end


