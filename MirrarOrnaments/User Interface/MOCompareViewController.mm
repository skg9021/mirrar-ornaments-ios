//
//  TapZoomRotateViewController.m
//  TapZoomRotate
//
//  Created by Matt Gallagher on 2010/09/27.
//  Copyright 2010 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (sin part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

#import "MOCompareViewController.h"
#import "MOZoomViewController.h"
#import "MOSplitViewController.h"
#import "MOJewelery.h"

@interface MOCompareViewController ()

@property (strong, nonatomic) NSSet *hingePoints;
@property (strong,nonatomic) NSSortDescriptor *sort;
@property (strong, nonatomic) NSArray *hingePointsArray;
@property (strong, nonatomic) MOJeweleryHingePoint *leftPoint;
@property (strong, nonatomic) MOJeweleryHingePoint *rightPoint;
@property CGPoint lastMovementPosition;


@end

@implementation MOCompareViewController

@synthesize hingePoints;
@synthesize sort;
@synthesize hingePointsArray;
@synthesize leftPoint;
@synthesize rightPoint;
@synthesize lastMovementPosition;

@synthesize zoomingView;
@synthesize zoomingView2;
@synthesize zoomingView3;
@synthesize zoomingView4;

@synthesize singleTapGestureRecognize;
@synthesize computePointsEngine;
@synthesize renderers;
@synthesize delegate;
@synthesize jeweleryArray = _jeweleryArray;
@synthesize proxyView=_proxyView;

@synthesize renderer;
@synthesize textureInput;
@synthesize startTime;
@synthesize renderer2;
@synthesize textureInput2;
@synthesize renderer3;
@synthesize textureInput3;
@synthesize renderer4;
@synthesize textureInput4;


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return NO;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    computePointsEngine = [[ComputePoints alloc] initWithSize:CGSizeMake(1536, 2048)];
    self.delegate = computePointsEngine;
    renderers = [[NSMutableArray alloc] init];
    [computePointsEngine startCameraCapture];
    mLastScale = 1.0f;
    mCurrentScale = 1.0f;
    
    mImagePositionGL = CGPointMake(0, 0);
    imageScale = 1.0;
    return self;
}
- (void)loadView
{
    [super loadView];
    
    //[zoomingView initWithFrame:CGRectMake(0, 44, 768, 1960)];
}

- (void) viewWillAppear:(BOOL)animated
{
    int noOfJewelry = [_jeweleryArray count];
    if ([[computePointsEngine renderers] count] != 0) {
        [[computePointsEngine renderers] removeAllObjects];
    }
    //////////////////////////***** First View *****//////////////////////////
	zoomingViewController = [[MOZoomViewController alloc] init];
	zoomingViewController.view = zoomingView;
	
	//CGRect zoomingViewFrame = zoomingViewController.view.frame;
    /*zoomingViewController.view.frame = CGRectMake(
     zoomingViewFrame.origin.x,zoomingViewFrame.origin.y,
     zoomingViewFrame.size.width,
     zoomingViewFrame.size.height);
     */
    
    
    [zoomingView setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    __weak ES2Renderer *weakRenderer;
    MOJewelery *jewelry;
    
    if (noOfJewelry>0) {
        
        jewelry = [_jeweleryArray objectAtIndex:0];
        noOfJewelry--;
        
        hingePoints = jewelry.jewelery_image.hinge_point_set.hinge_points;
        sort = [[NSSortDescriptor alloc]
                initWithKey:@"hinge_point_x" ascending:YES];
        hingePointsArray = [hingePoints sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
        
        leftPoint = [hingePointsArray objectAtIndex:0];
        
        rightPoint = [hingePointsArray objectAtIndex:1];
        
        leftHingePoint = CGPointMake([leftPoint.hinge_point_x intValue], [leftPoint.hinge_point_y intValue]);
        
        rightHingePoint = CGPointMake([rightPoint.hinge_point_x intValue], [rightPoint.hinge_point_y intValue]);
        
        
        // Renderer
        renderer = [[ES2Renderer alloc] initWithSize:[zoomingView sizeInPixels] jewelryName:[[jewelry.jewelery_image.jewelery_image_filename copy] stringByAppendingString:@".png"] leftPoint:leftHingePoint rightPoint:rightHingePoint];
        
        [renderer setComputePoints:computePointsEngine];
        
        renderers = [computePointsEngine renderers];
        
        weakRenderer = renderer;
        [renderers addObject:weakRenderer];
        
        textureInput = [[GPUImageTextureInput alloc] initWithTexture:renderer.outputTexture size:[zoomingView sizeInPixels]];
        
        [textureInput addTarget:zoomingView];
        
        __weak NSDate *weakStartTime = startTime;
        __weak GPUImageTextureInput *weakTextureInput = textureInput;
        [renderer setNewFrameAvailableBlock:^{
            float currentTimeInMilliseconds = [[NSDate date] timeIntervalSinceDate:weakStartTime] * 1000.0;
            
            [weakTextureInput processTextureWithFrameTime:CMTimeMake((int)currentTimeInMilliseconds, 1000)];
        }];
        
    }
    
    
    //////////////////////////***** Second View *****//////////////////////////
    zoomingViewController2 = [[MOZoomViewController alloc] init];
	zoomingViewController2.view = zoomingView2;
	
	//CGRect zoomingViewFrame2 = zoomingViewController2.view.frame;
    /*zoomingViewController2.view.frame = CGRectMake(
     zoomingViewFrame2.origin.x,zoomingViewFrame2.origin.y,
     zoomingViewFrame2.size.width,
     zoomingViewFrame2.size.height);
     */
    
    [zoomingView2 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    if (noOfJewelry>0) {
        
        jewelry = [_jeweleryArray objectAtIndex:1];
        noOfJewelry--;
        
        hingePoints = jewelry.jewelery_image.hinge_point_set.hinge_points;
        sort = [[NSSortDescriptor alloc]
                initWithKey:@"hinge_point_x" ascending:YES];
        hingePointsArray = [hingePoints sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
        
        leftPoint = [hingePointsArray objectAtIndex:0];
        
        rightPoint = [hingePointsArray objectAtIndex:1];
        
        leftHingePoint = CGPointMake([leftPoint.hinge_point_x intValue], [leftPoint.hinge_point_y intValue]);
        
        rightHingePoint = CGPointMake([rightPoint.hinge_point_x intValue], [rightPoint.hinge_point_y intValue]);
        
        
        // Renderer2
        renderer2 = [[ES2Renderer alloc] initWithSize:[zoomingView2 sizeInPixels] jewelryName:[[jewelry.jewelery_image.jewelery_image_filename copy] stringByAppendingString:@".png"] leftPoint:leftHingePoint rightPoint:rightHingePoint];
        
        [renderer2 setComputePoints:computePointsEngine];
        
        renderers = [computePointsEngine renderers];
        
        weakRenderer = renderer2;
        [renderers addObject:weakRenderer];
        
        textureInput2 = [[GPUImageTextureInput alloc] initWithTexture:renderer2.outputTexture size:[zoomingView2 sizeInPixels]];
        
        [textureInput2 addTarget:zoomingView2];
        
        __weak NSDate *weakStartTime = startTime;
        __weak GPUImageTextureInput *weakTextureInput = textureInput2;
        [renderer2 setNewFrameAvailableBlock:^{
            float currentTimeInMilliseconds = [[NSDate date] timeIntervalSinceDate:weakStartTime] * 1000.0;
            
            [weakTextureInput processTextureWithFrameTime:CMTimeMake((int)currentTimeInMilliseconds, 1000)];
        }];
        
    }
    
    
    //////////////////////////***** Third View *****//////////////////////////
    zoomingViewController3 = [[MOZoomViewController alloc] init];
	zoomingViewController3.view = zoomingView3;
	
	//CGRect zoomingViewFrame3 = zoomingViewController3.view.frame;
    /*zoomingViewController3.view.frame = CGRectMake(
     zoomingViewFrame3.origin.x,zoomingViewFrame3.origin.y,
     zoomingViewFrame3.size.width,
     zoomingViewFrame3.size.height);
     */
    
    [zoomingView3 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    if (noOfJewelry>0) {
        
        jewelry = [_jeweleryArray objectAtIndex:2];
        noOfJewelry--;
        
        hingePoints = jewelry.jewelery_image.hinge_point_set.hinge_points;
        sort = [[NSSortDescriptor alloc]
                initWithKey:@"hinge_point_x" ascending:YES];
        hingePointsArray = [hingePoints sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
        
        leftPoint = [hingePointsArray objectAtIndex:0];
        
        rightPoint = [hingePointsArray objectAtIndex:1];
        
        leftHingePoint = CGPointMake([leftPoint.hinge_point_x intValue], [leftPoint.hinge_point_y intValue]);
        
        rightHingePoint = CGPointMake([rightPoint.hinge_point_x intValue], [rightPoint.hinge_point_y intValue]);
        
        
        // Renderer3
        renderer3 = [[ES2Renderer alloc] initWithSize:[zoomingView3 sizeInPixels] jewelryName:[[jewelry.jewelery_image.jewelery_image_filename copy] stringByAppendingString:@".png"] leftPoint:leftHingePoint rightPoint:rightHingePoint];
        
        [renderer3 setComputePoints:computePointsEngine];
        
        renderers = [computePointsEngine renderers];
        
        weakRenderer = renderer3;
        [renderers addObject:weakRenderer];
        
        textureInput3 = [[GPUImageTextureInput alloc] initWithTexture:renderer3.outputTexture size:[zoomingView3 sizeInPixels]];
        
        [textureInput3 addTarget:zoomingView3];
        
        __weak NSDate *weakStartTime = startTime;
        __weak GPUImageTextureInput *weakTextureInput = textureInput3;
        [renderer3 setNewFrameAvailableBlock:^{
            float currentTimeInMilliseconds = [[NSDate date] timeIntervalSinceDate:weakStartTime] * 1000.0;
            
            [weakTextureInput processTextureWithFrameTime:CMTimeMake((int)currentTimeInMilliseconds, 1000)];
        }];
        
    }
    
    
    //////////////////////////***** Fourth View *****//////////////////////////
    zoomingViewController4 = [[MOZoomViewController alloc] init];
	zoomingViewController4.view = zoomingView4;
	
	//CGRect zoomingViewFrame4 = zoomingViewController4.view.frame;
    /*zoomingViewController4.view.frame = CGRectMake(
     zoomingViewFrame4.origin.x,zoomingViewFrame4.origin.y,
     zoomingViewFrame4.size.width,
     zoomingViewFrame4.size.height);
     */
    
    [zoomingView4 setFillMode:kGPUImageFillModePreserveAspectRatioAndFill];
    
    
    if (noOfJewelry>0) {
        
        jewelry = [_jeweleryArray objectAtIndex:3];
        noOfJewelry--;
        
        hingePoints = jewelry.jewelery_image.hinge_point_set.hinge_points;
        sort = [[NSSortDescriptor alloc]
                initWithKey:@"hinge_point_x" ascending:YES];
        hingePointsArray = [hingePoints sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
        
        leftPoint = [hingePointsArray objectAtIndex:0];
        
        rightPoint = [hingePointsArray objectAtIndex:1];
        
        leftHingePoint = CGPointMake([leftPoint.hinge_point_x intValue], [leftPoint.hinge_point_y intValue]);
        
        rightHingePoint = CGPointMake([rightPoint.hinge_point_x intValue], [rightPoint.hinge_point_y intValue]);
        
        
        // Renderer4
        renderer4 = [[ES2Renderer alloc] initWithSize:[zoomingView4 sizeInPixels] jewelryName:[[jewelry.jewelery_image.jewelery_image_filename copy] stringByAppendingString:@".png"] leftPoint:leftHingePoint rightPoint:rightHingePoint];
        
        [renderer4 setComputePoints:computePointsEngine];
        
        renderers = [computePointsEngine renderers];
        
        weakRenderer = renderer4;
        [renderers addObject:weakRenderer];
        
        textureInput4 = [[GPUImageTextureInput alloc] initWithTexture:renderer4.outputTexture size:[zoomingView4 sizeInPixels]];
        
        [textureInput4 addTarget:zoomingView4];
        
        __weak NSDate *weakStartTime = startTime;
        __weak GPUImageTextureInput *weakTextureInput = textureInput4;
        
        [renderer4 setNewFrameAvailableBlock:^{
            float currentTimeInMilliseconds = [[NSDate date] timeIntervalSinceDate:weakStartTime] * 1000.0;
            
            [weakTextureInput processTextureWithFrameTime:CMTimeMake((int)currentTimeInMilliseconds, 1000)];
        }];
        
        
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////
    
    pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    pinchGesture2 = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    pinchGesture3 = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    pinchGesture4 = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    
    [zoomingView addGestureRecognizer:pinchGesture];
    [zoomingView2 addGestureRecognizer:pinchGesture2];
    [zoomingView3 addGestureRecognizer:pinchGesture3];
    [zoomingView4 addGestureRecognizer:pinchGesture4];
    
    singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleZooms:)];
    singleTapGestureRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleZooms2:)];
    singleTapGestureRecognizer3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleZooms3:)];
    singleTapGestureRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleZooms4:)];
    
	singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer2.numberOfTapsRequired = 1;
    singleTapGestureRecognizer3.numberOfTapsRequired = 1;
    singleTapGestureRecognizer4.numberOfTapsRequired = 1;
    
	[zoomingView addGestureRecognizer:singleTapGestureRecognizer];
    [zoomingView2 addGestureRecognizer:singleTapGestureRecognizer2];
    [zoomingView3 addGestureRecognizer:singleTapGestureRecognizer3];
    [zoomingView4 addGestureRecognizer:singleTapGestureRecognizer4];
    
    
    zoomingView.userInteractionEnabled = YES;
    zoomingView2.userInteractionEnabled = YES;
    zoomingView3.userInteractionEnabled = YES;
    zoomingView4.userInteractionEnabled = YES;
    
    
    zoomingViewController.view.frame = CGRectMake(0, 0, 383, 533);
    zoomingViewController2.view.frame = CGRectMake(385, 0, 383, 533);
    zoomingViewController3.view.frame = CGRectMake(0, 491, 383, 533);
    zoomingViewController4.view.frame = CGRectMake(385, 491, 383, 533);
    
    
    self.navigationItem.hidesBackButton = YES;
    tappedGesture = false;

}
- (void)viewWillDisappear:(BOOL)animated
{
    /*
	[zoomingViewController dismissFullscreenView];
    	[zoomingViewController2 dismissFullscreenView];
    	[zoomingViewController3 dismissFullscreenView];
    	[zoomingViewController4 dismissFullscreenView];
     */
}

- (void)viewDidUnload
{
	// Release any retained subviews of the main view.
	zoomingViewController = nil;
    zoomingViewController2 = nil;
    zoomingViewController3 = nil;
    zoomingViewController4 = nil;
	self.zoomingView = nil;
    self.zoomingView2 = nil;
    self.zoomingView3 = nil;
    self.zoomingView4 = nil;
}

- (IBAction)goToCart:(id)sender {
    
    [computePointsEngine->videoCamera stopCameraCapture];
    UINavigationController *navigationController = self.navigationController;
    [navigationController popViewControllerAnimated:YES];
    
    renderer = nil;
    renderer2 = nil;
    renderer3 = nil;
    renderer4 = nil;
    
    [delegate performSelector:@selector(stopTracking:) withObject:[NSNumber numberWithBool:YES]];
    dispatch_release(computePointsEngine.faceQueue);
    computePointsEngine.faceQueue = nil;
    computePointsEngine = nil;
}

/*
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    
    [self setCalibrateRendererView:nil];
    [renderers removeAllObjects];
    calibrateRenderer = nil;
    //computePointsEngine.faceQueue = nil;
    //dispatch_suspend(computePointsEngine.faceQueue);
    
    //computePointsEngine.faceQueue = nil;
    computePointsEngine = nil;
    [delegate performSelector:@selector(stopTracking:) withObject:[NSNumber numberWithBool:YES]];
    dispatch_release(computePointsEngine.faceQueue);
    computePointsEngine.faceQueue = nil;
}
*/

- (void)needToInitPressed
{
    [delegate performSelector:@selector(needToInit:) withObject:[NSNumber numberWithBool:YES]];
}

- (void)handlePinch:(UIPinchGestureRecognizer *)sender
{
    [delegate performSelector:@selector(stopTracking:) withObject:[NSNumber numberWithBool:YES]];
    //NSLog(@"lastScale = %f",mLastScale);
    
    mCurrentScale = [sender scale] - mLastScale;
    mLastScale = [sender scale];
    
    //NSLog(@"Current Scale = %f",mCurrentScale);
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        mLastScale = 1.0f;
    }
    
    imageScale += mCurrentScale;
    //NSLog(@"Current scale from calib window : %f",imageScale);
    //[calibrateRenderer renderByRotatingAroundX:0.0 rotatingAroundY:0.0 scale:mCurrentScale];
    [renderer renderByRotatingAroundX:0.0 rotatingAroundY:0.0 scale:mCurrentScale];
    [renderer2 renderByRotatingAroundX:0.0 rotatingAroundY:0.0 scale:mCurrentScale];
    [renderer3 renderByRotatingAroundX:0.0 rotatingAroundY:0.0 scale:mCurrentScale];
    [renderer4 renderByRotatingAroundX:0.0 rotatingAroundY:0.0 scale:mCurrentScale];
    
}

- (void)toggleZooms:(UITapGestureRecognizer *)gestureRecognizer
{
    tappedGesture = true;
    
    MOSplitViewController *bigView = [[MOSplitViewController alloc] initWithNibName:@"MOSplitJeweleryTrial" bundle:nil];
    [bigView setSelectedJewelery:[_jeweleryArray objectAtIndex:0]];
    [bigView setComputePointsEngine:[self computePointsEngine]];
    bigView->mLastScale = mLastScale;
    bigView->mCurrentScale = mCurrentScale;
    bigView->mImagePositionGL = mImagePositionGL;
    bigView->imageScale = imageScale;
    [bigView setCompareView:self];
    [self.navigationController pushViewController:bigView animated:YES];
}

- (void)toggleZooms2:(UITapGestureRecognizer *)gestureRecognizer
{
    tappedGesture = true;
    
    MOSplitViewController *bigView = [[MOSplitViewController alloc] initWithNibName:@"MOSplitJeweleryTrial" bundle:nil];
    [bigView setSelectedJewelery:[_jeweleryArray objectAtIndex:1]];
    [bigView setComputePointsEngine:[self computePointsEngine]];
    bigView->mLastScale = mLastScale;
    bigView->mCurrentScale = mCurrentScale;
    bigView->mImagePositionGL = mImagePositionGL;
    bigView->imageScale = imageScale;
    [bigView setCompareView:self];
    [self.navigationController pushViewController:bigView animated:YES];
}

- (void)toggleZooms3:(UITapGestureRecognizer *)gestureRecognizer
{
    tappedGesture = true;
    
    MOSplitViewController *bigView = [[MOSplitViewController alloc] initWithNibName:@"MOSplitJeweleryTrial" bundle:nil];
    [bigView setSelectedJewelery:[_jeweleryArray objectAtIndex:2]];
    [bigView setComputePointsEngine:[self computePointsEngine]];
    bigView->mLastScale = mLastScale;
    bigView->mCurrentScale = mCurrentScale;
    bigView->mImagePositionGL = mImagePositionGL;
    bigView->imageScale = imageScale;
    [bigView setCompareView:self];
    [self.navigationController pushViewController:bigView animated:YES];
}

- (void)toggleZooms4:(UITapGestureRecognizer *)gestureRecognizer
{
    
    tappedGesture = true;
    
    MOSplitViewController *bigView = [[MOSplitViewController alloc] initWithNibName:@"MOSplitJeweleryTrial" bundle:nil];
    [bigView setSelectedJewelery:[_jeweleryArray objectAtIndex:3]];
    [bigView setComputePointsEngine:[self computePointsEngine]];
    bigView->mLastScale = mLastScale;
    bigView->mCurrentScale = mCurrentScale;
    bigView->mImagePositionGL = mImagePositionGL;
    bigView->imageScale = imageScale;
    [bigView setCompareView:self];
    [self.navigationController pushViewController:bigView animated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSMutableSet *currentTouches = [[event touchesForView:self.view] mutableCopy];
    [currentTouches minusSet:touches];
	
	// New touches are not yet included in the current touches for the view
	lastMovementPosition = [[touches anyObject] locationInView:self.view];
    
    
    
    if (renderer) {
        mImagePositionGL = CGPointMake(renderer->currentJewelryModelViewMatrix[12], renderer->currentJewelryModelViewMatrix[13]);
        
    }
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
{
    if (tappedGesture==false) {
        [delegate performSelector:@selector(stopTracking:) withObject:[NSNumber numberWithBool:YES]];
    }
    
	CGPoint currentMovementPosition = [[touches anyObject] locationInView:self.view];
    float currentPositionX = (currentMovementPosition.x-lastMovementPosition.x);
    float currentPositionY = (currentMovementPosition.y-lastMovementPosition.y);
    
    [renderer renderByRotatingAroundX:currentPositionX rotatingAroundY:currentPositionY scale:mCurrentScale];
    [renderer2 renderByRotatingAroundX:currentPositionX rotatingAroundY:currentPositionY scale:mCurrentScale];
    [renderer3 renderByRotatingAroundX:currentPositionX rotatingAroundY:currentPositionY scale:mCurrentScale];
    [renderer4 renderByRotatingAroundX:currentPositionX rotatingAroundY:currentPositionY scale:mCurrentScale];
    
    
    mImagePositionGL = CGPointMake(mImagePositionGL.x + ((currentPositionX*2)/1536)*2, mImagePositionGL.y + ((currentPositionY*2)/2048)*2);
    
    //NSLog(@"Position from calib window X:%f \t Y:%f",mImagePositionGL.x,mImagePositionGL.y);
    
	lastMovementPosition = currentMovementPosition;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSMutableSet *remainingTouches = [[event touchesForView:self.view] mutableCopy];
    [remainingTouches minusSet:touches];
    
	lastMovementPosition = [[remainingTouches anyObject] locationInView:self.view];
    
    if (tappedGesture==false) {
        [self calibrate];
    }
    else if (tappedGesture==true) {
        tappedGesture = false;
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	// Handle touches canceled the same as as a touches ended event
    [self touchesEnded:touches withEvent:event];
}

- (void)calibrate
{
    MOJewelery *jewelry = [_jeweleryArray objectAtIndex:1];
    
    size_t imgWidth = [jewelry.jewelery_image.jewelery_image_width intValue];
    size_t imgHeight = [jewelry.jewelery_image.jewelery_image_height intValue];
    
    //correct/size_t imgWidth = [_selectedJewelery.jewelery_image.jewelery_image_width intValue];
    
    //correct/size_t imgHeight = [_selectedJewelery.jewelery_image.jewelery_image_height intValue];
    
    
    NSLog(@"Position from calib window X:%f \t Y:%f",mImagePositionGL.x,mImagePositionGL.y);
    NSLog(@"Current scale from calib window : %f",imageScale);
    
    
    [delegate performSelector:@selector(setleftHingePoint:rightHingePoint:) withObject:[NSValue valueWithCGPoint:leftHingePoint] withObject:[NSValue valueWithCGPoint:rightHingePoint]];
    
    
    //correct/[delegate performSelector:@selector(setImageWidth:ImageHeight:) withObject:[NSNumber numberWithFloat:imgWidth] withObject:[NSNumber numberWithFloat:imgHeight]];
    [delegate performSelector:@selector(setImageWidth:ImageHeight:) withObject:[NSNumber numberWithFloat:imgWidth] withObject:[NSNumber numberWithFloat:imgHeight]];
    
    [delegate performSelector:@selector(setImageScale:) withObject:[NSNumber numberWithFloat:imageScale]];
    
    [delegate performSelector:@selector(computeCLickPoints:) withObject:[NSValue valueWithCGPoint:mImagePositionGL]];
    
    [self needToInitPressed];
    
}

@end
