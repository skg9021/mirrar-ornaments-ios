//
//  MOCartViewCell.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 24/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOJewelery.h"

@protocol MOCartViewCellProtocol <NSObject>

- (void) removeButtonClicked:(NSIndexPath*)indexPath;

@end

@interface MOCartViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *detailImageView;
@property (weak, nonatomic) IBOutlet UILabel *captionText;
@property (weak, nonatomic) IBOutlet UITextView *description;
@property (weak, nonatomic) IBOutlet UIButton *tryButton;
@property (weak, nonatomic) IBOutlet UIButton *detailsButton;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;

- (IBAction)tryButtonClicked:(id)sender;
- (IBAction)detailsButtonClicked:(id)sender;
- (IBAction)removeButtonClicked:(id)sender;

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) id<MOCartViewCellProtocol> delegate;
@property (nonatomic, strong) MOJewelery *jewelery;

@property BOOL details;
@end
