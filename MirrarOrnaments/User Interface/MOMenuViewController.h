//
//  MOMenuViewController.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOCoreDataPrereqs.h"
#import "MOMenuCell.h"
#import "MOFileSystemManager.h"

@interface MOMenuViewController : UITableViewController <NSFetchedResultsControllerDelegate, UITableViewDelegate, MOMenuCellProtocol>
@property (nonatomic, strong) NSString *viewTitle;
@property (strong, nonatomic) IBOutlet UITableView *tableView;


// Core Data
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSFetchedResultsController *jeweleryFetchedResultsController;

@property (nonatomic, retain) NSArray *menuCategorizationPredicates;

// Application Preferences Dictionary
@property (strong) NSMutableDictionary           *userPreferencesDictionary;
@property (strong) NSMutableArray           *shoppingCart;
@property (strong) NSMutableDictionary           *cellIndexes;

@property (strong) MOFileSystemManager *fileSystemManager;
@end
