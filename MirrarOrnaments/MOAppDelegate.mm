//
//  MOAppDelegate.m
//  MirrarOrnaments
//
//  Created by skg on 9/19/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOAppDelegate.h"
#import "MOUsers.h"
#import "MOLoginScreenViewController.h"

@implementation MOAppDelegate

@synthesize window                     =    _window;
@synthesize updateManager              =    _updateManager;
@synthesize managedObjectContext       =    _managedObjectContext;
@synthesize managedObjectModel         =    _managedObjectModel;
@synthesize persistentStoreCoordinator =    _persistentStoreCoordinator;
@synthesize updateAlertView            =    _updateAlertView;


@synthesize menuCategorizationPredicates    =   _menuCategorizationPredicates;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    _updateManager = [[MOUpdateManager alloc] initWithDelegate:self];
    BOOL answer;
    NSError *error;
    NSDictionary *urls = [NSDictionary dictionaryWithObjectsAndKeys:
                          @"www.google.com",@"Google",
                          @"aws.amazon.com", @"Amazon",
                          @"www.rhapsodylabs.com", @"Rhapsody Labs",
                          nil];
    
    answer = [_updateManager runReachabilityTestsOnURLs:urls
                                       didFailWithError:&error];
    if (!answer) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No network connection"
                                                        message:@"For best results please connect to the internet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            [alert show];
        });
    }
    else
        [_updateManager updateIsAvailable];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // Check if the app has run before by checking a key in user defaults
    if ([prefs boolForKey:@"hasRunBefore"] != YES) {
        // set the flags so we know not to run this next time
        [prefs setBool:YES forKey:@"hasRunBefore"];
        [prefs synchronize];
        
        // Add our default user object in the Core Data
        MOUsers *user = (MOUsers *)[NSEntityDescription insertNewObjectForEntityForName:@"MOUsers" inManagedObjectContext:self.managedObjectContext];
        [user setUsername:@"n"];
        [user setPassword:@"n"];
        
        //Commit to core data
        NSError *error;
        if(![self.managedObjectContext save:&error])
            NSLog(@"Failed to add default user with error: %@ , %@", error, [error userInfo]);
    }
    
    // Pass the managed object context to the root view controller (the login view)
    MOLoginScreenViewController *rootView = (MOLoginScreenViewController *)self.window.rootViewController;
    rootView.managedObjectContext = self.managedObjectContext;
    
    [self customizeAppearance];
    [self precomputeHierarchy];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -
#pragma mark Apply Update
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if (![title isEqualToString:@"Cancel"]) {
        NSLog(@"OK button pressed");
        _updateAlertView = [[UIAlertView alloc] initWithTitle:@"Updating the System\nPlease Wait..."
                                                      message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
        [_updateAlertView show];
        
        UIActivityIndicatorView *updateAlertIndicator = [[UIActivityIndicatorView alloc]
                                                         initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        CGPoint point = CGPointMake(140, 90);
        updateAlertIndicator.center = point;
        [updateAlertIndicator startAnimating];
        [_updateAlertView addSubview:updateAlertIndicator];
        
        
        NSLog(@"Update Starting..");
        NSError *error;
        [_updateManager updateUsingMOC:self.managedObjectContext didFailWithError:&error];
    }
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *) managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *) managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MODataModel"
                                              withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *) persistentStoreCoordinator
{
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory]
                       URLByAppendingPathComponent:@"MODataModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil URL:storeURL options:nil error:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (MOUpdateManager *) updateManager
{
    if(_updateManager) {
        return _updateManager;
    }
    
    _updateManager = [[MOUpdateManager alloc] initWithDelegate:self];
    return _updateManager;
}

-(void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

#pragma mark -
#pragma mark Update Manager Delegate
- (void) updateAvailable:(NSNumber *)count
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@ Available",[count intValue]==1?@"Update":@"Updates"]
                                                    message:[NSString stringWithFormat:@"%@ %@ found.\n Press OK to apply the update, or Press Cancel to proceed.",count,[count intValue]==1?@"update":@"updates"] 
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [alert show];
    });
    
}

- (void) updateCompleted
{
    NSLog(@"I was here");
    [self saveContext];
    [_updateAlertView dismissWithClickedButtonIndex:0 animated:YES];
}

- (void) updateFailed:(NSString *)reason
{
    NSLog(@"%@", reason);
}


#pragma mark -
#pragma mark Appearance Customization
-(void) customizeAppearance
{
    UIImage *gradientImage96 = [[UIImage imageNamed:@"nav_bar"]
                                resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [[UINavigationBar appearance] setBackgroundImage:gradientImage96 forBarMetrics:UIBarMetricsDefault];
    /*    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
     [UIColor colorWithRed:220.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0], UITextAttributeTextColor,
     [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0], UITextAttributeTextShadowColor,
     [UIFont fontWithName:@"Helvetica-Bold" size:25.0], UITextAttributeFont,
     nil] forState:UIControlStateNormal];*/
    // Customize UIBarButtonItems
    UIImage *buttonNormal = [[UIImage imageNamed:@"done_big"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
    UIImage *buttonSelected = [[UIImage imageNamed:@"done_big_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
    
    [[UIBarButtonItem appearance] setBackgroundImage:buttonNormal forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackgroundImage:buttonSelected forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    // Customize back button items differently
    UIImage *buttonBackNormal = [[UIImage imageNamed:@"back"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 5)];
    UIImage *buttonBackSelected = [[UIImage imageNamed:@"back_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 12, 0, 5)];
    
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:buttonBackNormal forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:buttonBackSelected forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    
}

#pragma mark -
#pragma mark Hierarchy Computation
- (void) precomputeHierarchy
{
    /*
     NSMutableDictionary *categorywiseJewelery = [[NSMutableDictionary alloc] initWithCapacity:1];
     
     // Fetch all the categories the menu
     NSFetchRequest *fetchRequestCategorizationPredicates = [[NSFetchRequest alloc] init];
     NSEntityDescription *categorizationPredicate = [NSEntityDescription entityForName:@"MOJeweleryCategorizationPredicate" inManagedObjectContext:managedObjectContext];
     [fetchRequestCategorizationPredicates setEntity:categorizationPredicate];
     [fetchRequestCategorizationPredicates setReturnsDistinctResults:YES];
     //[fetchRequestCategorizationPredicates returnsObjectsAsFaults:NO];
     
     NSSortDescriptor *sortByPredicate = [[NSSortDescriptor alloc] initWithKey:@"categorization_predicate_level" ascending:NO];
     
     [fetchRequestCategorizationPredicates setSortDescriptors:[NSArray arrayWithObject:sortByPredicate]];
     
     NSExpression *exprCategorizationPredicateLevel = [NSExpression expressionForKeyPath:@"categorization_predicate_level"];
     NSExpression *exprTargetPredicateLevel = [NSExpression expressionForConstantValue:[NSNumber numberWithInt:0]];
     NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprCategorizationPredicateLevel
     rightExpression:exprTargetPredicateLevel modifier:NSDirectPredicateModifier
     type:NSGreaterThanPredicateOperatorType options:0];
     
     [fetchRequestCategorizationPredicates setPredicate:predicate];
     
     
     
     MOJeweleryCategorizationPredicate *cpredicate = [_menuCategorizationPredicates objectAtIndex:0];
     MOJeweleryCategory *category1 =  [[cpredicate.predicate_result_set allObjects] objectAtIndex:0];
     MOJeweleryCategory *category2 =  [[cpredicate.predicate_result_set allObjects] objectAtIndex:1];
     
     NSLog(@"%@, %@", category1, category2);
     NSMutableSet *jewelery = [NSMutableSet setWithSet:category1.category_jewelery_set];
     [jewelery intersectSet:category2.category_jewelery_set];
     
     NSLog(@"%@",[jewelery valueForKey:@"jewelery_item_id"]);
     */
    
}

@end

