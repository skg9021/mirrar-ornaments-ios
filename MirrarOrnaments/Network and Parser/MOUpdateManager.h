//
//  MOUpdateManager.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MOCoreDataPrereqs.h"

#import "MOUIDevice-Reachability.h"
//#import "MODownloadHelper.h"
#import "MOFileSystemManager.h"
#import "SMXMLDocument.h"

@protocol MOUpdateManagerDelegate <NSObject>

@optional
- (void) updateCompleted;
- (void) updateAvailable:(NSNumber *)count;
- (void) updateReady;
- (void) updateFailed: (NSString *)reason;

@end

@interface MOUpdateManager : NSObject </*MODownloadHelperDelegate,*/
            MOJeweleryCommodityDelegate, MOJeweleryDelegate,
            MOJeweleryCategorizationPredicateDelegate>
{
    NSMutableString *networkLog;
    //MODownloadHelper *downloadHelper;
    SMXMLDocument   *updateDocumentTree;
    
    BOOL isUpdating;
    float percentComplete;
}

- (id) initWithDelegate:(id <MOUpdateManagerDelegate>)delegate;

- (BOOL) updateIsAvailable;
- (void) updateUsingMOC:(NSManagedObjectContext*)managedObjectContext
            didFailWithError:(NSError **)error;

- (void) log: (NSString *) formatString, ...;
- (BOOL) runReachabilityTestsOnURLs:(NSDictionary*)urls didFailWithError:(NSError **)error;
- (BOOL) checkSiteAvailabilityOfURL:(NSString*)url didFailWithError:(NSError *)error;
- (void) reachabilityChanged;

@property (weak) id <MOUpdateManagerDelegate> delegate;
@property (readonly) BOOL isUpdating;
@property (readonly) float percentComplete;
@property (nonatomic, retain) NSManagedObjectContext          *managedObjectContext;
@property bool workInProgress;
@property NSStringEncoding stringEncoding;

@end
