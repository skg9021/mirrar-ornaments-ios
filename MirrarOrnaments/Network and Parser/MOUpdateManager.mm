//
//  MOUpdateManager.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOUpdateManager.h"
#import "MORequestQueue.h"
#import "NSObject+SBJson.h"

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
    (([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
    [THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)

#define SERVER_URL  @"107.21.122.227"
//#define UPDATE_FILE_URL @"107.21.122.227/mo/xml"
//#define UPDATE_DATA_URL @"107.21.122.227/mo/images"
//#define UPDATE_FILENAME @"1101ver2.xml"
#define APPEND_PATH(PATH1, PATH2) [[PATH1 stringByAppendingString:@"/"] stringByAppendingString:PATH2]

static int commodityCount, jeweleryCategorizationPredicateCount, jeweleryCount = 0, downloadFilesCount=0, categoryFilesCompleted=0, jeweleryFilesCompleted=0;

@interface MOUpdateManager ()

@property (strong, nonatomic) MOFileSystemManager *fileSystemManager;

@property BOOL hasUpdatedJewelery;
@property BOOL hasUpdatedCategorizationPredicates;
@property BOOL hasUpdatedCommodities;

@property (strong, nonatomic) NSArray *updateXMLURLs;
@property (strong, nonatomic) NSString *updateImageURL;
@property int pendingUpdateCount;
@end

@implementation MOUpdateManager

@synthesize fileSystemManager;
@synthesize isUpdating, percentComplete;
@synthesize hasUpdatedCategorizationPredicates, hasUpdatedCommodities, hasUpdatedJewelery;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize delegate = _delegate;
@synthesize workInProgress;
@synthesize updateImageURL = _updateImageURL;
@synthesize updateXMLURLs = _updateXMLURLs;
@synthesize pendingUpdateCount = _pendingUpdateCount;
@synthesize stringEncoding;

- (NSString*)encodeURL:(NSString *)string
{
    stringEncoding = NSASCIIStringEncoding;
	NSString *newString = (__bridge NSString *)(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)string, NULL, CFSTR(":/?#[]@!$ &'()*+,;=\"<>%{}|\\^~`"), CFStringConvertNSStringEncodingToEncoding([self stringEncoding])));
	if (newString) {
		return newString;
	}
	return @"";
}

- (id) initWithDelegate:(id <MOUpdateManagerDelegate>)delegate
{
    if (self = [super init]) {
        [[UIDevice currentDevice] scheduleReachabilityWatcher:self];
        [self setFileSystemManager:[[MOFileSystemManager alloc] init]];
        BOOL answer = [self.fileSystemManager createDirectoryAtPath:self.fileSystemManager.storageDirectory error:nil];
        answer = [self.fileSystemManager createDirectoryAtPath:self.fileSystemManager.imageStorageDirectory error:nil];
        self.delegate = delegate;
        NSLog(@"%c", answer);
        stringEncoding = NSASCIIStringEncoding;
    }
    return self;
}

- (BOOL) updateIsAvailable
{
    isUpdating = NO;
    NSError *error;
    if (![self checkSiteAvailabilityOfURL:SERVER_URL didFailWithError:error]) {
        return NO;
    }
    else
    {
        NSString *post =[[NSString alloc] initWithFormat:@"device_id=%@&user=%@&pass=%@",[self encodeURL:@"1101"],[self encodeURL:@"jg"],[self encodeURL:@"jhg"]];
        NSURL *url=[NSURL URLWithString:@"https://107.21.122.227/mo/auth.dev.php"];
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        NSString *charset = (NSString *)CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding([self stringEncoding]));
        [request setValue:[NSString stringWithFormat:@"application/x-www-form-urlencoded; charset=%@",charset] forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        RQOperation *operation = [RQOperation operationWithRequest:request];
        operation.completionHandler = ^(NSURLResponse *response, NSData *data, NSError *error)
        {
            if (!error)
            {
                if (data)
                {
                    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    NSLog(@"%@", responseString);
                    
                    NSDictionary *responseDict = [responseString JSONValue];
                    _updateXMLURLs = [responseDict objectForKey:@"xmls"];
                    _updateImageURL = [responseDict objectForKey:@"images"];
                    if ([_updateXMLURLs count] > 0) {
                        SAFE_PERFORM_WITH_ARG(self.delegate, @selector(updateAvailable:), [NSNumber numberWithInt:[_updateXMLURLs count]]);
                    }
                    
                }
                else
                {
                    SAFE_PERFORM_WITH_ARG(self.delegate, @selector(updateFailed:), @"Empty JSON returned");
                }
            }
            else
            {
                SAFE_PERFORM_WITH_ARG(self.delegate, @selector(updateFailed:), [error userInfo]);
            }
        };
        //add operation to queue
        [[RequestQueue mainQueue] addOperation:operation];

    }
    
    return NO;
}

- (void) log: (NSString *) formatString, ...
{
    if (!formatString) return;
    
	va_list arglist;
	va_start(arglist, formatString);
	NSString *outstring = [[NSString alloc] initWithFormat:formatString arguments:arglist];
	va_end(arglist);
    
    printf("%s\n", [outstring UTF8String]);
    
    if (!networkLog) networkLog = [NSMutableString string];
    [networkLog insertString:@"\n" atIndex:0];
    [networkLog insertString:outstring atIndex:0];
}

#pragma mark -
#pragma mark Reachability Tests


- (BOOL) runReachabilityTestsOnURLs:(NSDictionary *)urls didFailWithError:(NSError **)error
{
    UIDevice *device = [UIDevice currentDevice];
    [self log:@"\n\n"];
    [self log:@"Current host: %@", [device hostname]];
    [self log:@"IPAddress: %@", [device localIPAddress]];
    [self log:@"Local: %@", [device localWiFiIPAddress]];
    [self log:@"All: %@", [device localWiFiIPAddresses]];
    
    [self log:@"Network available?: %@", [device networkAvailable] ? @"Yes" : @"No"];
    [self log:@"Active WLAN?: %@", [device activeWLAN] ? @"Yes" : @"No"];
    [self log:@"Active WWAN?: %@", [device activeWWAN] ? @"Yes" : @"No"];
    [self log:@"Active hotspot?: %@", [device activePersonalHotspot] ? @"Yes" : @"No"];
    if (([device activeWWAN] || [device activeWLAN] || [device activeWWAN]))
        return YES;
    
    __block NSMutableDictionary *ipAddressesOfURLs = [NSMutableDictionary new];
    //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [[[NSOperationQueue alloc] init] addOperationWithBlock:
     ^{
         
         NSArray *keys;
         int i,count;
         
         keys = [urls allKeys];
         count = [urls count];
         
         for (i=0; i <count; i++) {
             NSString *ip = [device getIPAddressForHost:[urls objectForKey:[keys objectAtIndex:i]]];
             [ipAddressesOfURLs setObject:ip forKey:[keys objectAtIndex:i]];
         }
         
         [[NSOperationQueue mainQueue] addOperationWithBlock:^{
             NSArray *keys;
             int i,count;
             
             keys = [ipAddressesOfURLs allKeys];
             count = [ipAddressesOfURLs count];
             
             for (i=0; i <count; i++) {
                 [self log:@"%@ : %@",[keys objectAtIndex:i], [ipAddressesOfURLs objectForKey:[keys objectAtIndex:i]]];
             }
             //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
         }];
     }];
    
    return YES;
}

#define CHECK(SITE) [self log:@"• %@ : %@", SITE, [device hostAvailable:SITE] ? @"available" : @"not available"];

- (BOOL) checkSiteAvailabilityOfURL:(NSString *)url didFailWithError:(NSError *)error
{
    UIDevice *device = [UIDevice currentDevice];
    if (![device hostAvailable:url]) {
        return NO;
    }
    return YES;
}

- (void) reachabilityChanged
{
    [self log:@"REACHABILITY CHANGED!\n"];
}



#pragma mark -
#pragma mark Update System

- (void) updateUsingMOC:(NSManagedObjectContext *)managedObjectContext
            didFailWithError:(NSError **)error
{
    
    if (!managedObjectContext) {
        SAFE_PERFORM_WITH_ARG(_delegate, @selector(updateFailed:),
                              @"Invalid Managed Object Context");
        return;
    }
    
    for (NSString *updateXMLURL in _updateXMLURLs) {
        //create a temporary directory to hold the download data
        NSArray *parts = [updateXMLURL componentsSeparatedByString:@"/"];
        NSString *filename = [parts objectAtIndex:[parts count]-1];
        NSString *tempImageDirectory = [[self fileSystemManager] pathInStorageDirectory:filename];
        NSLog(@"%@",tempImageDirectory);
        
        [self.fileSystemManager createDirectoryAtPath:tempImageDirectory error:nil];
        
        //create update xml download operation 
        NSURL *URL = [NSURL URLWithString:updateXMLURL];
        NSURLCacheStoragePolicy policy = NSURLCacheStorageNotAllowed;
        NSURLRequest *request = [NSURLRequest requestWithURL:URL cachePolicy:policy timeoutInterval:15.0];
        RQOperation *operation = [RQOperation operationWithRequest:request];
        
        __block NSManagedObjectContext *tempMOC = managedObjectContext;
        //completion handler
        operation.completionHandler = ^(NSURLResponse *response, NSData *data, NSError *error)
        {
            if (!error)
            {
                if (data)
                {
                    NSError *parsingError;
                    updateDocumentTree = [SMXMLDocument documentWithData:data error:&parsingError];
                    NSString *version = [updateDocumentTree.root attributeNamed:@"version"];
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    NSLog(@"Version = %@, \nData = %@", version, updateDocumentTree);
                    if ([prefs floatForKey:@"update_version"] < [version floatValue] ) {
                        [prefs setFloat:[version floatValue] forKey:@"update_version"];
                    }
                    else
                    {
                        SAFE_PERFORM_WITH_ARG(self.delegate, @selector(updateCompleted), NULL);
                        return;
                    }
                    
                    NSMutableArray *categoryImages = [[NSMutableArray alloc] init];
                    for (SMXMLElement *jcpredicate in [[updateDocumentTree.root childNamed:@"MOJeweleryCategorizationPredicateSet"] childrenNamed:@"MOJeweleryCategorizationPredicate"])
                    {
                        for (SMXMLElement *jc in [[jcpredicate childNamed:@"CategorizationPredicateResultSet"] childrenNamed:@"MOJeweleryCategory"]) {
                            [categoryImages addObject:[jc valueWithPath:@"CategoryCaptionImage"]];
                            downloadFilesCount++;
                        }
                    }
                    NSLog(@"%@, %u", categoryImages, [categoryImages count]);
                    
                    NSMutableArray *jeweleryCaptionImages = [[NSMutableArray alloc] init];
                    for (SMXMLElement *jewelery in [[updateDocumentTree.root childNamed:@"MOJewelerySet"] childrenNamed:@"MOJewelery"]) {
                        NSString *jeweleryCaptionImageName = [jewelery valueWithPath:@"JeweleryPreviewImage"];
                        NSString *jeweleryCaptionSmall = [[jeweleryCaptionImageName copy] stringByAppendingString:@"@2x.png"];
                        NSString *jeweleryCaptionBig = [[jeweleryCaptionImageName copy] stringByAppendingString:@"_big@2x.png"];
                        //for (int i=0; i<30; i++) {
                        [jeweleryCaptionImages addObject:jeweleryCaptionBig];
                        [jeweleryCaptionImages addObject:jeweleryCaptionSmall];
                        [jeweleryCaptionImages addObject:[[[jewelery childNamed:@"MOJeweleryImage"] valueWithPath:@"JeweleryFilename"] stringByAppendingString:@".png"]];
                        downloadFilesCount++;
                        //}
                    }
                    
                    NSLog(@"%@, %u", jeweleryCaptionImages, [jeweleryCaptionImages count]);
                    categoryFilesCompleted = 0;
                    for (NSString *urlString in categoryImages)
                    {
                        //create operation
                        NSString *urlPrefix = [[_updateImageURL copy] stringByAppendingString:urlString];
                        NSURL *URL = [NSURL URLWithString:urlPrefix];
                        NSURLCacheStoragePolicy policy = NSURLCacheStorageNotAllowed;
                        NSURLRequest *request = [NSURLRequest requestWithURL:URL cachePolicy:policy timeoutInterval:15.0];
                        RQOperation *operation = [RQOperation operationWithRequest:request];
                        
                        //completion handler
                        operation.completionHandler = ^(NSURLResponse *response, NSData *data, NSError *error)
                        {
                            categoryFilesCompleted++;
                            if (!error)
                            {
                                //image downloaded
                                UIImage *image = [UIImage imageWithData:data];
                                [self.fileSystemManager storeImage:image withName:urlString ofType:@"png" atPath:tempImageDirectory];
                            }
                            
                            if (categoryFilesCompleted == [categoryImages count]) {
                                NSString *action = [updateDocumentTree.root attributeNamed:@"action"];
                                if ([action isEqualToString:@"ADD"]) {
                                    [self createDatabaseUsingMOC:tempMOC didFailWithError:&error];
                                }
                                else if ([action isEqualToString:@"update"])
                                {
                                    [self updateDatabaseUsingMOC:tempMOC didFailWithError:&error];
                                }
                                else if ([action isEqualToString:@"delete"])
                                {
                                    [self deleteDatabaseUsingMOC:tempMOC didFailWithError:&error];
                                }
                            }
                        };
                        
                        //add operation to queue
                        [[RequestQueue mainQueue] addOperation:operation];
                    }
                    
                    jeweleryFilesCompleted = 0;
                    //load images
                    for (NSString *urlString in jeweleryCaptionImages)
                    {
                        //create operation
                        if ([self.fileSystemManager fileExistsInPath:[[tempImageDirectory copy] stringByAppendingPathComponent:urlString]]) {
                            jeweleryFilesCompleted++;
                            continue;
                        }
                        NSString *urlPrefix = [[_updateImageURL copy] stringByAppendingString:urlString];
                        NSURL *URL = [NSURL URLWithString:urlPrefix];
                        NSURLCacheStoragePolicy policy = NSURLCacheStorageNotAllowed;
                        NSURLRequest *request = [NSURLRequest requestWithURL:URL cachePolicy:policy timeoutInterval:15.0];
                        RQOperation *operation = [RQOperation operationWithRequest:request];
                        
                        //completion handler
                        operation.completionHandler = ^(NSURLResponse *response, NSData *data, NSError *error)
                        {
                            if (!error)
                            {
                                //image downloaded
                                jeweleryFilesCompleted++;
                                UIImage *image = [UIImage imageWithData:data];
                                NSLog(@"Download f");
                                [self.fileSystemManager storeImage:image withName:urlString ofType:@"png" atPath:tempImageDirectory];
                            }
                            
                            if (jeweleryFilesCompleted == [jeweleryCaptionImages count]) {
                                NSLog(@"Downloaded all files here");
                                [self.fileSystemManager moveContentsOfDirectory:tempImageDirectory toDirectory:[self.fileSystemManager imageStorageDirectory]];
                                NSString *action = [updateDocumentTree.root attributeNamed:@"action"];
                                if ([action isEqualToString:@"ADD"]) {
                                    [self createDatabaseUsingMOC:tempMOC didFailWithError:&error];
                                    [self.managedObjectContext save:&error];
                                    [self createCategoriesUsingMOC:tempMOC didFailWithError:nil];
                                    [self.managedObjectContext save:&error];
                                    [self createJeweleryUsingMOC:tempMOC didFailWithError:nil];
                                    
                                    
                                }
                                else if ([action isEqualToString:@"update"])
                                {
                                    [self updateDatabaseUsingMOC:managedObjectContext didFailWithError:&error];
                                }
                                else if ([action isEqualToString:@"delete"])
                                {
                                    [self deleteDatabaseUsingMOC:managedObjectContext didFailWithError:&error];
                                }
                            }
                        };
                        
                        //add operation to queue
                        [[RequestQueue mainQueue] addOperation:operation];
                    }
                }
                else
                {
                    NSLog(@"Could not parse update XML.");
                }
            }
            else
            {
                NSLog(@"Could not download update XML.\nError=%@", error);
            }
            
        };
        
        //add operation to queue
        [[RequestQueue mainQueue] addOperation:operation];
        break;
    }
    
    /*
    self.managedObjectContext = managedObjectContext;
    
    NSString *action = [updateDocumentTree.root attributeNamed:@"action"];
    if ([action isEqualToString:@"ADD"]) {
        [self createDatabaseUsingMOC:managedObjectContext didFailWithError:error];
    }
    else if ([action isEqualToString:@"update"])
    {
        [self updateDatabaseUsingMOC:managedObjectContext didFailWithError:error];
    }
    else if ([action isEqualToString:@"delete"])
    {
        [self deleteDatabaseUsingMOC:managedObjectContext didFailWithError:error];
    }
    */
}

- (void) createDatabaseUsingMOC:(NSManagedObjectContext*)managedObjectContext
                    didFailWithError:(NSError **)error
{
    isUpdating = YES;
    jeweleryCategorizationPredicateCount = 0;
    jeweleryCount = 0;
    commodityCount = 0;
    // Process the Jewelery Commodities Set first
    SMXMLElement *jeweleryCommoditySet = [updateDocumentTree.root childNamed:@"MOCommoditySet"];
    NSArray *commodities = [jeweleryCommoditySet childrenNamed:@"MOJeweleryCommodity"];
    commodityCount = [commodities count];
    for (SMXMLElement *commodity in commodities) {
        
        [MOJeweleryCommodity createJeweleryCommodityFrom:commodity
                                               inContext:managedObjectContext withDelegate:self];
    }
    
}

- (void) createCategoriesUsingMOC:(NSManagedObjectContext*)managedObjectContext
               didFailWithError:(NSError **)error
{
    // Process the Jewelery Categorization Predicates
    SMXMLElement *jeweleryPredicateSet = [updateDocumentTree.root childNamed:@"MOJeweleryCategorizationPredicateSet"];
    NSArray *jeweleryPredicates = [jeweleryPredicateSet childrenNamed:@"MOJeweleryCategorizationPredicate"];
    jeweleryCategorizationPredicateCount = [jeweleryPredicates count];
    for (SMXMLElement *jeweleryPredicate in jeweleryPredicates) {
        NSString *action = [jeweleryPredicate attributeNamed:@"action"];
        if ([action isEqualToString:@"ADD"]) {
            [MOJeweleryCategorizationPredicate createJeweleryCategorizationPredicateFrom:jeweleryPredicate
                                                                               inContext:managedObjectContext withDelegate:self];
        }
        else if ([action isEqualToString:@"update"])
        {
            [MOJeweleryCategorizationPredicate updateJeweleryCategorizationPredicateFrom:jeweleryPredicate
                                                                               inContext:managedObjectContext withDelegate:self];
        }
        else if ([action isEqualToString:@"delete"])
        {
            [MOJeweleryCategorizationPredicate updateJeweleryCategorizationPredicateFrom:jeweleryPredicate
                                                                               inContext:managedObjectContext withDelegate:self];
        }
        else
        {
            SAFE_PERFORM_WITH_ARG(_delegate, @selector(updateFailed:),
                                  @"Invalid action for jewelery predicate");
            return;
        }
    }
}

- (void) createJeweleryUsingMOC:(NSManagedObjectContext*)managedObjectContext
                 didFailWithError:(NSError **)error
{
    // Process the Jeweleries
    SMXMLElement *jewelerySet = [updateDocumentTree.root childNamed:@"MOJewelerySet"];
    NSArray *jeweleries = [jewelerySet childrenNamed:@"MOJewelery"];
    jeweleryCount = [jeweleries count];
    for (SMXMLElement *jewelery in jeweleries) {
        NSString *action = [jewelery attributeNamed:@"action"];
        if ([action isEqualToString:@"ADD"]) {
            [MOJewelery createJeweleryFrom:jewelery inContext:managedObjectContext withDelegate:self];
        }
        else if ([action isEqualToString:@"update"])
        {
            [MOJewelery updateJeweleryFrom:jewelery inContext:managedObjectContext withDelegate:self];
        }
        else if ([action isEqualToString:@"delete"])
        {
            [MOJewelery updateJeweleryFrom:jewelery inContext:managedObjectContext withDelegate:self];
        }
        else
        {
            SAFE_PERFORM_WITH_ARG(_delegate, @selector(updateFailed:),
                                  @"Invalid action for jewelery");
            return;
        }
    }
}

- (void) updateDatabaseUsingMOC:(NSManagedObjectContext*)managedObjectContext
         didFailWithError:(NSError **)error
{
    isUpdating = YES;
    jeweleryCategorizationPredicateCount = 0;
    jeweleryCount = 0;
    commodityCount = 0;
    // Process the Jewelery Commodities Set first
    SMXMLElement *jeweleryCommoditySet = [updateDocumentTree.root childNamed:@"MOCommoditySet"];
    NSArray *commodities = [jeweleryCommoditySet childrenNamed:@"MOJeweleryCommodity"];
    commodityCount = [commodities count];
    for (SMXMLElement *commodity in commodities) {
        
        [MOJeweleryCommodity createJeweleryCommodityFrom:commodity
                                               inContext:managedObjectContext withDelegate:self];
    }
}

- (void) updateCategoriesUsingMOC:(NSManagedObjectContext*)managedObjectContext
                 didFailWithError:(NSError **)error
{
    // Process the Jewelery Categorization Predicates
    SMXMLElement *jeweleryPredicateSet = [updateDocumentTree.root childNamed:@"MOJeweleryCategorizationPredicateSet"];
    NSArray *jeweleryPredicates = [jeweleryPredicateSet childrenNamed:@"MOJeweleryCategorizationPredicate"];
    jeweleryCategorizationPredicateCount = [jeweleryPredicates count];
    for (SMXMLElement *jeweleryPredicate in jeweleryPredicates) {
        NSString *action = [jeweleryPredicate attributeNamed:@"action"];
        if ([action isEqualToString:@"ADD"]) {
            [MOJeweleryCategorizationPredicate createJeweleryCategorizationPredicateFrom:jeweleryPredicate
                                                                               inContext:managedObjectContext withDelegate:self];
        }
        else if ([action isEqualToString:@"update"])
        {
            [MOJeweleryCategorizationPredicate updateJeweleryCategorizationPredicateFrom:jeweleryPredicate
                                                                               inContext:managedObjectContext withDelegate:self];
        }
        else if ([action isEqualToString:@"delete"])
        {
            [MOJeweleryCategorizationPredicate updateJeweleryCategorizationPredicateFrom:jeweleryPredicate
                                                                               inContext:managedObjectContext withDelegate:self];
        }
        else
        {
            SAFE_PERFORM_WITH_ARG(_delegate, @selector(updateFailed:),
                                  @"Invalid action for jewelery predicate update");
            return;
        }
    }
}

- (void) updateJeweleryUsingMOC:(NSManagedObjectContext*)managedObjectContext
               didFailWithError:(NSError **)error
{
    // Process the Jeweleries
    SMXMLElement *jewelerySet = [updateDocumentTree.root childNamed:@"MOJewelerySet"];
    NSArray *jeweleries = [jewelerySet childrenNamed:@"MOJewelery"];
    jeweleryCount = [jeweleries count];
    for (SMXMLElement *jewelery in jeweleries) {
        NSString *action = [jewelery attributeNamed:@"action"];
        if ([action isEqualToString:@"ADD"]) {
            [MOJewelery createJeweleryFrom:jewelery inContext:managedObjectContext withDelegate:self];
        }
        else if ([action isEqualToString:@"update"])
        {
            [MOJewelery updateJeweleryFrom:jewelery inContext:managedObjectContext withDelegate:self];
        }
        else if ([action isEqualToString:@"delete"])
        {
            [MOJewelery updateJeweleryFrom:jewelery inContext:managedObjectContext withDelegate:self];
        }
        else
        {
            SAFE_PERFORM_WITH_ARG(_delegate, @selector(updateFailed:),
                                  @"Invalid action for jewelery");
            return;
        }
    }
}


- (BOOL) deleteDatabaseUsingMOC:(NSManagedObjectContext*)managedObjectContext
         didFailWithError:(NSError **)error
{
    return YES;
}

#pragma mark -
#pragma mark Jewelery Protocols

- (void) jeweleryUpdateCompleted
{
    if (--jeweleryCount == 0) {
        [self.managedObjectContext save:nil];
        SAFE_PERFORM_WITH_ARG(self.delegate, @selector(updateCompleted), NULL);
    }
    
}

- (void) jeweleryUpdateFailed: (NSString *) reason
{

    SAFE_PERFORM_WITH_ARG(self.delegate, @selector(updateFailed:), reason);
}

/*
#pragma mark -
#pragma mark Commodity Protocols

- (void) jeweleryCommodityUpdateCompleted
{
    if (--commodityCount == 0) {
        hasUpdatedCommodities = YES;
        NSError *error;
    }
    
}

- (void) jeweleryCommodityUpdateFailed: (NSString *) reason
{
    SAFE_PERFORM_WITH_ARG(self.delegate, @selector(updateFailed:), reason);
}

#pragma mark -
#pragma mark Jewelery Categorization Predicate Protocols

- (void) jeweleryCategorizationPredicateUpdateCompleted
{
    hasUpdatedCategorizationPredicates = YES;
    NSError *error;
}

- (void) jeweleryCategorizationPredicateUpdateFailed: (NSString *) reason
{
    SAFE_PERFORM_WITH_ARG(self.delegate, @selector(updateFailed:), reason);
}

#pragma mark -
#pragma mark Download Helper Protocols

- (void) downloadFinished
{
    //NSLog(@"Download Complete");
    NSString *destPath = [self.fileSystemManager pathInImageStorageDirectory:UPDATE_FILENAME];
    NSLog(@"%@", destPath);
    
    NSData *data = [NSData dataWithContentsOfFile:destPath];
    NSError *parsingError;
    updateDocumentTree = [SMXMLDocument documentWithData:data error:&parsingError];
    
    if(parsingError)
    {
        NSLog(@"Error while parsing the document: %@", parsingError);
        SAFE_PERFORM_WITH_ARG(self.delegate, @selector(updateFailed:),
                              @"Could not parse Update File");

    }
    
    NSString *version = [updateDocumentTree.root attributeNamed:@"version"];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if ([prefs floatForKey:@"update_version"] < [version floatValue] ) {
        [prefs setFloat:[version floatValue] forKey:@"update_version"];
        SAFE_PERFORM_WITH_ARG(self.delegate, @selector(updateAvailable), nil);
    }
         
}

- (void) dataDownloadFailed: (NSString *) reason
{
    NSLog(@"Download Failed");
    SAFE_PERFORM_WITH_ARG(self.delegate, @selector(updateFailed:),
                          @"Could not download Update File");
}

- (void) downloadReceivedData
{
    float received = downloadHelper.bytesRead;
    float expected = downloadHelper.expectedLength;
    
    NSLog(@"Received %f",received / expected);
}

*/
@end