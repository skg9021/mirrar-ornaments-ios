// ARC Helper

#ifndef ah_retain
#if __has_feature(objc_arc)
#define ah_retain self
#define ah_dealloc self
#define release self
#define autorelease self
#else
#define ah_retain retain
#define ah_dealloc dealloc
#define __bridge
#endif
#endif

//  ARC Helper ends


#import <Foundation/Foundation.h>


extern NSString *const HTTPResponseErrorDomain;


typedef void (^RQCompletionHandler)(NSURLResponse *response, NSData *data, NSError *error);
typedef void (^RQProgressHandler)(float progress, NSInteger bytesTransferred, NSInteger totalBytes);
typedef void (^RQAuthenticationChallengeHandler)(NSURLAuthenticationChallenge *challenge);


typedef enum
{
    RequestQueueModeFirstInFirstOut = 0,
    RequestQueueModeLastInFirstOut
}
RequestQueueMode;


@interface RQOperation : NSOperation

@property (nonatomic, strong, readonly) NSURLRequest *request;
@property (nonatomic, copy) RQCompletionHandler completionHandler;
@property (nonatomic, copy) RQProgressHandler uploadProgressHandler;
@property (nonatomic, copy) RQProgressHandler downloadProgressHandler;
@property (nonatomic, copy) RQAuthenticationChallengeHandler authenticationChallengeHandler;
@property (nonatomic, copy) NSSet *autoRetryErrorCodes;
@property (nonatomic, assign) BOOL autoRetry;

+ (RQOperation *)operationWithRequest:(NSURLRequest *)request;
- (RQOperation *)initWithRequest:(NSURLRequest *)request;

@end


@interface RequestQueue : NSObject

@property (nonatomic, assign) NSUInteger maxConcurrentRequestCount;
@property (nonatomic, assign, getter = isSuspended) BOOL suspended;
@property (nonatomic, assign, readonly) NSUInteger requestCount;
@property (nonatomic, strong, readonly) NSArray *requests;
@property (nonatomic, assign) RequestQueueMode queueMode;
@property (nonatomic, assign) BOOL allowDuplicateRequests;

+ (RequestQueue *)mainQueue;

- (void)addOperation:(RQOperation *)operation;
- (void)addRequest:(NSURLRequest *)request completionHandler:(RQCompletionHandler)completionHandler;
- (void)cancelRequest:(NSURLRequest *)request;
- (void)cancelAllRequests;

@end
