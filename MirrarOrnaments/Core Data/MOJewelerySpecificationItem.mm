//
//  MOJewelerySpecificationItem.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOJewelerySpecificationItem.h"
#import "MOJeweleryCommodity.h"
#import "MOJewelerySpecification.h"

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)

@implementation MOJewelerySpecificationItem

@dynamic specification_item_commodity_amount;
@dynamic belongs_to_specification;
@dynamic specification_item_commodity;

@synthesize delegate;

+ (void) createJewelerySpecificationItemFrom:(SMXMLElement *)jewelerySpecificationItem
                                   inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJewelerySpecificationItemDelegate>)delegate
{
    MOJewelerySpecificationItem *newJewelerySpecificationItem =
    [NSEntityDescription insertNewObjectForEntityForName:@"MOJewelerySpecificationItem"
                                  inManagedObjectContext:managedObjectContext];
    
    if (!newJewelerySpecificationItem) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jewelerySpecificationItemUpdateFailed:),
                              @"Could not create MOJewelerySpecificationItem");
        return;
    }
    
    double amount = [[jewelerySpecificationItem attributeNamed:@"amount"] doubleValue];
    if (amount < 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jewelerySpecificationItemUpdateFailed:),
                              @"Expecting a positive amount");
        return;
    }
    
    newJewelerySpecificationItem.specification_item_commodity_amount = [NSNumber numberWithDouble:amount];

    
    int uid = [[jewelerySpecificationItem attributeNamed:@"commodity"] intValue];
    if (uid <= 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jewelerySpecificationItemUpdateFailed:),
                              @"Expecting a positive commodity ID");
        return;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOJeweleryCommodity"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSExpression *exprCommodityId = [NSExpression expressionForKeyPath:@"commodity_id"];
    NSExpression *exprUid = [NSExpression expressionForConstantValue:[NSNumber numberWithInt:uid]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprCommodityId
                                                                rightExpression:exprUid modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType options:0];
    [fetchRequest setPredicate:predicate];
    NSArray *commodities = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if ([commodities count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jewelerySpecificationItemUpdateFailed:),
                              @"Zero commodities with given ID found");
        return;
    }
    MOJeweleryCommodity *commodity = [commodities objectAtIndex:0];
    newJewelerySpecificationItem.specification_item_commodity = commodity;

    newJewelerySpecificationItem.belongs_to_specification = delegate;
    SAFE_PERFORM_WITH_ARG(delegate, @selector(jewelerySpecificationItemUpdateCompleted),NULL);
}

@end
