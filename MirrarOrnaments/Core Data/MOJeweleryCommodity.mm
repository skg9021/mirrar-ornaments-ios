//
//  MOJeweleryCommodity.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOJeweleryCommodity.h"
#import "MOJewelerySpecificationItem.h"

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)

@implementation MOJeweleryCommodity

@dynamic commodity_id;
@dynamic commodity_name;
@dynamic commodity_price_per_unit;
@dynamic commodity_used_in_specification_item;

@synthesize delegate;

+ (void) createJeweleryCommodityFrom:(SMXMLElement *)jeweleryCommodity
                           inContext:(NSManagedObjectContext *)managedObjectContext
                        withDelegate:(id <MOJeweleryCommodityDelegate>)delegate
{
    MOJeweleryCommodity *newCommodity =
    [NSEntityDescription insertNewObjectForEntityForName:@"MOJeweleryCommodity"
                                  inManagedObjectContext:managedObjectContext];
    if (!newCommodity) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCommodityUpdateFailed:),
                              @"Could not create MOJeweleryCommodity");
        return;
    }
    
    int uid = [[jeweleryCommodity valueWithPath:@"id"] intValue];
    if (uid <= 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCommodityUpdateFailed:),
                              @"Expecting a positive value for Commodity ID");
        return;
    }
    
    double price = [[jeweleryCommodity valueWithPath:@"price"] doubleValue];
    if (price <= 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCommodityUpdateFailed:),
                              @"Expecting a positive value for Commodity Price");
        return;
    }
    
    NSString *name = [jeweleryCommodity valueWithPath:@"name"];
    if ([name isEqualToString:@""]) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCommodityUpdateFailed:),
                              @"Expecting a non-null for Commodity Name");
        return;
    }
    
    newCommodity.delegate = delegate;
    newCommodity.commodity_id = [NSNumber numberWithInt:uid];
    newCommodity.commodity_name = name;
    newCommodity.commodity_price_per_unit = [NSNumber numberWithDouble:price];
    SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCommodityUpdateCompleted), NULL);
}


+ (void) deleteJeweleryCommodityFrom:(SMXMLElement *)jeweleryCommodity
                           inContext:(NSManagedObjectContext *)managedObjectContext
                        withDelegate:(id <MOJeweleryCommodityDelegate>)delegate
{
    int uid = [[jeweleryCommodity valueWithPath:@"uid"] intValue];
    if (uid <= 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCommodityUpdateFailed:),
                              @"Expecting a positive value for Commodity ID");
        return;
    }

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOJeweleryCommodity"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSExpression *exprCommodityId = [NSExpression expressionForKeyPath:@"commodity_id"];
    NSExpression *exprUid = [NSExpression expressionForConstantValue:[NSNumber numberWithInt:uid]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprCommodityId
                                                                rightExpression:exprUid modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType options:0];
    [fetchRequest setPredicate:predicate];
    NSArray *commodities = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if ([commodities count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jewelerySpecificationItemUpdateFailed:),
                              @"Zero commodities with given ID found");
        return;
    }
    MOJeweleryCommodity *commodity = [commodities objectAtIndex:0];
    [managedObjectContext deleteObject:commodity];
    SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCommodityUpdateCompleted), NULL);
        
}


+ (void) updateJeweleryCommodityFrom:(SMXMLElement *)jeweleryCommodity
                           inContext:(NSManagedObjectContext *)managedObjectContext
                        withDelegate:(id <MOJeweleryCommodityDelegate>)delegate
{
    int uid = [[jeweleryCommodity valueWithPath:@"uid"] intValue];
    if (uid <= 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCommodityUpdateFailed:),
                              @"Expecting a positive value for Commodity ID");
        return;
    }
    
    double price = [[jeweleryCommodity valueWithPath:@"price"] doubleValue];
    if (price <= 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCommodityUpdateFailed:),
                              @"Expecting a positive value for Commodity Price");
        return;
    }
    
    NSString *name = [jeweleryCommodity valueWithPath:@"name"];
    if ([name isEqualToString:@""]) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCommodityUpdateFailed:),
                              @"Expecting a non-null for Commodity Name");
        return;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOJeweleryCommodity"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSExpression *exprCommodityId = [NSExpression expressionForKeyPath:@"commodity_id"];
    NSExpression *exprUid = [NSExpression expressionForConstantValue:[NSNumber numberWithInt:uid]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprCommodityId
                                                                rightExpression:exprUid modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType options:0];
    [fetchRequest setPredicate:predicate];
    NSArray *commodities = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if ([commodities count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jewelerySpecificationItemUpdateFailed:),
                              @"Zero commodities with given ID found");
        return;
    }
    MOJeweleryCommodity *commodity = [commodities objectAtIndex:0];
    commodity.commodity_name = name;
    commodity.commodity_price_per_unit = [NSNumber numberWithDouble:price];
    SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCommodityUpdateCompleted), NULL);
}

@end
