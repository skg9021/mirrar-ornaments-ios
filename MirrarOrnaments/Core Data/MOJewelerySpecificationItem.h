//
//  MOJewelerySpecificationItem.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "SMXMLDocument.h"

@class MOJeweleryCommodity, MOJewelerySpecification;

@protocol MOJewelerySpecificationItemDelegate <NSObject>

@optional
- (void) jewelerySpecificationItemUpdateCompleted;
- (void) jewelerySpecificationItemUpdateFailed: (NSString *)reason;

@end

@interface MOJewelerySpecificationItem : NSManagedObject

@property (nonatomic, retain) NSNumber * specification_item_commodity_amount;
@property (nonatomic, retain) MOJewelerySpecification *belongs_to_specification;
@property (nonatomic, retain) MOJeweleryCommodity *specification_item_commodity;

@end

@interface MOJewelerySpecificationItem ()

+ (void) createJewelerySpecificationItemFrom:(SMXMLElement *)jewelerySpecificationItem
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJewelerySpecificationItemDelegate>)delegate;

@property (weak) id <MOJewelerySpecificationItemDelegate> delegate;
@end
