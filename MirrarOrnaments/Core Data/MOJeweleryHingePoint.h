//
//  MOJeweleryHingePoint.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "SMXMLDocument.h"

@class MOJeweleryHingePointsSet;

@protocol MOJeweleryHingePointDelegate <NSObject>

@optional
- (void) jeweleryHingePointUpdateCompleted;
- (void) jeweleryHingePointUpdateFailed: (NSString *)reason;

@end

@interface MOJeweleryHingePoint : NSManagedObject

@property (nonatomic, retain) NSNumber * hinge_point_distance;
@property (nonatomic, retain) NSNumber * hinge_point_x;
@property (nonatomic, retain) NSNumber * hinge_point_y;
@property (nonatomic, retain) MOJeweleryHingePointsSet *belongs_to_hinge_point_set;

@end

@interface MOJeweleryHingePoint ()

+ (void) createJeweleryHingePointFrom:(SMXMLElement *)jeweleryHingePoint
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJeweleryHingePointDelegate>)delegate;

@property (weak) id <MOJeweleryHingePointDelegate> delegate;
@end
