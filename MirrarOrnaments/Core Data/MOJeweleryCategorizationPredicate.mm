//
//  MOJeweleryCategorizationPredicate.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOJeweleryCategorizationPredicate.h"
#import "MOJeweleryCategory.h"

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)

static int resultCategoriesCount = 0;
@implementation MOJeweleryCategorizationPredicate

@dynamic categorization_predicate_level;
@dynamic categorization_predicate_name;
@dynamic predicate_result_set;

@synthesize delegate;

+ (void) createJeweleryCategorizationPredicateFrom:(SMXMLElement *)jeweleryCategorizationPredicate
                                         inContext:(NSManagedObjectContext *)managedObjectContext
                                      withDelegate:(id <MOJeweleryCategorizationPredicateDelegate>)delegate
{
    MOJeweleryCategorizationPredicate *newJeweleryCategorizationPredicate =
    [NSEntityDescription insertNewObjectForEntityForName:@"MOJeweleryCategorizationPredicate"
                                  inManagedObjectContext:managedObjectContext];
    if (!newJeweleryCategorizationPredicate) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategoryUpdateFailed:),
                              @"Could not create MOJeweleryCategorizationPredicate");
        return;
    }
    
    int predicate_level = [[jeweleryCategorizationPredicate valueWithPath:@"CategorizationPredicateLevel"] intValue];
    if (predicate_level < 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateFailed:),
                              @"Expected non-negative value for Categorization Predicate Level");
        return;
    }
    newJeweleryCategorizationPredicate.categorization_predicate_level = [NSNumber numberWithInt:predicate_level];
    
    NSString *predicate_name = [jeweleryCategorizationPredicate valueWithPath:@"CategorizationPredicateName"];
    if (predicate_name == (id)[NSNull null] || predicate_name.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateFailed:),
                              @"Expected a not-null value for Categorization Predicate Name");
        return;
    }
    newJeweleryCategorizationPredicate.categorization_predicate_name = predicate_name;
    
    newJeweleryCategorizationPredicate.delegate = delegate;
    SMXMLElement *categorizationPredicateResultSet = [jeweleryCategorizationPredicate
                                                 childNamed:@"CategorizationPredicateResultSet"];
    NSArray *categorizationPredicateCategories = [categorizationPredicateResultSet childrenNamed:@"MOJeweleryCategory"];
    if ([categorizationPredicateCategories count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateFailed:),
                              @"Expected atleast one result category for the predicate");
        return;
    }
    
    for (SMXMLElement *category in categorizationPredicateCategories) {
        resultCategoriesCount++;
        NSString *action = [category attributeNamed:@"action"];
        
        if ([action isEqualToString:@"ADD"]) {
            [MOJeweleryCategory createJeweleryCategoryFrom:category
                                                 inContext:managedObjectContext
                                              withDelegate:newJeweleryCategorizationPredicate];
        }
        else
        {
            SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateFailed:),
                                  @"Invalid action");
        }
    }
}


+ (void) deleteJeweleryCategorizationPredicateFrom:(SMXMLElement *)jeweleryCategorizationPredicate
                                         inContext:(NSManagedObjectContext *)managedObjectContext
                                      withDelegate:(id <MOJeweleryCategorizationPredicateDelegate>)delegate
{
    NSString *predicate_name = [jeweleryCategorizationPredicate valueWithPath:@"CategorizationPredicateName"];
    if (predicate_name == (id)[NSNull null] || predicate_name.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateFailed:),
                              @"Expected a not-null value for Categorization Predicate Name");
        return;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOJeweleryCategorizationPredicate"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSExpression *exprCategorizationPredicateName = [NSExpression expressionForKeyPath:@"categorization_predicate_name"];
    NSExpression *exprName = [NSExpression expressionForConstantValue:predicate_name];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprCategorizationPredicateName
                                                                rightExpression:exprName modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType options:0];
    [fetchRequest setPredicate:predicate];
    NSArray *predicates = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    if ([predicates count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateFailed:),
                              @"No categorization predicate with given name found");
        return;
    }
    /*
    MOJeweleryCategorizationPredicate *categorizationPredicate = [predicates objectAtIndex:0];
    
    for (MOJeweleryCategory *category in categorizationPredicate.predicate_result_set) {
        [MOJeweleryCategory deleteJeweleryCategoryFrom:<#(SMXMLElement *)#> inContext:<#(NSManagedObjectContext *)#> withDelegate:<#(id<MOJeweleryCategoryDelegate>)#>]
    }
     */
    [managedObjectContext deleteObject:[predicates objectAtIndex:0]];
    SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateCompleted), NULL);
    
}


+ (void) updateJeweleryCategorizationPredicateFrom:(SMXMLElement *)jeweleryCategorizationPredicate
                                         inContext:(NSManagedObjectContext *)managedObjectContext
                                      withDelegate:(id <MOJeweleryCategorizationPredicateDelegate>)delegate
{
    NSString *predicate_name = [jeweleryCategorizationPredicate valueWithPath:@"CategorizationPredicateName"];
    if (predicate_name == (id)[NSNull null] || predicate_name.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateFailed:),
                              @"Expected a not-null value for Categorization Predicate Name");
        return;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOJeweleryCategorizationPredicate"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSExpression *exprPredicateName = [NSExpression expressionForKeyPath:@"categorization_predicate_name"];
    NSExpression *exprName = [NSExpression expressionForConstantValue:predicate_name];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprPredicateName
                                                                rightExpression:exprName modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType options:0];
    [fetchRequest setPredicate:predicate];
    NSArray *predicates = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if ([predicates count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateFailed:),
                              @"Could not find any predicate with the given name");
        return;
    }

    MOJeweleryCategorizationPredicate *categorizationPredicate = [predicates objectAtIndex:0];
    int predicate_level = [[jeweleryCategorizationPredicate valueWithPath:@"CategorizationPredicateLevel"] intValue];
    if (predicate_level < 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateFailed:),
                              @"Expected non-negative value for Categorization Predicate Level");
        return;
    }
    categorizationPredicate.categorization_predicate_level = [NSNumber numberWithInt:predicate_level];
    
    categorizationPredicate.delegate = delegate;
    
    SMXMLElement *categorizationPredicateResultSet = [jeweleryCategorizationPredicate
                                                      childNamed:@"CategorizationPredicateResultSet"];
    NSArray *categorizationPredicateCategories = [categorizationPredicateResultSet childrenNamed:@"MOJeweleryCategory"];
    if ([categorizationPredicateCategories count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateFailed:),
                              @"Expected atleast one result category for the predicate");
        return;
    }
    
    resultCategoriesCount = 0;
    for (SMXMLElement *category in categorizationPredicateCategories) {
        resultCategoriesCount++;
        NSString *action = [category attributeNamed:@"action"];
        
        if ([action isEqualToString:@"ADD"]) {
            [MOJeweleryCategory createJeweleryCategoryFrom:category
                                                 inContext:managedObjectContext
                                              withDelegate:categorizationPredicate];
        }
        else if ([action isEqualToString:@"update"])
        {
            [MOJeweleryCategory updateJeweleryCategoryFrom:category
                                                 inContext:managedObjectContext
                                              withDelegate:categorizationPredicate];
        }
        else if ([action isEqualToString:@"delete"])
        {
            [MOJeweleryCategory deleteJeweleryCategoryFrom:category
                                                 inContext:managedObjectContext
                                              withDelegate:categorizationPredicate];
        }
        else
        {
            SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateFailed:),
                                  @"Invalid action");
        }
    }

}

- (void) jeweleryCategoryUpdateCompleted
{
    if (--resultCategoriesCount == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateCompleted),NULL);
    }
}

- (void) jeweleryCategoryUpdateFailed:(NSString *)reason
{
    SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateFailed:), reason);
}

@end
