//
//  MOJeweleryCommodity.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "SMXMLDocument.h"

@class MOJewelerySpecificationItem;

@protocol MOJeweleryCommodityDelegate <NSObject>

@optional
- (void) jeweleryCommodityUpdateCompleted;
- (void) jeweleryCommodityUpdateFailed: (NSString *)reason;

@end

@interface MOJeweleryCommodity : NSManagedObject

@property (nonatomic, retain) NSNumber * commodity_id;
@property (nonatomic, retain) NSString * commodity_name;
@property (nonatomic, retain) NSNumber * commodity_price_per_unit;
@property (nonatomic, retain) NSSet *commodity_used_in_specification_item;
@end

@interface MOJeweleryCommodity (CoreDataGeneratedAccessors)

- (void)addCommodity_used_in_specification_itemObject:(MOJewelerySpecificationItem *)value;
- (void)removeCommodity_used_in_specification_itemObject:(MOJewelerySpecificationItem *)value;
- (void)addCommodity_used_in_specification_item:(NSSet *)values;
- (void)removeCommodity_used_in_specification_item:(NSSet *)values;

@end

@interface MOJeweleryCommodity ()

+ (void) createJeweleryCommodityFrom:(SMXMLElement *)jeweleryCommodity
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJeweleryCommodityDelegate>)delegate;


+ (void) deleteJeweleryCommodityFrom:(SMXMLElement *)jeweleryCommodity
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJeweleryCommodityDelegate>)delegate;


+ (void) updateJeweleryCommodityFrom:(SMXMLElement *)jeweleryCommodity
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJeweleryCommodityDelegate>)delegate;


@property (weak) id <MOJeweleryCommodityDelegate> delegate;
@end
