//
//  MOUsers.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOUsers.h"

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)

@implementation MOUsers

@dynamic password;
@dynamic username;

@synthesize delegate;

+ (void) createUsers:(SMXMLElement *)users
           inContext:(NSManagedObjectContext *)managedObjectContext
        withDelegate:(id <MOUsersDelegate>)delegate
{
    // Add our default user object in the Core Data
    MOUsers *user = (MOUsers *)[NSEntityDescription insertNewObjectForEntityForName:@"MOUsers" inManagedObjectContext:managedObjectContext];
    if (!user) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(usersUpdateFailed:),
                              @"Could not create MOUser");
        return;
    }
    
    NSString *username = [users valueWithPath:@"username"];
    if (username == (id)[NSNull null] || username.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(usersUpdateFailed:),
                              @"Empty username");
        return;
    }
    user.username = username;
    
    NSString *password = [users valueWithPath:@"password"];
    if (password == (id)[NSNull null] || password.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(usersUpdateFailed:),
                              @"Empty password");
        return;
    }
    user.password = password;
    user.delegate = delegate;
    SAFE_PERFORM_WITH_ARG(delegate, @selector(usersUpdateCompleted), NULL);
}


+ (void) deleteUsers:(SMXMLElement *)users
           inContext:(NSManagedObjectContext *)managedObjectContext
        withDelegate:(id <MOUsersDelegate>)delegate
{
    NSString *username = [users valueWithPath:@"username"];
    if (username == (id)[NSNull null] || username.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(usersUpdateFailed:),
                              @"Empty username");
    }
    
    NSString *password = [users valueWithPath:@"password"];
    if (password == (id)[NSNull null] || password.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(usersUpdateFailed:),
                              @"Empty password");
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOUsers"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSExpression *usernameInDB = [NSExpression expressionForKeyPath:@"username"];
    NSExpression *usernameINXML = [NSExpression expressionForConstantValue:username];
    
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:usernameInDB
                                                                rightExpression:usernameINXML modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType options:0];
    [fetchRequest setPredicate:predicate];
    NSArray *usersMatched = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if ([usersMatched count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(usersUpdateFailed:),
                              @"No users with given Username found");
        return;
    }
    MOUsers *user = [usersMatched objectAtIndex:0];
    [managedObjectContext deleteObject:user];
    SAFE_PERFORM_WITH_ARG(delegate, @selector(usersUpdateCompleted), NULL);
}


+ (void) updateUsers:(SMXMLElement *)users
           inContext:(NSManagedObjectContext *)managedObjectContext
        withDelegate:(id <MOUsersDelegate>)delegate
{
    NSString *username = [users valueWithPath:@"username"];
    if (username == (id)[NSNull null] || username.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(usersUpdateFailed:),
                              @"Empty username");
    }
    
    NSString *password = [users valueWithPath:@"password"];
    if (password == (id)[NSNull null] || password.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(usersUpdateFailed:),
                              @"Empty password");
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOUsers"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSExpression *usernameInDB = [NSExpression expressionForKeyPath:@"username"];
    NSExpression *usernameINXML = [NSExpression expressionForConstantValue:username];
    
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:usernameInDB
                                                                rightExpression:usernameINXML modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType options:0];
    [fetchRequest setPredicate:predicate];
    NSArray *usersMatched = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if ([usersMatched count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(usersUpdateFailed:),
                              @"No users with given Username found");
        return;
    }
    MOUsers *user = [usersMatched objectAtIndex:0];
    user.password = password;
    SAFE_PERFORM_WITH_ARG(delegate, @selector(usersUpdateCompleted), NULL);
}

@end

