//
//  MOJewelery.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MOJeweleryImage.h"
#import "MOJewelerySpecification.h"

#import "SMXMLDocument.h"
//#import "MODownloadHelper.h"
#import "MOFileSystemManager.h"

@class MOJeweleryCategory, MOJeweleryImage, MOJewelerySpecification;

@protocol MOJeweleryDelegate <NSObject>

@optional
- (void) jeweleryUpdateCompleted;
- (void) jeweleryUpdateFailed: (NSString *)reason;

@end

@interface MOJewelery : NSManagedObject <MOJeweleryImageDelegate, MOJewelerySpecificationDelegate/*, MODownloadHelperDelegate*/>

@property (nonatomic, retain) NSString * jewelery_caption_text;
@property (nonatomic, retain) NSString * jewelery_description;
@property (nonatomic, retain) NSNumber * jewelery_item_id;
@property (nonatomic, retain) NSString * jewelery_name;
@property (nonatomic, retain) NSString * jewelery_preview_image;
@property (nonatomic, retain) NSNumber * jewelery_price;
@property (nonatomic, retain) NSSet *belongs_to_categories;
@property (nonatomic, retain) MOJeweleryImage *jewelery_image;
@property (nonatomic, retain) MOJewelerySpecification *jewelery_specification;
@end

@interface MOJewelery (CoreDataGeneratedAccessors)

- (void)addBelongs_to_categoriesObject:(MOJeweleryCategory *)value;
- (void)removeBelongs_to_categoriesObject:(MOJeweleryCategory *)value;
- (void)addBelongs_to_categories:(NSSet *)values;
- (void)removeBelongs_to_categories:(NSSet *)values;

@end

@interface MOJewelery()
{
  //  MODownloadHelper *downloadHelper;
    MOFileSystemManager *fileSystemManager;
    
    BOOL isUpdating;
    float percentComplete;
    BOOL hasPreviewImage;
    BOOL hasDownloadedPreviewImage;
    BOOL hasDownloadedImage;
    BOOL hasCreatedSpecification;
}

+ (void) createJeweleryFrom:(SMXMLElement *)jewelery
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJeweleryDelegate>)delegate;


+ (void) deleteJeweleryFrom:(SMXMLElement *)jewelery
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJeweleryDelegate>)delegate;
                            

+ (void) updateJeweleryFrom:(SMXMLElement *)jewelery
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJeweleryDelegate>)delegate;

- (void) start;

@property (readonly) BOOL isUpdating;
@property BOOL hasPreviewImage;
@property int previewCount;
@property BOOL hasDownloadedPreviewImage;
@property BOOL hasDownloadedImage;
@property BOOL hasCreatedSpecification;
@property (readonly) float percentComplete;
@property MOFileSystemManager *fileSystemManager;
//@property (strong) MODownloadHelper *downloadHelper;

@property (weak) id <MOJeweleryDelegate> delegate;
@end
