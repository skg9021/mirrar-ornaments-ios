//
//  MOJeweleryCategory.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "SMXMLDocument.h"
//#import "MODownloadHelper.h"
#import "MOFileSystemManager.h"

@class MOJewelery, MOJeweleryCategorizationPredicate;

@protocol MOJeweleryCategoryDelegate <NSObject>

@optional
- (void) jeweleryCategoryUpdateCompleted;
- (void) jeweleryCategoryUpdateFailed: (NSString *)reason;

@end

@interface MOJeweleryCategory : NSManagedObject /*<MODownloadHelperDelegate>*/

@property (nonatomic, retain) NSString * category_caption_image;
@property (nonatomic, retain) NSString * category_description;
@property (nonatomic, retain) NSNumber * category_id;
@property (nonatomic, retain) NSString * category_name;
@property (nonatomic, retain) NSSet *category_jewelery_set;
@property (nonatomic, retain) MOJeweleryCategorizationPredicate *category_predicate;
@end

@interface MOJeweleryCategory (CoreDataGeneratedAccessors)

- (void)addCategory_jewelery_setObject:(MOJewelery *)value;
- (void)removeCategory_jewelery_setObject:(MOJewelery *)value;
- (void)addCategory_jewelery_set:(NSSet *)values;
- (void)removeCategory_jewelery_set:(NSSet *)values;

@end

@interface MOJeweleryCategory ()
{
    //MODownloadHelper *downloadHelper;
    MOFileSystemManager *fileSystemManager;
    
    BOOL isUpdating;
    float percentComplete;
}

+ (void) createJeweleryCategoryFrom:(SMXMLElement *)jeweleryCategory
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJeweleryCategoryDelegate>)delegate;


+ (void) deleteJeweleryCategoryFrom:(SMXMLElement *)jeweleryCategory
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJeweleryCategoryDelegate>)delegate;


+ (void) updateJeweleryCategoryFrom:(SMXMLElement *)jeweleryCategory
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJeweleryCategoryDelegate>)delegate;

@property (readonly) BOOL isUpdating;
@property (readonly) float percentComplete;
//@property (strong) MODownloadHelper *downloadHelper;
@property MOFileSystemManager *fileSystemManager;

@property (weak) id <MOJeweleryCategoryDelegate> delegate;
@end
