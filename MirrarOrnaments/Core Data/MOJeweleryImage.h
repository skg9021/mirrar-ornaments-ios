//
//  MOJeweleryImage.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MOJeweleryHingePointsSet.h"

#import "SMXMLDocument.h"
//#import "MODownloadHelper.h"
#import "MOFileSystemManager.h"

@class MOJewelery, MOJeweleryHingePointsSet;

@protocol MOJeweleryImageDelegate <NSObject>

@optional
- (void) jeweleryImageUpdateCompleted;
- (void) jeweleryImageUpdateFailed: (NSString *)reason;

@end

@interface MOJeweleryImage : NSManagedObject </*MODownloadHelperDelegate,*/ MOJeweleryHingePointsSetDelegate>

@property (nonatomic, retain) NSString * jewelery_image_filename;
@property (nonatomic, retain) NSNumber * jewelery_image_height;
@property (nonatomic, retain) NSNumber * jewelery_image_width;
@property (nonatomic, retain) MOJewelery *belongs_to_jewelery;
@property (nonatomic, retain) MOJeweleryHingePointsSet *hinge_point_set;

@end

@interface MOJeweleryImage ()
{
    //MODownloadHelper *downloadHelper;
    MOFileSystemManager *fileSystemManager;
    BOOL isUpdating;
    float percentComplete;
}

+ (void) createJeweleryImageFrom:(SMXMLElement *)jeweleryImage
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJeweleryImageDelegate>)delegate;

@property (readonly) BOOL isUpdating;
@property BOOL hingePointsSetCompleted;
@property (readonly) float percentComplete;
@property MOFileSystemManager *fileSystemManager;
//@property (strong) MODownloadHelper *downloadHelper;

@property (weak) id <MOJeweleryImageDelegate> delegate;
@end
