//
//  MOJeweleryCategory.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOJeweleryCategory.h"
#import "MOJewelery.h"
#import "MOJeweleryCategorizationPredicate.h"

#define SERVER_URL  @"107.21.122.227"
#define UPDATE_DATA_URL @"107.21.122.227/mo/images"
#define APPEND_PATH(PATH1, PATH2) [PATH1 stringByAppendingPathComponent:PATH2]

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)

@implementation MOJeweleryCategory

@dynamic category_caption_image;
@dynamic category_description;
@dynamic category_id;
@dynamic category_name;
@dynamic category_jewelery_set;
@dynamic category_predicate;

@synthesize delegate, isUpdating, percentComplete;
//@synthesize downloadHelper;
@synthesize fileSystemManager;

+ (void) createJeweleryCategoryFrom:(SMXMLElement *)jeweleryCategory
                          inContext:(NSManagedObjectContext *)managedObjectContext
                       withDelegate:(id <MOJeweleryCategoryDelegate>)delegate
{
    MOJeweleryCategory *newJeweleryCategory =
    [NSEntityDescription insertNewObjectForEntityForName:@"MOJeweleryCategory"
                                  inManagedObjectContext:managedObjectContext];
    if (!newJeweleryCategory) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategoryUpdateFailed:),
                              @"Could not create MOJeweleryCategory");
        return;
    }
    
        
    NSString *name          = [jeweleryCategory valueWithPath:@"CategoryName"];
    if (name == (id)[NSNull null] || name.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategoryUpdateFailed:),
                              @"Empty name for the category");
    }
    newJeweleryCategory.category_name = name;
    
    int uid                 = [[jeweleryCategory valueWithPath:@"CategoryID"] intValue];
    if (uid <= 0 ) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategoryUpdateFailed:),
                              @"Expecting a positive id for the category");
    }
    newJeweleryCategory.category_id = [NSNumber numberWithInt:uid];
    
    newJeweleryCategory.fileSystemManager = [[MOFileSystemManager alloc] init];
    
    NSString *description   = [jeweleryCategory valueWithPath:@"CategoryDescription"];
    
    newJeweleryCategory.category_description = description;
    newJeweleryCategory.delegate = delegate;
    newJeweleryCategory.category_predicate = delegate;
    
    NSString *captionImage  = [jeweleryCategory valueWithPath:@"CategoryCaptionImage"];
    if (!(captionImage == (id)[NSNull null] || captionImage.length == 0)) {
        NSString *destImagePath = [newJeweleryCategory.fileSystemManager pathInImageStorageDirectory:captionImage];
        newJeweleryCategory.category_caption_image = destImagePath;
    }
    else
    {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategoryUpdateCompleted), NULL);
    }
    
    
    
}


+ (void) deleteJeweleryCategoryFrom:(SMXMLElement *)jeweleryCategory
                          inContext:(NSManagedObjectContext *)managedObjectContext
                       withDelegate:(id <MOJeweleryCategoryDelegate>)delegate
{
    int uid                 = [[jeweleryCategory valueWithPath:@"CategoryID"] intValue];
    if (uid <= 0 ) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategoryUpdateFailed:),
                              @"Expecting a positive id for the category");
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOJeweleryCategory"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSExpression *exprCategoryId = [NSExpression expressionForKeyPath:@"category_id"];
    NSExpression *exprUid = [NSExpression expressionForConstantValue:[NSNumber numberWithInt:uid]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprCategoryId
                                                                rightExpression:exprUid modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType options:0];
    [fetchRequest setPredicate:predicate];
    NSArray *categories = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if ([categories count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategoryUpdateFailed:),
                              @"No categories for the given ID found");
        return;
    }
    
    MOJeweleryCategory *category = [categories objectAtIndex:0];
    if ([category.fileSystemManager fileExistsInPath:category.category_caption_image]) {
        [category.fileSystemManager deleteFileAtPath:category.category_caption_image error:nil];
    }
    [managedObjectContext deleteObject:category];
    SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategoryUpdateCompleted), NULL);

}


+ (void) updateJeweleryCategoryFrom:(SMXMLElement *)jeweleryCategory
                          inContext:(NSManagedObjectContext *)managedObjectContext
                       withDelegate:(id <MOJeweleryCategoryDelegate>)delegate
{
    int uid                 = [[jeweleryCategory valueWithPath:@"CategoryID"] intValue];
    if (uid <= 0 ) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategoryUpdateFailed:),
                              @"Expecting a positive id for the category");
    }

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOJeweleryCategory"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSExpression *exprCategoryId = [NSExpression expressionForKeyPath:@"category_id"];
    NSExpression *exprUid = [NSExpression expressionForConstantValue:[NSNumber numberWithInt:uid]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprCategoryId
                                                                rightExpression:exprUid modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType options:0];
    [fetchRequest setPredicate:predicate];
    NSArray *categories = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if ([categories count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategoryUpdateFailed:),
                              @"No categories for the given ID found");
    }
    
    MOJeweleryCategory *category = [categories objectAtIndex:0];
    
    NSString *name          = [jeweleryCategory valueWithPath:@"CategoryName"];
    if (name == (id)[NSNull null] || name.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategoryUpdateFailed:),
                              @"Empty name for the category");
    }
    category.category_name = name;
    
    
    NSString *captionImage  = [jeweleryCategory valueWithPath:@"CategoryCaptionImage"];
    if (!(captionImage == (id)[NSNull null] || captionImage.length == 0)) {
        NSString *destImagePath = [category.fileSystemManager pathInImageStorageDirectory:captionImage];
        category.category_caption_image = destImagePath;
    }
    
    
    NSString *description   = [jeweleryCategory valueWithPath:@"CategoryDescription"];
    category.category_description = description;
    category.delegate = delegate;
    category.category_predicate = delegate;
    SAFE_PERFORM_WITH_ARG(category.delegate, @selector(jeweleryCategoryUpdateCompleted), NULL);
}

@end
