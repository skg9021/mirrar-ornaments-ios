//
//  MOJeweleryCategorizationPredicate.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MOJeweleryCategory.h"

#import "SMXMLDocument.h"

@class MOJeweleryCategory;

@protocol MOJeweleryCategorizationPredicateDelegate <NSObject>

@optional
- (void) jeweleryCategorizationPredicateUpdateCompleted;
- (void) jeweleryCategorizationPredicateUpdateFailed: (NSString *)reason;

@end

@interface MOJeweleryCategorizationPredicate : NSManagedObject <MOJeweleryCategoryDelegate>

@property (nonatomic, retain) NSNumber * categorization_predicate_level;
@property (nonatomic, retain) NSString * categorization_predicate_name;
@property (nonatomic, retain) NSSet *predicate_result_set;
@end

@interface MOJeweleryCategorizationPredicate (CoreDataGeneratedAccessors)

- (void)addPredicate_result_setObject:(MOJeweleryCategory *)value;
- (void)removePredicate_result_setObject:(MOJeweleryCategory *)value;
- (void)addPredicate_result_set:(NSSet *)values;
- (void)removePredicate_result_set:(NSSet *)values;

@end

@interface MOJeweleryCategorizationPredicate ()
{
    BOOL isUpdating;
    float percentComplete;
}

+ (void) createJeweleryCategorizationPredicateFrom:(SMXMLElement *)jeweleryCategorizationPredicate
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                      withDelegate:(id <MOJeweleryCategorizationPredicateDelegate>)delegate;


+ (void) deleteJeweleryCategorizationPredicateFrom:(SMXMLElement *)jeweleryCategorizationPredicate
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                      withDelegate:(id <MOJeweleryCategorizationPredicateDelegate>)delegate;


+ (void) updateJeweleryCategorizationPredicateFrom:(SMXMLElement *)jeweleryCategorizationPredicate
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                      withDelegate:(id <MOJeweleryCategorizationPredicateDelegate>)delegate;

@property (readonly) BOOL isUpdating;
@property (readonly) float percentComplete;

@property (weak) id <MOJeweleryCategorizationPredicateDelegate> delegate;
@end
