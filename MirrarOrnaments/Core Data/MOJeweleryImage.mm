//
//  MOJeweleryImage.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOJeweleryImage.h"
#import "MOJewelery.h"
#import "MOJeweleryHingePointsSet.h"

#define SERVER_URL  @"107.21.122.227"
#define UPDATE_DATA_URL @"107.21.122.227/mo/images"
#define APPEND_PATH(PATH1, PATH2) [PATH1 stringByAppendingPathComponent:PATH2]

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)

@implementation MOJeweleryImage

@dynamic jewelery_image_filename;
@dynamic jewelery_image_height;
@dynamic jewelery_image_width;
@dynamic belongs_to_jewelery;
@dynamic hinge_point_set;

@synthesize delegate, isUpdating, percentComplete, hingePointsSetCompleted;
//@synthesize downloadHelper;
@synthesize fileSystemManager;

+ (void) createJeweleryImageFrom:(SMXMLElement *)jeweleryImage
                       inContext:(NSManagedObjectContext *)managedObjectContext
                    withDelegate:(id <MOJeweleryImageDelegate>)delegate
{
    MOJeweleryImage *newJeweleryImage =
    [NSEntityDescription insertNewObjectForEntityForName:@"MOJeweleryImage"
                                  inManagedObjectContext:managedObjectContext];
    if (!newJeweleryImage) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryImageUpdateFailed:),
                              @"Could not create MOJeweleryImage");
        return;
    }
    
    int jeweleryImageHeight = [[jeweleryImage valueWithPath:@"JeweleryImageHeight"] intValue];
    if (jeweleryImageHeight <= 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryImageUpdateFailed:),
                              @"Expecting a positive height for image");
    }
    newJeweleryImage.jewelery_image_height = [NSNumber numberWithInt:jeweleryImageHeight];
    
    
    int jeweleryImageWidth = [[jeweleryImage valueWithPath:@"JeweleryImageWidth"] intValue];
    if (jeweleryImageWidth <= 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryImageUpdateFailed:),
                              @"Expecting a positive width for image");
    }
    newJeweleryImage.jewelery_image_width = [NSNumber numberWithInt:jeweleryImageWidth];
    newJeweleryImage.fileSystemManager = [[MOFileSystemManager alloc] init];
    
    NSString *imageFilename  = [jeweleryImage valueWithPath:@"JeweleryFilename"];
    if (!(imageFilename == (id)[NSNull null] || imageFilename.length == 0)) {
        NSString *destImagePath = [newJeweleryImage.fileSystemManager pathInImageStorageDirectory:imageFilename];
        newJeweleryImage.jewelery_image_filename = destImagePath;
    }
    else
    {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryImageUpdateFailed:),
                              @"Non-empty image filename required");
        return;
    }
    
    newJeweleryImage.hingePointsSetCompleted = NO;
    SMXMLElement *hingePointSet = [jeweleryImage childNamed:@"MOJeweleryHingePoint"];
    [MOJeweleryHingePointsSet createJeweleryHingePointsSetFrom:hingePointSet
                                                 inContext:managedObjectContext
                                              withDelegate:newJeweleryImage];
    newJeweleryImage.delegate = delegate;
    newJeweleryImage.belongs_to_jewelery = (MOJewelery *)delegate;
    
}

@end

