//
//  MOUsers.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "SMXMLDocument.h"

@protocol MOUsersDelegate <NSObject>

@optional
- (void) usersUpdateCompleted;
- (void) usersUpdateFailed: (NSString *)reason;

@end

@interface MOUsers : NSManagedObject

@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * username;

@end

@interface MOUsers ()

+ (void) createUsers:(SMXMLElement *)users
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOUsersDelegate>)delegate;


+ (void) deleteUsers:(SMXMLElement *)users
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOUsersDelegate>)delegate;


+ (void) updateUsers:(SMXMLElement *)users
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOUsersDelegate>)delegate;

@property (weak) id <MOUsersDelegate> delegate;
@end
