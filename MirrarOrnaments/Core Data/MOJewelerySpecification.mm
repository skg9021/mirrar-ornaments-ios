//
//  MOJewelerySpecification.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOJewelerySpecification.h"
#import "MOJewelery.h"
#import "MOJewelerySpecificationItem.h"

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)


static int specificationItemsToUpdateCount = 0;

@implementation MOJewelerySpecification

@dynamic specification_name;
@dynamic belongs_to_jewelery;
@dynamic specification_constituents;

@synthesize delegate;

+ (void) createJewelerySpecificationFrom:(SMXMLElement *)jewelerySpecification
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id<MOJewelerySpecificationDelegate>)delegate
{
    // Get the data members first
    MOJewelerySpecification *newJewelerySpecification =
        [NSEntityDescription insertNewObjectForEntityForName:@"MOJewelerySpecification"
                                inManagedObjectContext:managedObjectContext];
    
    if (!newJewelerySpecification) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jewelerySpecificationUpdateFailed:),
                              @"Could not create MOJewelerySpecification");
        return;
    }
    
    NSString *name = [jewelerySpecification attributeNamed:@"name"];
    newJewelerySpecification.specification_name = name;
    if (!name) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jewelerySpecificationUpdateFailed:),
                              @"Expecting a not null name for MOJewelerySpecification");
        return;
    }
    
    newJewelerySpecification.delegate = delegate;
    NSArray *jewelerySpeficationItems = [jewelerySpecification childrenNamed:@"MOJewelerySpecificationItem"];
    specificationItemsToUpdateCount = [jewelerySpeficationItems count];
    for (SMXMLElement *specificationItem in jewelerySpeficationItems) {
        [MOJewelerySpecificationItem createJewelerySpecificationItemFrom:specificationItem
                        inContext:managedObjectContext withDelegate:newJewelerySpecification];
    }
 
    newJewelerySpecification.belongs_to_jewelery = (MOJewelery*)delegate;
}

- (void) jewelerySpecificationItemUpdateCompleted
{
    if (--specificationItemsToUpdateCount == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jewelerySpecificationUpdateCompleted),NULL);
    }
}

- (void) jewelerySpecificationItemUpdateFailed: (NSString *)reason
{
    SAFE_PERFORM_WITH_ARG(delegate, @selector(jewelerySpecificationUpdateFailed:),reason);
}

@end
