//
//  MOJeweleryHingePoint.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOJeweleryHingePoint.h"
#import "MOJeweleryHingePointsSet.h"

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)

@implementation MOJeweleryHingePoint

@dynamic hinge_point_distance;
@dynamic hinge_point_x;
@dynamic hinge_point_y;
@dynamic belongs_to_hinge_point_set;

@synthesize delegate;


+ (void) createJeweleryHingePointFrom:(SMXMLElement *)jeweleryHingePoint
                            inContext:(NSManagedObjectContext *)managedObjectContext
                         withDelegate:(id <MOJeweleryHingePointDelegate>)delegate
{
    MOJeweleryHingePoint *newHingePoint =
    [NSEntityDescription insertNewObjectForEntityForName:@"MOJeweleryHingePoint"
                                  inManagedObjectContext:managedObjectContext];
    
    if (!newHingePoint) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryHingePointUpdateFailed:),
                              @"Could not create MOJeweleryHingePoint");
        return;
    }
    
    int x_coordinate = [[jeweleryHingePoint attributeNamed:@"x"] intValue];
    if (x_coordinate < 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryHingePointUpdateFailed:),
                              @"Expecting a non-zero x coordinate");
        return;
    }
    newHingePoint.hinge_point_x = [NSNumber numberWithInt:x_coordinate];
    
    int y_coordinate = [[jeweleryHingePoint attributeNamed:@"y"] intValue];
    if (x_coordinate < 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryHingePointUpdateFailed:),
                              @"Expecting a non-zero y coordinate");
        return;
    }
    newHingePoint.hinge_point_y = [NSNumber numberWithInt:y_coordinate];
    
    int distance = [[jeweleryHingePoint attributeNamed:@"distance"] doubleValue];
    newHingePoint.hinge_point_distance = [NSNumber numberWithDouble:distance];
    
    newHingePoint.belongs_to_hinge_point_set = delegate;
    SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryHingePointUpdateCompleted),NULL);
}

@end
