//
//  MOJewelery.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOJewelery.h"
#import "MOJeweleryCategory.h"
#import "MOJeweleryImage.h"
#import "MOJewelerySpecification.h"

#define SERVER_URL  @"107.21.122.227"
#define UPDATE_DATA_URL @"107.21.122.227/mo/images"
#define APPEND_PATH(PATH1, PATH2) [PATH1 stringByAppendingPathComponent:PATH2]

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)

@implementation MOJewelery

@dynamic jewelery_caption_text;
@dynamic jewelery_description;
@dynamic jewelery_item_id;
@dynamic jewelery_name;
@dynamic jewelery_preview_image;
@dynamic jewelery_price;
@dynamic belongs_to_categories;
@dynamic jewelery_image;
@dynamic jewelery_specification;

@synthesize delegate, isUpdating, percentComplete, hasPreviewImage, previewCount;;
@synthesize hasDownloadedPreviewImage, hasDownloadedImage, hasCreatedSpecification;
//@synthesize downloadHelper;
@synthesize fileSystemManager;

+ (void) createJeweleryFrom:(SMXMLElement *)jewelery
                  inContext:(NSManagedObjectContext *)managedObjectContext
               withDelegate:(id <MOJeweleryDelegate>)delegate
{
    MOJewelery *newJewelery =
    [NSEntityDescription insertNewObjectForEntityForName:@"MOJewelery"
                                  inManagedObjectContext:managedObjectContext];
    if (!newJewelery) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryUpdateFailed:),
                              @"Could not create MOJewelery");
        return;
    }
    
    [newJewelery start];
    newJewelery.delegate = delegate;
    
    int uid = [[jewelery valueWithPath:@"JeweleryItemID"] intValue];
    if (uid <= 0 ) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryUpdateFailed:),
                              @"Expecting a positive ID for jewelery");
        return;
    }
    newJewelery.jewelery_item_id = [NSNumber numberWithInt:uid];
    
    NSString *jeweleryName = [jewelery valueWithPath:@"JeweleryName"];
    if (jeweleryName == (id)[NSNull null] || jeweleryName.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryUpdateFailed:),
                              @"Expecting a non-null name for the jewelery");
        return;
    }
    newJewelery.jewelery_name = jeweleryName;
    
    NSString *jeweleryCaptionText = [jewelery valueWithPath:@"JeweleryCaptionText"];
    newJewelery.jewelery_caption_text = jeweleryCaptionText;
    
    NSString *jeweleryDescription = [jewelery valueWithPath:@"JeweleryDescription"];
    newJewelery.jewelery_description = jeweleryDescription;
    
    newJewelery.fileSystemManager = [[MOFileSystemManager alloc] init];
    
    NSString *jeweleryPreviewImage = [jewelery valueWithPath:@"JeweleryPreviewImage"];
    if (!(jeweleryPreviewImage == (id)[NSNull null] || jeweleryPreviewImage.length == 0)) {
        newJewelery.hasPreviewImage = YES;
        newJewelery.hasDownloadedPreviewImage = NO;
        newJewelery.previewCount = 0;
        NSString *destImagePath = [newJewelery.fileSystemManager pathInImageStorageDirectory:jeweleryPreviewImage];
        newJewelery.jewelery_preview_image = destImagePath;
    }

    double jewelery_price = [[jewelery valueWithPath:@"JeweleryPrice"] doubleValue];
    newJewelery.jewelery_price = [NSNumber numberWithDouble:jewelery_price];
    
    NSArray *categoryStringIDs = [[jewelery childNamed:@"JeweleryCategories"].children valueForKey:@"value"];
    
    NSMutableArray *categoryIDs = [[NSMutableArray alloc] init];
    for (NSString *categoryStringID in categoryStringIDs) {
        [categoryIDs addObject:[NSNumber numberWithInt:[categoryStringID intValue]]];
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOJeweleryCategorizationPredicate"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setResultType:NSDictionaryResultType];
    [fetchRequest setReturnsDistinctResults:YES];
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:@"categorization_predicate_name"]];
    
    NSArray *categorization_predicates = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    NSNumber *numberOfPredicates = [categorization_predicates valueForKeyPath:@"@count"];
    
    NSArray *array = [[NSArray alloc] initWithArray:categoryIDs];
    
    
    NSNumber *numberofInputCategories = [NSNumber numberWithInt:[categoryIDs count]];
    NSComparisonResult comparisonResult = [numberofInputCategories compare:numberOfPredicates];
    if ( comparisonResult == NSOrderedAscending) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryUpdateFailed:),
                              @"Number of input categories is less than number of predicates");
        return;
    }
    
    NSFetchRequest *fetchRequestCategories = [[NSFetchRequest alloc] init];
    NSEntityDescription *categoryEntity = [NSEntityDescription entityForName:@"MOJeweleryCategory"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequestCategories setEntity:categoryEntity];
    /*
    NSExpression *exprCategoryId = [NSExpression expressionForKeyPath:@"category_id"];
    NSExpression *exprUid = [NSExpression expressionForConstantValue:[NSNumber numberWithInt:2]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprCategoryId
                                                                rightExpression:exprUid modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType options:0];
    
    
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:[NSExpression expressionForKeyPath:@"category_id"]
                            rightExpression:[NSExpression expressionForConstantValue:[array objectAtIndex:0]] modifier:NSDirectPredicateModifier
                            type:NSEqualToPredicateOperatorType options:0];;
    */
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category_id IN %@",array];
    [fetchRequestCategories setPredicate:predicate];
    
    NSError *error;
    NSArray *results = [managedObjectContext executeFetchRequest:fetchRequestCategories error:&error];
    NSSet *categories = [NSSet setWithArray:results];
    
    comparisonResult = [[NSNumber numberWithInt:[categories count]] compare:numberofInputCategories];
    
    if (comparisonResult == NSOrderedAscending) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryUpdateFailed:),
                              @"Not all categories were found in database");
        return;
    }

    [newJewelery addBelongs_to_categories:categories];
    
    newJewelery.hasDownloadedImage = NO;
    SMXMLElement *jeweleryImage = [jewelery childNamed:@"MOJeweleryImage"];
    [MOJeweleryImage createJeweleryImageFrom:jeweleryImage inContext:managedObjectContext
                                withDelegate:newJewelery];
    
    
    newJewelery.hasCreatedSpecification = NO;
    SMXMLElement *jewelerySpecification = [jewelery childNamed:@"MOJewelerySpecification"];
    if (jewelerySpecification) {
        [MOJewelerySpecification createJewelerySpecificationFrom:jewelerySpecification
                                                       inContext:managedObjectContext withDelegate:newJewelery];
    }
    newJewelery.hasCreatedSpecification = YES;
    
    SAFE_PERFORM_WITH_ARG(newJewelery.delegate, @selector(jeweleryUpdateCompleted), NULL);
}


+ (void) deleteJeweleryFrom:(SMXMLElement *)jewelery
                  inContext:(NSManagedObjectContext *)managedObjectContext
               withDelegate:(id <MOJeweleryDelegate>)delegate
{
    int uid = [[jewelery valueWithPath:@"JeweleryItemID"] intValue];
    if (uid <= 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryUpdateFailed:),
                              @"Expected a non zero value for Jewelery ID");
        return;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOJewelery"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSExpression *exprJeweleryID = [NSExpression expressionForKeyPath:@"jewelery_item_id"];
    NSExpression *exprUID = [NSExpression expressionForConstantValue:[NSNumber numberWithInt:uid]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprJeweleryID
                                                                rightExpression:exprUID modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType options:0];
    [fetchRequest setPredicate:predicate];
    NSArray *jeweleries = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    if ([jeweleries count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryUpdateFailed:),
                              @"No jewelery with given id found");
        return;
    }
    /*
     MOJeweleryCategorizationPredicate *categorizationPredicate = [predicates objectAtIndex:0];
     
     for (MOJeweleryCategory *category in categorizationPredicate.predicate_result_set) {
     [MOJeweleryCategory deleteJeweleryCategoryFrom:<#(SMXMLElement *)#> inContext:<#(NSManagedObjectContext *)#> withDelegate:<#(id<MOJeweleryCategoryDelegate>)#>]
     }
     */
    [managedObjectContext deleteObject:[jeweleries objectAtIndex:0]];
    SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryCategorizationPredicateUpdateCompleted), NULL);
    
}


+ (void) updateJeweleryFrom:(SMXMLElement *)jewelery
                  inContext:(NSManagedObjectContext *)managedObjectContext
               withDelegate:(id <MOJeweleryDelegate>)delegate
{
    int uid = [[jewelery valueWithPath:@"JeweleryItemID"] intValue];
    if (uid <= 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryUpdateFailed:),
                              @"Expected a non zero value for Jewelery ID");
        return;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MOJewelery"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSExpression *exprJeweleryID = [NSExpression expressionForKeyPath:@"jewelery_item_id"];
    NSExpression *exprUID = [NSExpression expressionForConstantValue:[NSNumber numberWithInt:uid]];
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:exprJeweleryID
                                                                rightExpression:exprUID modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType options:0];
    [fetchRequest setPredicate:predicate];
    NSArray *jeweleries = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    if ([jeweleries count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryUpdateFailed:),
                              @"No jewelery with given id found");
        return;
    }
    
    MOJewelery *jeweleryToUpdate = [jeweleries objectAtIndex:0];
    [jeweleryToUpdate start];
    
    NSString *jeweleryName = [jewelery valueWithPath:@"JeweleryName"];
    if (jeweleryName == (id)[NSNull null] || jeweleryName.length == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryUpdateFailed:),
                              @"Expecting a non-null name for the jewelery");
        return;
    }
    jeweleryToUpdate.jewelery_name = jeweleryName;
    
    NSString *jeweleryCaptionText = [jewelery valueWithPath:@"JeweleryCaptionText"];
    jeweleryToUpdate.jewelery_caption_text = jeweleryCaptionText;
    
    NSString *jeweleryDescription = [jewelery valueWithPath:@"JeweleryDescription"];
    jeweleryToUpdate.jewelery_description = jeweleryDescription;
    
    NSString *jeweleryPreviewImage = [jewelery valueWithPath:@"JeweleryPreviewImage"];
    if (!(jeweleryPreviewImage == (id)[NSNull null] || jeweleryPreviewImage.length == 0)) {
        jeweleryToUpdate.hasPreviewImage = YES;
        jeweleryToUpdate.hasDownloadedPreviewImage = NO;
        NSString *destImagePath = [jeweleryToUpdate.fileSystemManager pathInImageStorageDirectory:jeweleryPreviewImage];
        jeweleryToUpdate.jewelery_preview_image = destImagePath;
    }
    
    double jewelery_price = [[jewelery valueWithPath:@"JeweleryPrice"] doubleValue];
    jeweleryToUpdate.jewelery_price = [NSNumber numberWithDouble:jewelery_price];
    
    NSArray *categoryIDs = [[jewelery childNamed:@"JeweleryCategories"].children valueForKey:@"value"];
    
    NSFetchRequest *predicatesFetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *predicatesEntity = [NSEntityDescription entityForName:@"MOJeweleryCategorizationPredicate"
                                              inManagedObjectContext:managedObjectContext];
    [predicatesFetchRequest setEntity:predicatesEntity];
    [predicatesFetchRequest setResultType:NSDictionaryResultType];
    [predicatesFetchRequest setReturnsDistinctResults:YES];
    [predicatesFetchRequest setPropertiesToFetch:[NSArray arrayWithObject:@"categorization_predicate_name"]];
    
    NSArray *categorization_predicates = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    NSNumber *numberOfPredicates = [categorization_predicates valueForKeyPath:@"@count"];
    
    NSArray *array = [[NSArray alloc] initWithArray:categoryIDs];
    
    
    NSNumber *numberofInputCategories = [NSNumber numberWithInt:[categoryIDs count]];
    NSComparisonResult comparisonResult = [numberofInputCategories compare:numberOfPredicates];
    
    if ( comparisonResult == NSOrderedAscending) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryUpdateFailed:),
                              @"Number of input categories is less than number of predicates");
        return;
    }
    
    NSFetchRequest *fetchRequestCategories = [[NSFetchRequest alloc] init];
    NSEntityDescription *categoryEntity = [NSEntityDescription entityForName:@"MOJeweleryCategorizationPredicate"
                                                      inManagedObjectContext:managedObjectContext];
    [fetchRequestCategories setEntity:categoryEntity];
    
    NSPredicate *categoryPredicate = [NSPredicate predicateWithFormat:@"category_id IN %@", array];
    [fetchRequestCategories setPredicate:categoryPredicate];
    
    NSError *error;
    NSSet *categories = [NSSet setWithArray:[managedObjectContext executeFetchRequest:fetchRequestCategories error:&error]];
    
    if ([categories count] < [numberofInputCategories intValue]) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryUpdateFailed:),
                              @"Not all categories were found in database");
        return;
    }
    
    [jeweleryToUpdate removeBelongs_to_categories:jeweleryToUpdate.belongs_to_categories];
    [jeweleryToUpdate addBelongs_to_categories:categories];
    
    jeweleryToUpdate.hasDownloadedImage = NO;
    SMXMLElement *jeweleryImage = [jewelery childNamed:@"MOJeweleryImage"];
    [MOJeweleryImage createJeweleryImageFrom:jeweleryImage inContext:managedObjectContext
                                withDelegate:jeweleryToUpdate];
    
    
    jeweleryToUpdate.hasCreatedSpecification = NO;
    SMXMLElement *jewelerySpecification = [jewelery childNamed:@"MOJewelerySpecification"];
    if (jewelerySpecification) {
        [MOJewelerySpecification createJewelerySpecificationFrom:jewelerySpecification
                                                       inContext:managedObjectContext withDelegate:jeweleryToUpdate];
    }
    jeweleryToUpdate.hasCreatedSpecification = YES;
    
    SAFE_PERFORM_WITH_ARG(jeweleryToUpdate.delegate, @selector(jeweleryUpdateCompleted), NULL);
}

- (void) start
{
    isUpdating = YES;
}
@end
