//
//  MOCoreDataPrereqs.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 14/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//
#ifdef __OBJC__

#import "MOCoreDataHelper.h"
#import "MOJewelery.h"
#import "MOJeweleryCategorizationPredicate.h"
#import "MOJeweleryCategory.h"
#import "MOJeweleryCommodity.h"
#import "MOJeweleryHingePoint.h"
#import "MOJeweleryHingePointsSet.h"
#import "MOJeweleryImage.h"
#import "MOJewelerySpecification.h"
#import "MOJewelerySpecificationItem.h"
#import "MOUsers.h"

#endif