//
//  MOJeweleryHingePointsSet.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MOJeweleryHingePoint.h"

#import "SMXMLDocument.h"

@class MOJeweleryHingePoint, MOJeweleryImage;

@protocol MOJeweleryHingePointsSetDelegate <NSObject>

@optional
- (void) jeweleryHingePointsSetUpdateCompleted;
- (void) jeweleryHingePointsSetUpdateFailed: (NSString *)reason;

@end

@interface MOJeweleryHingePointsSet : NSManagedObject <MOJeweleryHingePointDelegate>

@property (nonatomic, retain) MOJeweleryImage *belongs_to_jewelery_image;
@property (nonatomic, retain) NSSet *hinge_points;
@end

@interface MOJeweleryHingePointsSet (CoreDataGeneratedAccessors)

- (void)addHinge_pointsObject:(MOJeweleryHingePoint *)value;
- (void)removeHinge_pointsObject:(MOJeweleryHingePoint *)value;
- (void)addHinge_points:(NSSet *)values;
- (void)removeHinge_points:(NSSet *)values;

@end

@interface MOJeweleryHingePointsSet ()

+ (void) createJeweleryHingePointsSetFrom:(SMXMLElement *)jeweleryHingePointsSet
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                 withDelegate:(id <MOJeweleryHingePointsSetDelegate>)delegate;


@property (weak) id <MOJeweleryHingePointsSetDelegate> delegate;
@end
