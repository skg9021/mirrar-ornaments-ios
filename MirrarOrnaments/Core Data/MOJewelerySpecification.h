//
//  MOJewelerySpecification.h
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MOJewelerySpecificationItem.h"

#import "SMXMLDocument.h"

@class MOJewelery, MOJewelerySpecificationItem;

@protocol MOJewelerySpecificationDelegate <NSObject>
        
@optional
- (void) jewelerySpecificationUpdateCompleted;
- (void) jewelerySpecificationUpdateFailed: (NSString *)reason;

@end

@interface MOJewelerySpecification : NSManagedObject <MOJewelerySpecificationItemDelegate>

@property (nonatomic, retain) NSString * specification_name;
@property (nonatomic, retain) MOJewelery *belongs_to_jewelery;
@property (nonatomic, retain) NSSet *specification_constituents;

@end

@interface MOJewelerySpecification (CoreDataGeneratedAccessors)

- (void)addSpecification_constituentsObject:(MOJewelerySpecificationItem *)value;
- (void)removeSpecification_constituentsObject:(MOJewelerySpecificationItem *)value;
- (void)addSpecification_constituents:(NSSet *)values;
- (void)removeSpecification_constituents:(NSSet *)values;

@end

@interface MOJewelerySpecification ()

+ (void) createJewelerySpecificationFrom:(SMXMLElement *)jewelerySpecification
                               inContext:(NSManagedObjectContext *)managedObjectContext
                                withDelegate:(id <MOJewelerySpecificationDelegate>)delegate;
 
@property (weak) id <MOJewelerySpecificationDelegate> delegate;

@end

