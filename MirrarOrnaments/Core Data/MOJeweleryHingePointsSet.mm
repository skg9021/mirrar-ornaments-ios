//
//  MOJeweleryHingePointsSet.m
//  MirrarOrnaments
//
//  Created by neelesh soni on 13/08/12.
//  Copyright (c) 2012 Rhapsody Labs. All rights reserved.
//

#import "MOJeweleryHingePointsSet.h"
#import "MOJeweleryHingePoint.h"
#import "MOJeweleryImage.h"

#define SAFE_PERFORM_WITH_ARG(THE_OBJECT, THE_SELECTOR, THE_ARG) \
(([THE_OBJECT respondsToSelector:THE_SELECTOR]) ? \
[THE_OBJECT performSelector:THE_SELECTOR withObject:THE_ARG] : nil)

static int hingePointsCount = 0;

@implementation MOJeweleryHingePointsSet

@dynamic belongs_to_jewelery_image;
@dynamic hinge_points;

@synthesize delegate;

+ (void) createJeweleryHingePointsSetFrom:(SMXMLElement *)jeweleryHingePointsSet
                                inContext:(NSManagedObjectContext *)managedObjectContext
                                    withDelegate:(id <MOJeweleryHingePointsSetDelegate>)delegate
{
    
    MOJeweleryHingePointsSet *newJeweleryHingePointsSet =
    [NSEntityDescription insertNewObjectForEntityForName:@"MOJeweleryHingePointsSet"
                                  inManagedObjectContext:managedObjectContext];
    if (!newJeweleryHingePointsSet) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryHingePointsSetUpdateFailed:),
                              @"Could not create MOJeweleryHingePointsSet");
        return;
    }
    
    NSLog(@"hinge point");
    newJeweleryHingePointsSet.delegate = delegate;
    NSArray *hingePoints = [jeweleryHingePointsSet childrenNamed:@"MOHingePoint"];
    if ([hingePoints count] == 0) {
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryHingePointsSetUpdateFailed:),
                              @"Expected atleast one hinge point in the set");
        return;
    }
    
    
    hingePointsCount = [hingePoints count];
    for (SMXMLElement *hingePoint in hingePoints) {
        
        [MOJeweleryHingePoint createJeweleryHingePointFrom:hingePoint
                                                 inContext:managedObjectContext
                                              withDelegate:newJeweleryHingePointsSet];
    }
    
    newJeweleryHingePointsSet.belongs_to_jewelery_image = (MOJeweleryImage *)delegate;
}

- (void) jeweleryHingePointUpdateCompleted
{
    
    if (--hingePointsCount == 0) {
        NSLog(@"hinge point set");
        SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryHingePointsSetUpdateCompleted),NULL);
    }
}

- (void) jeweleryHingePointUpdateFailed:(NSString *)reason
{
    SAFE_PERFORM_WITH_ARG(delegate, @selector(jeweleryHingePointsSetUpdateFailed:),reason);
}

@end
